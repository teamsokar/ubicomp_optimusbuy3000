<?php

/**
 * calculates distance between two points on a sphere via haversine formula
 * source: http://www.codecodex.com/wiki/Calculate_Distance_Between_Two_Points_on_a_Globe#PHP
 *
 * @param double $latitude1 latitude of source point
 * @param double $longitude1 logitude of source point
 * @param double $latitude2 latitude of destination point
 * @param double $longitude2 longitude of destination point
 * @return double distance calculated distance
 */
function calculate_distance($latitude1, $longitude1, $latitude2, $longitude2) {
	$earth_radius = 6371;

	$dLat = deg2rad($latitude2 - $latitude1);
	$dLon = deg2rad($longitude2 - $longitude1);

	$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
	$c = 2 * asin(sqrt($a));
	$d = $earth_radius * $c;

	return $d;
}


//$link = mysql_connect('localhost', 'root', 'k02+daRaIUZD');

// if (!$link) {
//     die('Could not connect: ' . mysql_error());
// }

require_once(__DIR__.'/include/DB_Connect.php');

// connecting to database
$db = new DB_Connect();
$link = $db->connect();

if (!$link) {
     die('Could not connect: ' . mysql_error());
}

//echo 'Connected successfully <br />';

mysql_close($link);


/**
 *  File to handle all API requests
 *  Accepts GET and POST
 *
 *  Each request will be identified by TAG
 *  Response will be JSON data
 */


 /**
 *  check for POST request
 */
 
if (isset($_POST['tag']) && $_POST['tag'] != '') 
{
    // get tag
    $tag = $_POST['tag'];
	
	// include db handler
	require_once 'include/DB_Functions.php';
	$db_functions = new DB_Functions();

	// response Array
    $response = array("tag" => $tag, "success" => 0, "error" => 0);

     // check for tag type
    if ($tag == 'login') 
    {
        // Request type is Login
        $email = $_POST['email'];
        $password = $_POST['password'];

        // check for user
        $user = $db_functions->getUserByEmailAndPassword($email, $password);
        if ($user != false) 
        {
            // user found
            // echo json with success = 1
            $response["success"] = 1;
            $response["id"] = $user["id"];
            $response["user"]["name"] = $user["name"];
            $response["user"]["email"] = $user["email"];
            $response["user"]["created_at"] = $user["created_at"];
            $response["user"]["updated_at"] = $user["updated_at"];
            $response["user"]["lat"] = $user["lat"];
            $response["user"]["lng"] = $user["lng"];
            echo json_encode($response);
        } 
        else 
        {
            // user not found
            // echo json with error = 1
            $response["error"] = 1;
            $response["error_msg"] = "Incorrect email or password!";
            echo json_encode($response);
        }
    }
    
    else if($tag == 'shoppinglist') 
    {
        if(!empty($_POST['id'])) 
        {
            $shoppinglist_id = $_POST['id'];
            $shoppinglist = $db_functions->getShoppingListById($shoppinglist_id);
            
            if ($shoppinglist != false) 
            {
                $response["success"] = 1;
                $response["shoppinglist"] = $shoppinglist;
                echo $json = json_encode($response);
            }
            else
            {
                $response["error"] = 1;
                $response["error_msg"] = "Shoppinglist with id " .$shoppinglist_id. " not found";
                echo json_encode($response);
            }
        
        } 
        else if(!empty($_POST['user']))
        {
            $user_id = $_POST['user'];

           	if(isset($_POST['withouttags']) && ($_POST['withouttags'] == "true" )){
           		
           		$result = mysql_query("SELECT `lat` , `lng` FROM `users` WHERE `id` = $user_id") or die(mysql_error());
           		$user_home = mysql_fetch_array($result);
           		
           		$result = mysql_query("SELECT `distance` FROM `preferences` WHERE `id` = $user_id") or die(mysql_error());
           		$user_dis = mysql_fetch_array($result);
           		$user_distance = floatval($user_dis['distance']) / 10; // distance is stored in hundrets of meters
           		
           		$shoppinglist = $db_functions->getShoppingListByUserWithoutTags($user_id);
           		
           		if($shoppinglist != false){
           			
           			// remove multiple entries due to different prices in shops, keep only the ones with lowest prices
           			// and inside users distance limit
           			$shoppinglist_size = count($shoppinglist);           			
           			
           			for($i = 0; $i < $shoppinglist_size; $i++){           				
           				if(array_key_exists($i, $shoppinglist)){
           					 
           					for($j = 0; $j < $shoppinglist_size; $j++){
           						
           						if( array_key_exists($i, $shoppinglist) && array_key_exists($j, $shoppinglist)) {
           							if( ($i != $j) && ( $shoppinglist[$i]['product_collection_id'] == $shoppinglist[$j]['product_collection_id'] )){
           								
           								//floatval may cause rounding errors, but I don't care about them here because I only compare and
           								// don't use them for further calculations
           								$price1 = floatval($shoppinglist[$i]['price']);
           								$price2 = floatval($shoppinglist[$j]['price']);
           								
           								// get coordinates of shops to calculate distance
           								$si_id = $shoppinglist[$i]['shop_id'];
           								$sj_id = $shoppinglist[$j]['shop_id'];
           									
           								$result = mysql_query("SELECT `lat` , `lng` FROM `shops` WHERE `id` = $si_id") or die(mysql_error());
           								$si_coords = mysql_fetch_array($result);
           									
           								$result = mysql_query("SELECT `lat` , `lng` FROM `shops` WHERE `id` = $sj_id") or die(mysql_error());
           								$sj_coords = mysql_fetch_array($result);
           									
           								$dis1 = calculate_distance($si_coords['lat'], $si_coords['lng'], $user_home['lat'], $user_home['lng']);
           								$dis2 = calculate_distance($sj_coords['lat'], $sj_coords['lng'], $user_home['lat'], $user_home['lng']);
           									
           								if(
           										($dis1 > $user_distance)	// shop is too far away
           									 || (($price1 > $price2) && $dis2 <= $user_distance) // price lower and in range -> drop the other
           									 || (($dis1 <= $user_distance) && ($dis2 <= $user_distance) && ($dis1 > $dis2) && ($price1 > $price2)) //shop2 is nearer and has better price
           									)
           								  {
           									unset($shoppinglist[$i]);
           								}
           							}
           						}
           					}
           				}
           			}
           		}
           		
           		$shoppinglist = array_values($shoppinglist); //fix array since the holes produced by using unset make an invalid array in json           		
           	} 
           	else 
           	{
           		$shoppinglist = $db_functions->getShoppingListByUser($user_id);
           	}

           	if($shoppinglist != false){
           		$response["success"] = 1;
           		$response["shoppinglist"] = $shoppinglist;
           		echo $json = json_encode($response);
           	}
           	else
           	{
           		$response["error"] = 1;
           		$response["error_msg"] = "Shoppinglist for user with id " .$user_id. " not found";
           		echo json_encode($response);
           	}
        }
        else
        {
            $repsonse["error"] = 1;
            $response["error_msg"] = "No user id or shoppinglist id stated";
            echo json_encode($response);
        }
        //echo var_dump($shoppinglist);
        // var_dump(json_decode($json));
    }
    else if($tag == 'is_product_in_shop')
    {
    	include('is_product_in_shop.php');
    }    
    else if($tag == 'shops')
    {
        include('shops_api.php');
    }
    else if($tag == 'products')
    {
        include('productsinshops_api.php');
    }
    else if($tag == 'budget')
    {
        include('budget.php');
    }
    else if($tag == 'add_product')
    {
        include('add_product.php');
    }
    else if($tag == 'delete_product')
    {
        include('delete_product.php');
    }
    else if($tag == 'get_price')
    {
        include('get_price.php');
    }
    else if($tag == 'tobuy')
    {
        include('tobuy_api.php');
    }
     else if($tag == 'payments')
    {
       include('payments.php');
    }
    else if($tag == 'map_tags')
    {
       include('map_tags.php');
    }
    else if($tag == 'preferences')
    {
	   include('preferences.php');
    }
    else if($tag == 'fridge')
    {
        include('fridgelist_api.php');
    }
    else if($tag == 'askforupdates')
    {
        include('askforupdates.php');
    }
    else if($tag == 'comp') // compare prices
    {
        include('comp.php');
    }
    else if($tag == 'getrecipe') // get a single recipe from yummly
    {
        include('get_recipe.php');
    }
    else
    {
        echo 'Invalid Request';
    }
}

/**
 *  eventuell sollte nach erfolgreichem Login eine Session-ID in der DB gespeichert werden..
 */
function checkUser($email, $password)
{
     $db_functions = new DB_Functions();
     $user = $db_functions->getUserByEmailAndPassword($email, $password);
     if($user == false) 
     {
        $response["error"] = 1;
        $response["error_msg"] = "Incorrect email or password!";
        echo json_encode($response);

        mysql_close();
        die();
     } 
}

// require_once 'include/DB_Functions.php';
// $db_functions = new DB_Functions();
// // store user
// $name = 'user';
// $email = 'mail@test.de';
// $password = 'password';
// $user = $db_functions->storeUser($name, $email, $password);
// if($user)
// {
// 	echo 'User created';
// }
// else 
// {
// 	echo 'Creation failed';
// }

// $res = $db_functions->getUserByEmailAndPassword('mail@test.de', 'password');
// if ($res)
// {
// 	var_dump($res);
// }
// else
// {
// 	echo 'User not found';
// }

?>


