<?php

// id was sent
if (!empty($_POST['id']))
{
	$shop_id = $_POST['id'];

	$shop = $db_functions->getShopById($shop_id);

	if ($shop != false) 
	{
	    $response["success"] = 1;
	    $response["shop"] = $shop;
	    echo $json = json_encode($response);
	}
	else
	{
	    $response["error"] = 1;
	    $response["error_msg"] = "shop with id " .$shop_id. " not found";
	    echo json_encode($response);
	}
}
// no id was sent
else
{
	$shops = $db_functions->getShops();

	if ($shops != false) 
	{
	    $response["success"] = 1;
	    $response["shops"] = $shops;
	    echo $json = json_encode($response);
	}
	else
	{
	    $response["error"] = 1;
	    $response["error_msg"] = "no shops found";
	    echo json_encode($response);
	}
}

?>