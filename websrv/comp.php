<?php

// TODO braucht kein Mensch mehr

/**
 * calculates distance between two points on a sphere via haversine formula
 * source: http://www.codecodex.com/wiki/Calculate_Distance_Between_Two_Points_on_a_Globe#PHP
 * 
 * @param double $latitude1 latitude of source point
 * @param double $longitude1 logitude of source point
 * @param double $latitude2 latitude of destination point
 * @param double $longitude2 longitude of destination point
 * @return double distance calculated distance
 */
function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
	$earth_radius = 6371;
	
	$dLat = deg2rad($latitude2 - $latitude1);
	$dLon = deg2rad($longitude2 - $longitude1);
	
	$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
	$c = 2 * asin(sqrt($a));
	$d = $earth_radius * $c;
	
	return $d;
}

/**
 * compares price of two products and inserts cheaper price into database
 * @param array $product1 product 1
 * @param array $product2 product 2
 * @param integer $uid user id
 * @param integer $plist_id product list id
 */
function comparePrices($product1, $product2, $uid, $plist_id){
	$id1 = $product1['id'];
	$id2 = $product2['id'];
	
	// get id of shops where products are
	$mysql_r8  = mysql_query( "SELECT `lat`, `lng` FROM `shops` WHERE `id` = ( SELECT `shop_id` FROM `products_in_shops` WHERE `product_collection_id` = $id1 )" ) or die(mysql_error());
	$mysql_r9  = mysql_query( "SELECT `lat`, `lng` FROM `shops` WHERE `id` = ( SELECT `shop_id` FROM `products_in_shops` WHERE `product_collection_id` = $id2 )" ) or die(mysql_error());
	$mysql_r10 = mysql_query( "SELECT `lat`, `lng` FROM `users` WHERE `id` = $uid" ) or die(mysql_error()); // home coordiantes
	$mysql_r11 = mysql_query( "SELECT `distance` FROM `preferences` WHERE `id` = $uid" ) or die(mysql_error()); // max distance
	
	$home = array();
	while($r12 = mysql_fetch_array($mysql_r10))
	{
		$home['lat'] = $r12['lat'];
		$home['lng'] = $r12['lng'];
	}
	
	while($r13 = mysql_fetch_array($mysql_r11)){
		$home['dis'] = $r13['distance'];
	}
	
	$shop_array = array();
	
	// get shop with current price
	while($r10 = mysql_fetch_array($mysql_r8)){
		$shop_array[0]['lat'] = $r10['lat'];
		$shop_array[0]['lng'] = $r10['lng'];
	}
	
	// get shop with cheaper price
	while($r11 = mysql_fetch_array($mysql_r9)){
		$shop_array[1]['lat'] = $r11['lat'];
		$shop_array[1]['lng'] = $r11['lng'];
	}
	 
	
	if( $home['dis'] <= getDistance($home['lat'], $home['lng'], $shop_array[1]['lat'], $shop_array[1]['lng']) ){
		$n_pid = $product2['id'];
		$n_price = $product2['price'];
		$n_pricetype = $product2['pricetype'];
		$n_name = $product2['product_name'];
		$n_ingredient = $product2['ingredient'];
		$n_category = $product2['category'];
	
		$old_id = $product1['id'];
	
		// insert new item with lower price
		$mysql_r3 = mysql_query("INSERT INTO `products` (productlist_id, product_name, price,      pricetype,      ingredient,      category,      created_at )
		VALUES  ('$plist_id',   '$n_name',     '$n_price', '$n_pricetype', '$n_ingredient', '$n_category', NOW())") or die(mysql_error());
	
		$n_id = mysql_insert_id();
	
		// get shop id from cheaper product and create new entry products_in_shops_table
		$mysql_r5 = mysql_query("SELECT `shop_id` FROM `products_in_shops` WHERE `product_id` = $n_pid") or die(mysql_error());
		$n_sid = mysql_fetch_array($mysql_r5);
		$n_sid = $n_sid['shop_id'];
		$mysql_r4 = mysql_query("INSERT INTO `products_in_shops`( `product_collection_id` , `shop_id` ) VALUES ( '$n_id', '$n_sid' )") or die(mysql_error());
	
		// delete old product
		$mysql_r6 = mysql_query("DELETE FROM `products` WHERE id = '$old_id'") or die(mysql_error());
	
		// update date/time in shoppinglist
		$mysql_r7 = mysql_query("UPDATE `shopping_lists` SET `updated_at`=NOW() WHERE `user_id` = $uid") or die(mysql_error());
	}	
}

if(isset($_POST['user']))
        {
           $uid = $_POST['user'];
           // get productlist_id of user
           $result = mysql_fetch_array(mysql_query("SELECT `id` FROM `product_lists` WHERE `shoppinglist_id` = ( SELECT `id` FROM `shopping_lists` WHERE `user_id` = $uid )")) or die(mysql_error());
           
           $plist_id = $result['id']; // product list of user
           
           // get products on users product list
           $result = mysql_query("SELECT `id` , `product_name`, ingredient, pricetype, category FROM `products` WHERE `productlist_id` = $plist_id ") or die(mysql_error());
           
           while($row = mysql_fetch_array($result)){
                $p_name = $row['product_name'];
                
                // compare price of one product
                
                // select product by name
                $res = mysql_query("SELECT `id` , `price` , `pricetype`, `productlist_id`, `product_name`, `ingredient`, `category` FROM `products` WHERE `product_name` = '$p_name'") or die(mysql_error());
            
                $i = 0;
                $p_arr = array();
                
                // put products in array
                while($r2 = mysql_fetch_array($res))
                {
                    $p_arr[$i]['id'] = $r2['id'];
                    //$p_arr[$i]['price'] = $r2['price'];
                   // $p_arr[$i]['pricetype'] = $r2['pricetype'];
                    $p_arr[$i]['productlist_id'] = $r2['productlist_id'];
                    $p_arr[$i]['product_name'] = $r2['product_name'];
                    $p_arr[$i]['ingredient'] = $r2['ingredient'];
                    $p_arr[$i]['category'] = $r2['category'];
                    
                    $i++;
                }

                $k = 0;
                $l = 0;
                $numberOfProducts = count($p_arr);
                
                while($k < $numberOfProducts){
                	while($l < $numberOfProducts){
                		if($k != $l){
                			if( ($p_arr[$k]['price'] != 0) && ($p_arr[$l]['price'] != 0)){
                				 
                				if( ($p_arr[$k]['price'] > $p_arr[$l]['price']) && ($p_arr[$l]['productlist_id'] != $plist_id) ){
                					
                					comparePrices($p_arr[$k], $p_arr[$l], $uid, $plist_id);
                			
                				} else if( ($p_arr[$k]['price'] < $p_arr[$l]['price']) && ($p_arr[$k]['productlist_id'] != $plist_id)){
                					
                					comparePrices($p_arr[$l], $p_arr[$k], $uid, $plist_id);
                				}
                			}
                		}
                		
                		$l++;
                	}
                	
                	$k++;
                }
           }
           
           $response["success"] = 1;
           echo json_encode($response);
        }
        else 
        {
            $response["error"] = 1;
            $response["error_msg"] = "no user id stated";
            echo json_encode($response);
        }

?>
