<?php
// mobile1.informatik.uni-ulm.de
// check that the tag is present
if (isset($_POST['tag']) && $_POST['tag'] != '') 
{
    $tag = $_POST['tag'];
    
    // Create a connection the the database and table
    $error = 'Could not connect to the database' . mysql_error();
    
    // Depending on your setup you might need to change the below for your own credentials
    $link = mysql_connect('localhost', 'root', 'k02+daRaIUZD') or die ($error);
    mysql_select_db('levazu') or die ($error);
    
    // check if tag = 'getShoppingList'
    if ($tag == 'getShoppingList') 
    {
        // Get id
        $shoppinglistid = mysql_real_escape_string($_POST['id']);

        // Select records from the database
        // $find = mysql_query("SELECT * FROM shopping_lists WHERE id='$shoppinglistid'");
        $find = mysql_query(
            "SELECT * FROM product_lists, products
            WHERE product_lists.id = products.productlist_id
            AND product_lists.shoppinglist_id = $shoppinglistid
            LIMIT 0 , 30");

        //check that records exist
        if (mysql_num_rows($find) > 0) 
        {
            $rows = array();
            while ($find_row = mysql_fetch_assoc($find))
            {
                // Get row
                $rows[] = $find_row;
                // $userid = $find_row['user_id'];
                // $productid = $find_row['id'];
                // $productname = $find_row['product_name'];
                // echo var_dump($find_row);
            
                // Return the response as a success, including userid.
                // $response["success"][] = 1;
                // $response["shoppinglist"]["name"][] = $shoppinglistid;
                // $response["shoppinglist"]["userid"][] = $userid;
                // $response["shoppinglist"]["productid"][] = $productid;
                // $response["shoppinglist"]["productname"][] = $productname;
            }
            echo json_encode($rows);
            // echo json_encode($response);
        }
        else 
        {
            //Return error
            $response["success"] = 0;
            $response["error"] = 1;
            $response["error_msg"] = "Entry with id: " . $shoppinglistid . " could not be found";
            echo json_encode($response);
         }
    } 
    else 
    {
        echo "No action for the tag";
    }
    
    mysql_close($link);

} 
else 
{
     echo "Unknown error";
     echo "<br />";
     echo var_dump($_POST);
}
?>