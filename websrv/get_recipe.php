<?php
$debug = true;
$enableYummlyRequest = true;

date_default_timezone_set('UTC');

// Functions
include_once(__DIR__ . "/EverYumRetroPublic/libs/ingredient_calc_functions.php");

$loader = require __DIR__ . '/EverYumRetroPublic/libs/autoload.php';

// Config
$config = include __DIR__ . '/EverYumRetroPublic/config.php';

// EverYum App
$app = new \EverYum\Application($config);


// if tag is empty
if (!isset($_POST['tag']) && $_POST['tag'] == '') 
	die("get_recipe.php died");


// code ...
echo "get some recipe!";

?>