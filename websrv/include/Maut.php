<?php

/**
 * This class is responsible for the MAUT computing.
 * (Multi Attribute Utility Theory)
 * 
 * @author vlad
 */

class Maut
{
	/**
     * @var array
     * @access protected
     */
	protected $mPreferences = array();

	/**
	 * Initializes the Maut class.
	 */
	public function __construct(array $pPreferences)
	{
		$this->mPreferences = $pPreferences;
	
		$this->init();
	}

	/**
	 * Initalizes stuff..
	 */
	protected function init()
	{
		echo "<table border='1'>";
		foreach ($this->mPreferences as $key => $value) 
		{
			echo "<tr>
					<td>".$key."</td>
				  	<td>".$value."</td>
				  </tr>";
		}
		echo "</table>";
	}
}

?>