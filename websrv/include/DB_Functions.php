<?php
 
class DB_Functions 
{
 
    private $db;
 
    // constructor
    function __construct() 
    {
        require_once 'DB_Connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->db->connect();
    }
 
    // destructor
    function __destruct() 
    {
        //$this->db->close();
    }

    /**
     * Storing new user
     * @param unknown $name
     * @param unknown $email
     * @param unknown $password
     * @return multitype:|boolean user details or false if failed
     */
    public function storeUser($name, $email, $password) 
    {
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
        
        $result = mysql_query(
            "INSERT INTO users(name, email, password, salt, created_at) 
            VALUES('$name', '$email', '$encrypted_password', '$salt', NOW())");

        // check for successful store
        if ($result) 
        {
            // get user details
            $id = mysql_insert_id(); // last inserted id
            $result = mysql_query("SELECT * FROM users WHERE id = $id");
            
            // return user details
            return mysql_fetch_array($result);
        } 
        else 
        {
            return false;
        }
    }
    
    /**
     * Get user by email and password
     * @param string $email
     * @param string $password
     * @return Ambigous <resource, multitype:>|boolean
     */
    public function getUserByEmailAndPassword($email, $password) 
    {
        $result = mysql_query("SELECT * FROM users WHERE email = '$email'") or die(mysql_error());
        
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            $result = mysql_fetch_array($result);
            $salt = $result['salt'];
            $encrypted_password = $result['password'];
            $hash = $this->checkhashSSHA($salt, $password);
            
            // check for password equality
            if ($encrypted_password == $hash) 
            {
                // user authentication details are correct
                return $result;
            }
        } 
        else 
        {
            // user not found
            return false;
        }
    }

    /**
     * 
     * @param unknown $shoppinglist_id
     * @return multitype:|boolean
     */
    public function getShoppingListById($shoppinglist_id)
    {
        $result = mysql_query(
            "SELECT * FROM product_lists, products
            WHERE product_lists.id = products.productlist_id
            AND product_lists.shoppinglist_id = $shoppinglist_id") or die(mysql_error());
        
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }

            return $rows;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * return shoppinglist of user
     * @param integer $user_id
     * @return shoppinglist as array or false if no shoppinglist
     */
    public function getShoppingListByUserWithoutTags($user_id)
    {
        $result = mysql_query(
            "SELECT DISTINCT pc.id AS product_collection_id, pl.shoppinglist_id, pl.id AS productlist_id, 
        		pc.product_name, pc.ingredient, pc.pricetype, pis.price, pis.shop_id, pc.category
			 FROM products AS p
			 INNER JOIN product_collection AS pc ON p.product_collection_id = pc.id
			 INNER JOIN product_lists pl ON p.productlist_id = pl.id
			 INNER JOIN products_in_shops AS pis ON pc.id = pis.product_collection_id
			 AND pl.shoppinglist_id = ( SELECT  `id` 
                                        FROM  `shopping_lists` 
                                        WHERE  `user_id` = '".$user_id."' )")
            or die(mysql_error());
        
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }

            //var_dump($rows);
            return $rows;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * returns shoppinglist of user only products that have rfid tags
     * @param integer $user_id
     * @return shoppinglist as array or false if no shoppinglist
     */
    public function getShoppingListByUser($user_id)
    {
    	$result = mysql_query(
    			"SELECT DISTINCT pc.id AS product_collection_id, pl.shoppinglist_id, pl.id AS productlist_id, 
    				pc.product_name, pc.ingredient, pc.pricetype, pis.price, pc.category, m.tag
			 FROM products AS p
			 INNER JOIN product_collection AS pc ON p.product_collection_id = pc.id
    		 INNER JOIN map_tags AS m ON m.product_collection_id = pc.id
			 INNER JOIN product_lists pl ON p.productlist_id = pl.id
			 INNER JOIN products_in_shops AS pis ON pc.id = pis.product_collection_id
			 AND pl.shoppinglist_id = ( SELECT  `id` 
                                        FROM  `shopping_lists` 
                                        WHERE  `user_id` = '".$user_id."' )") or die(mysql_error());
    
    	
    	  
    	// check for result
    	$num_of_rows = mysql_num_rows($result);
    	if ($num_of_rows > 0)
    	{
    		while ($row = mysql_fetch_assoc($result))
    		{
    			$rows[] = $row;
    		}
    
    		return $rows;
    	}
    	else
    	{
    		return false;
    	}
    }

    /**
     * 
     * @param number $pUserid
     * @return multitype:|boolean
     */
    public function getFridgeItems($pUserid = 4)
    {
        // the user_id is optional. if there is no user_id
        // passed, the default value will be used
        $result = mysql_query
        (
            "SELECT *
            FROM fridge_list, map_tags, product_collection
            WHERE fridge_list.tag = map_tags.tag 
            AND map_tags.product_collection_id = product_collection.id
            AND fridge_list.user_id = '$pUserid'"
        ) 
        OR die(mysql_error());

        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
                $rows[] = $row;

            return $rows;
        }
        else
            return false;
    }
    
    /**
     * 
     * @param unknown $user_id
     * @return multitype:|boolean
     */
     public function getBudgetByUserId($user_id)
     {
        $result = mysql_query("SELECT * FROM `budget` WHERE `uid` = $user_id") or die(mysql_error());
        
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }

            return $rows;
        }
        else
        {
            return false;
        }
     }
    
    /**
     * 
     * @param unknown $shop_id
     * @return multitype:|boolean
     */
    public function getShopById($shop_id)
    {
        $result = mysql_query(
            "SELECT *
            FROM shops
            WHERE id = $shop_id") or die(mysql_error());
        
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }

            return $rows;
        }
        else
        {
            return false;
        }
    }

    /**
     * returns all shops
     * @return multitype:|boolean array with shops, false if none found
     */
    public function getShops()
    {
        $result = mysql_query( "SELECT * FROM shops") or die(mysql_error());
        
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }

            return $rows;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * returns products in a shop 
     * @param int $shop_id id of shop
     * @return multitype:|boolean array of products or false if none found
     */
    public function getProductsByShopId($shop_id)
    {
        $result = mysql_query(
            "SELECT *
            FROM products_in_shops
            WHERE shop_id = $shop_id") or die(mysql_error());
        
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }

            return $rows;
        }
        else
        {
            return false;
        }
    }

    /**
     * 
     * @return multitype:|boolean
     */
    public function getProducts()
    {
        $result = mysql_query(
            "SELECT pis.product_collection_id, pis.shop_id, pc.product_name, pis.price FROM products_in_shops AS pis
             INNER JOIN product_collection AS pc ON pis.product_collection_id = pc.id") or die(mysql_error());
        
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }

            return $rows;
        }
        else
        {
            return false;
        }
    }

    /**
     * returns preferences of user
     * @param int $id user id
     * @return multitype:|boolean array with preferences or false if none found
     */
    public function getPreferencesById($id)
    {
        $result = mysql_query(
            "SELECT * 
            FROM  preferences
            WHERE  id =  $id") or die(mysql_error());
        // check for result
        $num_of_rows = mysql_num_rows($result);
        if ($num_of_rows > 0) 
        {
            while ($row = mysql_fetch_assoc($result))
            {
                $rows[] = $row;
            }

            return $rows;
        }
        else
        {
            return false;
        }
    }
    
    /**
     *  Save new Settings
     */
    //public functions saveSettings($preferences){
        
    //}
    
    /**
     * check if user exists or not
     * @param string $email
     * @return boolean true if user exists, false if not
     */
    public function isUserExisted($email) 
    {
        $result = mysql_query("SELECT email from users WHERE email = '$email'");
        
        $num_of_rows = mysql_num_rows($result);
        
        if ($num_of_rows > 0) 
        {
            // user existed
            return true;
        } 
        else 
        {
            // user not existed
            return false;
        }
    }
 
    /**
     *  Encrypting password
     *  @param password
     *  returns salt and encrypted password
     */
    public function hashSSHA($password) 
    {
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
    /**
     *  Decrypting password
     *  @param salt, password
     *  returns hash string
     */
    public function checkhashSSHA($salt, $password) 
    {
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
 
        return $hash;
    }
 
}
 
?>