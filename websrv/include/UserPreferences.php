<?php

/**
 * Represents the user preferences.
 */

class UserPreferences
{
	/**
	 * @var array
	 * @access protected
	 */
	protected $mPreferences;

	/**
	 * Initializes the UserPreferences Class.
	 */
	public function __construct(array $pPreferences)
	{
		foreach ($pPreferences as $key => $value)
			$this->mPreferences[$key] = $value;
	}



}
?>