-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 17. Mrz 2014 um 16:28
-- Server Version: 5.5.35
-- PHP-Version: 5.4.4-14+deb7u8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `levazu`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `budget`
--

/**
 *
 */
CREATE TABLE IF NOT EXISTS `budget` (
  `uid` int(11) NOT NULL,
  `groceries` int(11) NOT NULL,
  `gVegetables` int(11) NOT NULL,
  `gFruit` int(11) NOT NULL,
  `gMeat` int(11) NOT NULL,
  `gDairy` int(11) NOT NULL,
  `gBread` int(11) NOT NULL,
  `gPasta` int(11) NOT NULL,
  `gBeverages` int(11) NOT NULL,
  `gSpirits` int(11) NOT NULL,
  `gSpice` int(11) NOT NULL,
  `gOther` int(11) NOT NULL,
  `hyginie` int(11) NOT NULL,
  `housekeeping` int(11) NOT NULL,
  `stationery` int(11) NOT NULL,
  `shoes` int(11) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `budget`
--

INSERT INTO `budget` (`uid`, `groceries`, `gVegetables`, `gFruit`, `gMeat`, `gDairy`, `gBread`, `gPasta`, `gBeverages`, `gSpirits`, `gSpice`, `gOther`, `hyginie`, `housekeeping`, `stationery`, `shoes`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 100, 100, 100),
(4, 68, 4, 6, 6, 7, 4, 6, 6, 9, 6, 7, 100, 100, 100, 100),
(5, 576, 57, 31, 65, 62, 40, 117, 45, 62, 42, 55, 20, 20, 15, 40),
(6, 133, 37, 31, 10, 15, 20, 0, 10, 10, 0, 0, 80, 25, 20, 100);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fridge_list`
--

CREATE TABLE IF NOT EXISTS `fridge_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tag` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=630 ;

--
-- Daten für Tabelle `fridge_list`
--

INSERT INTO `fridge_list` (`id`, `user_id`, `tag`, `created_at`, `updated_at`) VALUES
(612, 4, '0900764A4C', '2014-02-25 16:00:52', '2014-02-25 16:00:52'),
(613, 4, '09006DABCC', '2014-02-25 16:00:52', '2014-02-25 16:00:52'),
(616, 4, '09008EA4BE', '2014-02-25 16:00:52', '2014-02-25 16:00:52'),
(623, 4, '0A008FB02B', '2014-02-25 16:20:00', '2014-02-25 16:20:00'),
(624, 4, '09006DBEB2', '2014-02-25 16:20:00', '2014-02-25 16:20:00'),
(625, 4, '0A00917417', '2014-02-25 16:20:00', '2014-02-25 16:20:00'),
(626, 4, '0B003F76CC', '2014-02-25 16:20:00', '2014-02-25 16:20:00'),
(627, 4, '0109B2019C', '2014-02-25 16:20:00', '2014-02-25 16:20:00'),
(628, 4, '0109B24F21', '2014-02-25 16:20:00', '2014-02-25 16:20:00'),
(629, 4, '0000A5FB58', '2014-02-25 16:20:00', '2014-02-25 16:20:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `fridge_list_removed`
--

CREATE TABLE IF NOT EXISTS `fridge_list_removed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `map_tags`
--

CREATE TABLE IF NOT EXISTS `map_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_collection_id` int(10) NOT NULL,
  `tag` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=75 ;

--
-- Daten für Tabelle `map_tags`
--

INSERT INTO `map_tags` (`id`, `product_collection_id`, `tag`) VALUES
(1, 1, '0000A5FB58'),
(2, 2, '0109B2019C'),
(3, 20, '09006DBEB2'),
(4, 4, '09006DABCC'),
(5, 5, '0900764A4C'),
(6, 6, '09008EA4BE'),
(7, 25, '0A008FB02B'),
(9, 11, '0A00917411'),
(10, 10, '#C'),
(12, 24, '0B003F76CC'),
(13, 14, '0109B24F21'),
(14, 12, '#B'),
(15, 15, '#P'),
(16, 21, '#D'),
(17, 22, '#E'),
(18, 23, '#F'),
(19, 13, '#G'),
(20, 26, '#26'),
(21, 27, '#27'),
(22, 28, '#28'),
(23, 29, '#29'),
(24, 30, '#30'),
(25, 31, '#31'),
(26, 32, '#32'),
(27, 33, '#33'),
(28, 34, '#34'),
(29, 35, '#35'),
(30, 36, '#36'),
(31, 37, '#37'),
(32, 38, '#38'),
(33, 39, '#39'),
(34, 40, '#40'),
(35, 41, '#41'),
(36, 42, '#42'),
(37, 43, '#43'),
(38, 44, '#44'),
(39, 45, '#45'),
(40, 46, '#46'),
(41, 47, '#47'),
(42, 48, '#48'),
(43, 49, '#49'),
(44, 50, '#50'),
(45, 51, '#51'),
(46, 52, '#52'),
(47, 53, '#53'),
(48, 54, '#54'),
(49, 55, '#55'),
(50, 56, '#56'),
(51, 57, '#57'),
(52, 58, '#58'),
(53, 59, '#59'),
(54, 60, '#60'),
(55, 61, '#61'),
(56, 62, '#62'),
(57, 63, '#63'),
(58, 64, '#64'),
(59, 65, '#65'),
(60, 66, '#66'),
(61, 67, '#67'),
(62, 68, '#68'),
(63, 69, '#69'),
(64, 70, '#70'),
(65, 71, '#71'),
(66, 72, '#72'),
(67, 73, '#73'),
(68, 74, '#74'),
(69, 75, '#75'),
(70, 76, '#76'),
(71, 77, '#77'),
(72, 78, '#78'),
(73, 79, '#79'),
(74, 80, '#80');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shoppinglist_id` int(11) NOT NULL,
  `productlist_id` int(11) NOT NULL,
  `product_collection_id` int(10) NOT NULL,
  `amount` int(10) NOT NULL,
  `price` double NOT NULL,
  `bought_when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=140 ;

--
-- Daten für Tabelle `payments`
--

INSERT INTO `payments` (`id`, `shoppinglist_id`, `productlist_id`, `product_collection_id`, `amount`, `price`, `bought_when`, `created_at`, `updated_at`) VALUES
(130, 6, 7, 1, 1, 1.2, '2014-02-25 15:17:20', '2014-02-25 16:17:20', '2014-02-25 16:17:20'),
(131, 6, 7, 2, 1, 0.2, '2014-02-25 15:17:20', '2014-02-25 16:17:20', '2014-02-25 16:17:20'),
(132, 6, 7, 11, 1, 1, '2014-02-25 15:17:20', '2014-02-25 16:17:20', '2014-02-25 16:17:20'),
(133, 6, 7, 14, 1, 0.5, '2014-02-25 15:17:20', '2014-02-25 16:17:20', '2014-02-25 16:17:20'),
(134, 6, 7, 10, 1, 0.25, '2014-02-25 15:17:20', '2014-02-25 16:17:20', '2014-02-25 16:17:20'),
(135, 6, 7, 20, 1, 2, '2014-02-25 15:17:20', '2014-02-25 16:17:20', '2014-02-25 16:17:20'),
(136, 6, 7, 13, 1, 1.5, '2014-02-25 15:17:20', '2014-02-25 16:17:20', '2014-02-25 16:17:20'),
(137, 6, 7, 12, 1, 1.5, '2014-02-25 15:17:20', '2014-02-25 16:17:20', '2014-02-25 16:17:20'),
(138, 6, 7, 25, 1, 5.99, '2014-02-25 15:18:47', '2014-02-25 16:18:47', '2014-02-25 16:18:47'),
(139, 6, 7, 24, 1, 2, '2014-02-25 15:18:47', '2014-02-25 16:18:47', '2014-02-25 16:18:47');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `poll_recipes`
--

CREATE TABLE IF NOT EXISTS `poll_recipes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `preferences`
--

CREATE TABLE IF NOT EXISTS `preferences` (
  `id` int(11) NOT NULL,
  `nutritionstyle` int(11) NOT NULL,
  `distance` double NOT NULL,
  `calories` int(11) NOT NULL,
  `notifications` int(11) NOT NULL,
  `notificationRate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `preferences`
--

INSERT INTO `preferences` (`id`, `nutritionstyle`, `distance`, `calories`, `notifications`, `notificationRate`) VALUES
(2, 2, 114, 3064, 11, 21),
(4, 0, 93, 3064, 111, 1),
(5, 0, 15, 3064, 0, 0),
(6, 0, 20, 3064, 0, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_collection_id` int(10) NOT NULL,
  `productlist_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=804 ;

--
-- Daten für Tabelle `products`
--

INSERT INTO `products` (`id`, `product_collection_id`, `productlist_id`, `created_at`, `updated_at`) VALUES
(18, 8, 3, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(19, 11, 3, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(20, 7, 3, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(23, 12, 3, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(25, 13, 4, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(26, 14, 4, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(27, 8, 4, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(28, 11, 4, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(29, 7, 4, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(30, 4, 4, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(32, 12, 4, '2014-01-17 21:18:28', '0000-00-00 00:00:00'),
(55, 13, 3, '2014-02-06 18:11:39', '0000-00-00 00:00:00'),
(56, 14, 4, '2014-02-11 17:20:15', '0000-00-00 00:00:00'),
(60, 15, 3, '2014-02-12 16:55:40', '0000-00-00 00:00:00'),
(131, 4, 3, '2014-02-19 14:46:50', '0000-00-00 00:00:00'),
(249, 1, 2, '2014-02-22 21:36:43', '2014-02-22 21:36:43'),
(366, 2, 2, '2014-02-24 19:33:25', '2014-02-24 19:33:25'),
(497, 25, 2, '2014-02-25 08:28:15', '2014-02-25 08:28:15'),
(498, 14, 2, '2014-02-25 08:28:16', '2014-02-25 08:28:16'),
(500, 20, 2, '2014-02-25 08:28:18', '2014-02-25 08:28:18'),
(502, 12, 2, '2014-02-25 08:28:19', '2014-02-25 08:28:19'),
(646, 49, 2, '2014-02-25 12:58:20', '2014-02-25 12:58:20'),
(651, 46, 2, '2014-02-25 12:59:01', '2014-02-25 12:59:01'),
(653, 47, 2, '2014-02-25 12:59:02', '2014-02-25 12:59:02'),
(656, 48, 2, '2014-02-25 12:59:04', '2014-02-25 12:59:04'),
(658, 32, 2, '2014-02-25 12:59:07', '2014-02-25 12:59:07'),
(659, 51, 2, '2014-02-25 12:59:07', '2014-02-25 12:59:07'),
(661, 53, 2, '2014-02-25 12:59:08', '2014-02-25 12:59:08'),
(680, 41, 2, '2014-02-25 14:16:47', '2014-02-25 14:16:47'),
(681, 42, 2, '2014-02-25 14:16:48', '2014-02-25 14:16:48'),
(682, 43, 2, '2014-02-25 14:16:50', '2014-02-25 14:16:50'),
(683, 44, 2, '2014-02-25 14:16:50', '2014-02-25 14:16:50'),
(684, 45, 2, '2014-02-25 14:16:52', '2014-02-25 14:16:52'),
(685, 65, 2, '2014-02-25 14:16:53', '2014-02-25 14:16:53'),
(797, 3, 7, '2014-02-27 21:08:13', '2014-02-27 21:08:13'),
(798, 4, 7, '2014-02-28 18:04:27', '2014-02-28 18:04:27'),
(799, 34, 7, '2014-02-28 18:04:27', '2014-02-28 18:04:27'),
(800, 77, 7, '2014-02-28 18:04:28', '2014-02-28 18:04:28');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `products_in_shops`
--

CREATE TABLE IF NOT EXISTS `products_in_shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_collection_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=84 ;

--
-- Daten für Tabelle `products_in_shops`
--

INSERT INTO `products_in_shops` (`id`, `product_collection_id`, `shop_id`, `price`) VALUES
(37, 12, 3, 1.69),
(38, 4, 3, 0.35),
(39, 15, 3, 4.5),
(40, 13, 4, 2),
(41, 11, 4, 1.22),
(42, 7, 4, 0.4),
(43, 14, 6, 3),
(44, 8, 6, 0.1),
(45, 12, 9, 1.7),
(46, 4, 9, 4),
(47, 15, 9, 4.5),
(48, 13, 12, 2),
(49, 11, 12, 1.22),
(50, 7, 12, 0.4),
(51, 14, 11, 3),
(52, 8, 11, 0.1),
(53, 12, 6, 1.6),
(54, 12, 4, 1.65),
(55, 1, 5, 1.2),
(56, 2, 5, 0.2),
(57, 3, 5, 2.2),
(59, 5, 5, 0.15),
(60, 6, 5, 3.15),
(61, 8, 5, 0.15),
(62, 10, 5, 0.25),
(63, 11, 5, 1),
(64, 12, 5, 1.5),
(65, 13, 5, 1.5),
(66, 14, 5, 0.5),
(67, 15, 5, 0.2),
(68, 18, 5, 1.2),
(69, 19, 5, 1),
(70, 20, 5, 2),
(71, 21, 5, 0.55),
(72, 22, 5, 0.55),
(73, 23, 5, 0.55),
(74, 7, 5, 2.1),
(75, 24, 5, 2.5),
(76, 1, 9, 1.3),
(77, 2, 9, 0.6),
(78, 24, 9, 15),
(79, 25, 5, 12.99),
(80, 24, 8, 2),
(82, 11, 9, 1.25),
(83, 25, 8, 5.99);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_collection`
--

CREATE TABLE IF NOT EXISTS `product_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ingredient` int(1) NOT NULL,
  `pricetype` int(1) NOT NULL,
  `price` double NOT NULL,
  `category` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Daten für Tabelle `product_collection`
--

INSERT INTO `product_collection` (`id`, `product_name`, `ingredient`, `pricetype`, `price`, `category`) VALUES
(1, 'spaghetti', 1, 1, 3, 'gPasta'),
(2, 'tomato', 1, 2, 5, 'gVegetables'),
(3, 'ground sirloin', 1, 2, 2, 'gMeat'),
(4, 'salt', 1, 2, 4, 'gSpice'),
(5, 'shallots', 1, 0, 0, 'gVegetables'),
(6, 'coffee', 1, 3, 6, 'gOther'),
(7, 'mayonnaise', 1, 2, 5, 'gOther'),
(8, 'onion ', 1, 1, 1, 'gVegetables'),
(10, 'fresh oregano', 1, 2, 4, 'gSpice'),
(11, 'eggs', 1, 1, 0, 'gOther'),
(12, 'angel hair pasta', 1, 1, 0, 'gBread'),
(13, 'garlic cloves', 1, 2, 0, 'gMeat'),
(14, 'red chili peppers', 1, 1, 0, 'gVegetables'),
(15, 'pepper', 1, 2, 0, 'gSpice'),
(18, 'italian spici sausag', 1, 1, 0, 'gMeat'),
(19, 'coarse salt', 1, 1, 0, 'gSpice'),
(20, 'olive oil', 1, 1, 0, 'gOther'),
(21, 'peeled *tomato*es', 1, 1, 0, 'gVegetables'),
(22, 'yellow *onion*', 1, 1, 0, 'gVegetables'),
(23, 'ground pepper', 1, 1, 0, 'gSpice'),
(24, 'red wine', 1, 1, 0, 'gSpirits'),
(25, 'vodka', 1, 1, 0, 'gSpirits'),
(26, 'gnocchi', 1, 1, 5, 'gVegetables'),
(27, 'grated parmesan cheese', 1, 1, 5, 'gVegetables'),
(28, 'red pepper flakes', 1, 1, 5, 'gVegetables'),
(29, 'mozzarella', 1, 1, 5, 'gVegetables'),
(30, 'freshly ground pepper', 1, 1, 5, 'gVegetables'),
(31, 'diced tomatoes', 1, 1, 5, 'gVegetables'),
(32, 'oregano', 1, 1, 5, 'gVegetables'),
(33, 'cherry tomatoes', 1, 1, 5, 'gVegetables'),
(34, 'salami', 1, 1, 5, 'gVegetables'),
(35, 'sun-dried tomatoes', 1, 1, 5, 'gVegetables'),
(36, 'fresh basil', 1, 1, 5, 'gVegetables'),
(37, 'penne', 1, 1, 5, 'gVegetables'),
(38, 'fresh mint', 1, 1, 5, 'gVegetables'),
(39, 'piment despelett', 1, 1, 5, 'gVegetables'),
(40, 'peeled tomatoes', 1, 1, 5, 'gVegetables'),
(41, 'shrimp paste', 1, 1, 5, 'gVegetables'),
(42, 'kabocha squash', 1, 1, 5, 'gVegetables'),
(43, 'okra', 1, 1, 5, 'gVegetables'),
(44, 'pork belly', 1, 1, 5, 'gVegetables'),
(45, 'anaheim chile', 1, 1, 5, 'gVegetables'),
(46, 'grated parmesan cheese', 1, 1, 5, 'gVegetables'),
(47, 'red pepper flakes', 1, 1, 5, 'gVegetables'),
(48, 'mozzarella', 1, 1, 5, 'gVegetables'),
(49, 'freshly ground pepper', 1, 1, 5, 'gVegetables'),
(50, 'diced tomatoes', 1, 1, 5, 'gVegetables'),
(51, 'oregano', 1, 1, 5, 'gVegetables'),
(52, 'cherry tomatoes', 1, 1, 5, 'gVegetables'),
(53, 'salami', 1, 1, 5, 'gVegetables'),
(54, 'sun-dried tomatoes', 1, 1, 5, 'gVegetables'),
(55, 'fresh basil', 1, 1, 5, 'gVegetables'),
(56, 'penne', 1, 1, 5, 'gVegetables'),
(57, 'fresh mint', 1, 1, 5, 'gVegetables'),
(58, 'piment despelett', 1, 1, 5, 'gVegetables'),
(59, 'peeled tomatoes', 1, 1, 5, 'gVegetables'),
(60, 'shrimp paste', 1, 1, 5, 'gVegetables'),
(61, 'kabocha squash', 1, 1, 5, 'gVegetables'),
(62, 'okra', 1, 1, 5, 'gVegetables'),
(63, 'pork belly', 1, 1, 5, 'gVegetables'),
(64, 'anaheim chile', 1, 1, 5, 'gVegetables'),
(65, 'japanese eggplants', 1, 1, 5, 'gVegetables'),
(66, 'chives', 1, 1, 5, 'gVegetables'),
(67, 'juice', 1, 1, 5, 'gVegetables'),
(68, 'fresh lemon juice', 1, 1, 5, 'gVegetables'),
(69, 'avocado', 1, 1, 5, 'gVegetables'),
(70, 'soy sauce', 1, 1, 5, 'gVegetables'),
(71, 'cayenne pepper', 1, 1, 5, 'gVegetables'),
(72, 'mushroom caps', 1, 1, 5, 'gVegetables'),
(73, 'heavy cream', 1, 1, 5, 'gVegetables'),
(74, 'fresh tomatoes', 1, 1, 5, 'gVegetables'),
(75, 'champagne vinegar', 1, 1, 5, 'gVegetables'),
(76, 'dressing', 1, 1, 5, 'gVegetables'),
(77, 'unsalted butter', 1, 1, 5, 'gVegetables'),
(78, 'greens', 1, 1, 5, 'gVegetables'),
(79, 'large shrimp', 1, 1, 5, 'gVegetables'),
(80, 'extra-virgin olive oil', 1, 1, 5, 'gVegetables');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product_lists`
--

CREATE TABLE IF NOT EXISTS `product_lists` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `shoppinglist_id` int(10) NOT NULL,
  `list_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `processed_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Daten für Tabelle `product_lists`
--

INSERT INTO `product_lists` (`id`, `shoppinglist_id`, `list_name`, `created_at`, `updated_at`, `processed_at`) VALUES
(1, 1, 'produkt-liste 1', '2013-11-06 15:43:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 'produkt-liste 2', '2013-11-06 15:43:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 'Hot Turkey Salad Sandwiches recipe', '2014-01-15 00:00:00', '2014-01-15 00:00:00', '0000-00-00 00:00:00'),
(4, 3, 'Hot Turkey Salad Sandwiches recipe', '2014-01-16 00:00:00', '2014-01-16 00:00:00', '0000-00-00 00:00:00'),
(5, 6, 'spaghetti bolognese products list', '2014-01-16 14:12:45', '2014-01-16 14:12:45', '2014-02-22 20:27:42'),
(7, 6, '', '2014-02-22 20:27:42', '2014-02-22 20:27:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shopping_lists`
--

CREATE TABLE IF NOT EXISTS `shopping_lists` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `list_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `shopping_lists`
--

INSERT INTO `shopping_lists` (`id`, `user_id`, `list_name`, `created_at`, `updated_at`) VALUES
(1, 2, 'shopping-liste 1', '2013-11-11 16:14:06', '0000-00-00 00:00:00'),
(2, 5, 'shop-user list', '2014-01-15 00:00:00', '2014-02-19 14:46:50'),
(3, 6, 'ulm shopping list', '2014-01-16 00:00:00', '2014-02-12 18:38:43'),
(5, 7, 'hot turkey shopping list', '2014-01-24 14:40:36', '2014-01-24 14:40:36'),
(6, 4, 'user 4 mail@test shoppinglist', '2014-01-26 14:11:21', '2014-01-26 14:11:21');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shops`
--

CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Daten für Tabelle `shops`
--

INSERT INTO `shops` (`id`, `name`, `lat`, `lng`) VALUES
(1, 'Uni Ulm', 48.422681, 9.956735),
(3, 'Mensa', 48.421974, 9.955447),
(4, 'Cafete Nord', 48.423345, 9.954229),
(5, 'Rewe', 48.399698, 9.99119),
(6, 'N27', 48.423569, 9.956981),
(7, 'See', 48.423338, 9.952599),
(8, 'Bahnhof', 48.39895, 9.983921),
(10, 'Shop 2', 48.399815, 9.987596),
(11, 'Shop 3', 48.398177, 9.987097),
(12, 'Shop 4', 48.39739, 9.99171),
(13, 'Cafete', 48.422223, 9.955817);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `password`, `salt`, `lat`, `lng`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'hagbart', 'koch', 'vladoogle@googlemail.com', 'e10ac44ef11ec259bfdcb024e8ee55d823b1157f', '', 0, 0, '2013-11-11 17:57:19', '0000-00-00 00:00:00'),
(2, 'Testuser', '', '', 'test@test.de', 'uabXEUyMmiG+MV654kag+uhqoF8yMDZiMGEwMDU1', '206b0a0055', 0, 0, '2013-11-12 10:49:05', '0000-00-00 00:00:00'),
(4, 'user', '', '', 'mail@test.de', 'uabXEUyMmiG+MV654kag+uhqoF8yMDZiMGEwMDU1', '206b0a0055', 48.396674, 9.98847, '2013-12-02 14:14:08', '0000-00-00 00:00:00'),
(5, 'shop', '', '', 'shop@test.de', 'uabXEUyMmiG+MV654kag+uhqoF8yMDZiMGEwMDU1', '206b0a0055', 48.419432, 9.943696, '2013-12-02 14:14:08', '2014-01-15 00:00:00'),
(6, 'ulm', '', '', 'ulm@test.de', 'uabXEUyMmiG+MV654kag+uhqoF8yMDZiMGEwMDU1', '206b0a0055', 48.396674, 9.98847, '2013-12-02 14:14:08', '2014-01-16 00:00:00'),
(7, 'pref', '', '', 'pref@test.de', 'uabXEUyMmiG+MV654kag+uhqoF8yMDZiMGEwMDU1', '206b0a0055', 0, 0, '2013-12-02 14:14:08', '2014-01-16 00:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_product_history`
--

CREATE TABLE IF NOT EXISTS `user_product_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `product_collection_id` int(11) NOT NULL,
  `counter` int(11) NOT NULL,
  `lasttime_used` datetime NOT NULL,
  `deleted_by_user_on` datetime NOT NULL,
  `lasttime_added_to_shoppinglist` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Daten für Tabelle `user_product_history`
--

INSERT INTO `user_product_history` (`id`, `uid`, `product_collection_id`, `counter`, `lasttime_used`, `deleted_by_user_on`, `lasttime_added_to_shoppinglist`) VALUES
(1, 4, 4, 22, '2014-02-25 14:34:37', '2014-02-25 13:54:10', '2014-02-28 18:04:27'),
(2, 4, 5, 4, '2014-02-25 14:34:37', '2014-02-24 18:22:45', '2014-02-24 18:22:22'),
(3, 4, 1, 13, '2014-02-22 16:01:51', '2014-02-22 16:01:51', '2014-02-25 16:03:59'),
(4, 4, 2, 17, '2014-02-22 16:01:51', '2014-02-22 16:01:51', '2014-02-25 16:03:59'),
(5, 4, 3, 10, '2014-02-16 00:00:00', '2014-02-24 18:59:40', '2014-02-27 21:08:13'),
(6, 2, 3, 3, '2014-02-16 00:00:00', '2014-02-20 12:57:13', '0000-00-00 00:00:00'),
(7, 2, 2, 3, '2014-02-16 00:00:00', '2014-02-20 13:25:22', '2014-02-24 19:33:25'),
(8, 2, 5, 3, '2014-02-16 00:00:00', '2014-02-24 19:44:16', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
