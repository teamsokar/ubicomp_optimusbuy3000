<?php

// id was sent
if (!empty($_POST['id']))
{
	$shop_id = $_POST['id'];

	$products = $db_functions->getProductsByShopId($shop_id);

	if ($products != false) 
	{
	    $response["success"] = 1;
	    $response["products"] = $products;
	    echo $json = json_encode($response);
	}
	else
	{
	    $response["error"] = 1;
	    $response["error_msg"] = "shop with id " .$shop_id. " not found";
	    echo json_encode($response);
	}
}
// no id was sent
else
{
	$products = $db_functions->getProducts();

	if ($products != false) 
	{
	    $response["success"] = 1;
	    $response["products"] = $products;
	    echo $json = json_encode($response);
	}
	else
	{
	    $response["error"] = 1;
	    $response["error_msg"] = "no shops found";
	    echo json_encode($response);
	}
}

?>