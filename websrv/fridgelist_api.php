<?php
	if (empty($_POST)) 
		die("fridgelist_api.php: no post vars");

	// PUT TAGS INTO DB
	if(isset($_POST['action']) && $_POST['action'] == 'sendallrfids')
	{
		$user_id = $_POST['userid'];
		$json_string = $_POST['rfidmarkers'];
		$arr_tags = explode(",",$json_string); // tags from the request
		
		// check for rfidmarkers
		if (isset($_POST['rfidmarkers']) && $_POST['rfidmarkers'] != "")
		{
			$removedTags = getRemovedTags($arr_tags);
			$deletedTags = removeTagsInDB($removedTags);

			foreach ($arr_tags as $tag) 
			{
				// INSERT IGNORE INTO throws a warning when there are duplicates, 
				// but besides from that just ignore to insert the duplicate into the database.
				// This IGNORE has to be used in combination with a UNIQUE key.
				$query = mysql_query(
					"INSERT IGNORE INTO levazu.fridge_list (user_id, tag, created_at, updated_at) 
					VALUES ('$user_id', '$tag', NOW(), NOW())") 
					or die(mysql_error());
			}

			$response["success"] = 1;
			$response["tags"] = $arr_tags;
			if (!empty($deletedTags))
				$response["deleted_tags"] = $deletedTags; 

			echo json_encode($response);
		}
		else
		{
			$response["error"] = 1;
			$response["error_msg"] = "rfidmarkers is empty";
			echo json_encode($response);
		}
	}

	// RECEIVE TAGS FROM DB
	else
	{
		$user_id = 4; // default user value
		if (isset($_POST['userid']))
			$user_id = $_POST['userid'];

		$arr_fridgelist = $db_functions->getFridgeItems($user_id);

		if($arr_fridgelist != false)
		{
			$response["success"] = 1;
			$response["fridgelist"] = $arr_fridgelist;
			echo json_encode($response);
		}
		else 
		{
			$response["error"] = 1;
			$response["error_msg"] = "there are no items in the fridge for user " . $user_id;
			$response["fridgelist"] = $arr_fridgelist;
			echo json_encode($response);
		}
	}


	// Compares the tags in our db with the tags from the request
	// and returns those which are not in both arrays.
	// You can turn on $debug, to see what happens.
 	function getRemovedTags($pTags)
	{
		$debug = true;
		$tagsInDB = array();

		// select tags currently in our db
		$result = mysql_query("SELECT tag FROM fridge_list");
		while($row = mysql_fetch_array($result))
		{
			$tagsInDB[] = $row;
		} 

		if ($debug) echo "In DB: <br />";
		foreach ($tagsInDB as $key_db => $value) 
		{
			if ($debug) echo $key_db . " -> " . $value["tag"];
			$tagInDB = $value["tag"];
			foreach ($pTags as $key => $value) 
			{
				if ($value == $tagInDB)
				{
					if ($debug) echo "*"; // mark tags which matches
					unset($tagsInDB[$key_db]); // delete matched tags
				}
			}
			if ($debug) echo "<br />";
		}

		if ($debug) echo "<p></p>";
		if ($debug) echo "From Request: <br />";
		foreach ($pTags as $key => $value) 
		{
			if ($debug) echo $key . " -> " . $value . "<br />";
		}

		if ($debug) echo "<p></p>";
		if ($debug) echo "After (tags to be deleted): <br />";
		foreach ($tagsInDB as $key => $value) 
		{
			if ($debug) echo $key . " -> " . $value["tag"] . "<br />";
		}

		return $tagsInDB;
	}

	// Removes the given tags in our fridge_list table
	function removeTagsInDB($pTags)
	{
		if (!empty($pTags))
		{
			// OLD QUERY
			/*
			foreach ($pTags as $key => $value) 
			{
				$query = "DELETE FROM fridge_list WHERE tag = '".$value['tag']."'";
				mysql_query($query);
			}
			*/

			// DELETE
			// build condition string for sql query
			$condition = "WHERE tag IN (";
			foreach ($pTags as $key => $value) 
			{
				$deletedTags[] = $value['tag'];
				$condition .= "'".$value['tag']."', ";
			}
			$condition = rtrim($condition, ", ");
			$condition .= ")";
			
			mysql_query("DELETE FROM fridge_list " . $condition);

			
			// UPDATE user_product_history 
			// where (userid, product_collection_id)
			$user_id = 4; // the fridge for user 4 (mail@test.de)

			// get product_collection_id by for each tag
			foreach ($deletedTags as $key => $value) 
			{
				$result = mysql_query(
				"SELECT map_tags.product_collection_id
				FROM map_tags 
				WHERE map_tags.tag ='$value'") or die(mysql_error());

				$row = mysql_fetch_row($result);
				$product_collection_id = $row[0];

				// update lasttime_used
				$update = mysql_query(
					"UPDATE `user_product_history` 
					SET lasttime_used = NOW() 
					WHERE `uid`='$user_id' AND `product_collection_id`='$product_collection_id'"); 
			}

			
			return $deletedTags;
		}
	}
?>