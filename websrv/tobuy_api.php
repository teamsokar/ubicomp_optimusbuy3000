<?php
$debug = false;
$enableYummlyRequest = true;

date_default_timezone_set('UTC');

include_once(__DIR__ . "/EverYumRetroPublic/libs/ingredient_calc_functions.php");

// MAUT Class
include_once __DIR__ . '/include/Maut.php';
include_once __DIR__ . '/include/UserPreferences.php';

$loader = require __DIR__ . '/EverYumRetroPublic/libs/autoload.php';

// Config
$config = include __DIR__ . '/EverYumRetroPublic/config.php';


// EverYum App
$app = new \EverYum\Application($config);


if (!isset($_POST['tag']) && $_POST['tag'] == '') 
	die();

$nutritionstyle = "";

// if user-id was sent, consider preferences of the user
if (isset($_POST['userid']))
{
	$preferences = $db_functions->getPreferencesById($_POST['userid']);

	// Maut 
	//$maut = new Maut($preferences[0]);
	
	if ($debug) {
		foreach ($preferences[0] as $key => $value) 
			echo $key . " = " . $value . "<br />";
	}
	
	$index = $preferences[0]['nutritionstyle'];

	// mapping for nutritionstyles
	$arr_nutritionstyle[0] = "course^course-Main Dishes";
	$arr_nutritionstyle[1] = "387^Lacto-ovo vegetarian";
	$arr_nutritionstyle[2] = "386^Vegan";
	$arr_nutritionstyle[3] = "";
	$arr_nutritionstyle[4] = "";

	$nutritionstyle = $arr_nutritionstyle[$index];
}


// $course = array('course^course-Main Dishes');
// $diet = array('386^Vegan');
$course = array();
$diet = array($nutritionstyle);

//replace with EverNote results
// $grocery_list = "salt";
$grocery_list = str_replace(",", ", ", $_POST['grocery_list']);
$fridge_list = $_POST['fridge_list'];
//$fridge_list = implode(",", $ingredients);
//$ingredients = array('tomato', 'salt', 'spaghetti', 'red wine');
//$fridge_list = implode(",", $ingredients);
$ingredients = explode(",",$_POST['fridge_list'].",".$_POST['grocery_list']);

$arr_my_items = return_have_items_array($fridge_list, $grocery_list);
$my_items_count = count($arr_my_items);
?>


<?php
if ($debug) 
{
	print_r($ingredients);

	echo 'in my fridge / on my grocery list:';
	echo '<p></p>';

	//build yumly URL
	for($i=0; $i<$my_items_count; $i++)
	{
		$item = $arr_my_items[$i];
		echo $item . "<br>";
	}
	reset($arr_my_items);

	echo '<p>';
	echo 'What to buy in order to make...';
	echo '</p>';
}
?>

<?php
if (!$enableYummlyRequest) die("<p>no request to yummly needed right now..</p>");
//replace filename with json data variable
//$arr_recipes = return_recipe_array("../libs/yumly_jsonp.json");

//this is an object...
//$test_recipe =  $app->service['yummly']->getRecipe("Vegan-Bulgur-Chili-Naturally-Ella-46337");
//echo "test recipe = $test_recipe <br>";

$arr_recipes = $app->service['yummly']->getRecipesByIngredients($ingredients, $course, $diet);

//foreach($arr_recipes->matches as $recipe) 
?>

<?php
//$arr_tobuy = return_shopping_list($arr_my_items, $arr_recipes);
$arr_tobuy = return_shopping_list_obj($arr_my_items, $arr_recipes);

//var_dump($arr_tobuy);

$response = array("tag" => $_POST['tag'], "success" => 0, "error" => 0);

if (empty($arr_tobuy)) 
{
	$response["success"] = 0;
	$response["error"] = 1;
	$response["error_msg"] = "no recipes found for: " . implode(", ",$arr_my_items) . " => (to buy array is empty)";
	echo json_encode($response);
	die();	
}

$search  = array("<font color=\"red\">*", "*</font>");
$replace = array("*", "*");

//show what it can do
$response["success"] = 1;
$response["nutritionstyle"] = $nutritionstyle; 
$i = 0;
foreach ($arr_tobuy as $recipe => $payload)
{
	$arr_payload = array();
	$payload_clean = str_replace($search, $replace, $payload);
	$arr_payload = explode(",",$payload_clean);
	
	$recipe_id = $arr_payload[0]; 	unset($arr_payload[0]);
	$rating = $arr_payload[1]; 		unset($arr_payload[1]);
	$score = $arr_payload[2]; 		unset($arr_payload[2]);
	$small_image = $arr_payload[3]; unset($arr_payload[3]);
	
	$payload_clean = implode(",", $arr_payload);
	$response[$i]["recipe"] = $recipe;
	$response[$i]["what_to_buy"] = explode(",",$payload_clean);
	$response[$i]["recipe_id"] = $recipe_id;
	$response[$i]["rating"] = $rating;
	$response[$i]["score"] = $score;
	$response[$i]["small_image"] = $small_image;
	$i++;
?>
<?php 
if ($debug) 
{
	echo '<b>' . $recipe . ':</b><br>';
	//echo str_replace(",", "<br>", $payload);
	echo $payload; 
	echo "<br />";
	echo "Score: " . $score;
	echo '<p></p>';
}
?>

<?php
}
	echo json_encode($response); 
?>