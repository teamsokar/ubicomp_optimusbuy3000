/** 
 *  @file   
 *  @brief  Get id of current item and send it over BLE to Smartphone
 */ 

#include <SPI.h>
#include <boards.h>
#include <services.h>
#include <ble_shield.h>


void getRFIDReaderOne();
void sendIDViaBTLE(char []);

int  valReaderOne = 0;
char codeReaderOne[11];     
boolean readerOneGotID = false;
unsigned long timeReaderOne = 0;
int bytesread = 0;

void setup(){
   ble_begin();
   Serial.begin(57600); 
   Serial1.begin(9600);
}

void loop(){
  
  if(Serial1.available() > 0 && (millis()-timeReaderOne)>2000 ) {          // if data available from reader 
    getRFIDReaderOne();
  }
  else {
    while(Serial1.available()>0)Serial1.read();
  }
  if(readerOneGotID){
    sendIDViaBTLE(codeReaderOne);
    readerOneGotID = false ;  
  }
  ble_do_events();
}

/**
* RFID-reader connected to Serial1 reads data and stores them in codeReaderOne[].
*/
void getRFIDReaderOne(){
    if((valReaderOne = Serial1.read()) == 02) {   // check for header 
      bytesread = 0; 
      while(bytesread<10) {              // read 10 digit code 
        if( Serial1.available() > 0) { 
          valReaderOne = Serial1.read(); 
          if((valReaderOne == 10)||(valReaderOne == 13)) { // if header or stop bytes before the 10 digit reading 
            break;                       // stop reading 
          } 
          codeReaderOne[bytesread] = valReaderOne;         // add the digit           
          bytesread++;                   // ready to read next digit  
        } 
      } 
      if(bytesread == 10) {        // if 10 digit read is complete 
        timeReaderOne = millis();
//        Serial.print("Reader code is: ");   // possibly a good TAG 
//        Serial.print(codeReaderOne);            // print the TAG code 
//        Serial.print(" Timestamp: ");
//        Serial.println(timeReaderOne);
        readerOneGotID = true;
      } 
      bytesread = 0;
      while(Serial1.available()>0)Serial1.read();
      delay(100);                       // wait for a 1/2 second 
    } 
}

/**
* Sends an id via BT to a connected device
* @param id
*/
void sendIDViaBTLE(char id[]){
  for(int i = 0; i < strlen(id); i++){
    ble_write(id[i]);
  }
}

