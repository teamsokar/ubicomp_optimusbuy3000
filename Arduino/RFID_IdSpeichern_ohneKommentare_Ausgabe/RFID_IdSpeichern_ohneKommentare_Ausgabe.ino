/** 
 *  @file   
 *  @brief  Save id, send it over WiFi, access LEDs
 */ 

#include <Arduino.h>
#include <SoftwareSerial.h>
#include "WiFly.h"
#include "HTTPClient.h"
#include <stdbool.h>
#define RFID_LEGACY 0x0F
#define SSID      "ArduinoTest"
#define KEY       "password"
#define AUTH      WIFLY_AUTH_WPA2_PSK
#define HTTP_POST_URL_server "http://mobile1.informatik.uni-ulm.de:80/websrv/index.php"

/**
* If 
* @variable switchpin 
*/
int switchPin = 11;
int switchState;
int greenLed = 8;
int yellowLed = 9;
int redLed = 10;

WiFly wifly(Serial1);
HTTPClient http;
char get;
char data[] = "tag=fridge&action=sendallrfids&userid=4";
char data_SEND_RFID_Markers[200];
boolean markersSend = true;

/**
* Struct for a list of items that store an id (RFID-Tag) and the time of registering.
*/
struct item{
   char id[11];
   unsigned long registeredTime;
   struct item *next;
};

/**
* list variable which represents the head of the list of items
*/
struct item* list;

void printList(struct item *);
int search_for(char [], struct item *);
void insertNewItem(char[], struct item *);
void getRFIDReaderOne();
void getRFIDReaderTwo();
void blinkLed(int);
void sendIDtoServer(struct item *);

void setup() {
  pinMode(greenLed, OUTPUT); 
  pinMode(yellowLed, OUTPUT); 
  pinMode(redLed, OUTPUT); 
  pinMode(switchPin,INPUT);
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(9600);
  Serial3.begin(9600);
  
  if (!wifly.isAssociated(SSID)) {
    while (!wifly.join(SSID, KEY, AUTH)) {
      Serial.println("Failed to join " SSID);
      Serial.println("Wait 0.1 second and try again...");
      delay(100);
    }
    Serial.println("Joined " SSID);
    wifly.save();    // save configuration, 
  }
  Serial.println("Joined " SSID);
   digitalWrite(greenLed, HIGH);
   digitalWrite(yellowLed, HIGH);
   digitalWrite(redLed, HIGH);
   delay(100);              
   digitalWrite(greenLed, LOW);
   digitalWrite(yellowLed, LOW);
   digitalWrite(redLed, LOW);
   delay(1000);
  
}

void loop() {  
  switchState = digitalRead(switchPin);
  if(switchState == LOW && markersSend==false){
    Serial.println("Door is closed");
    sendIDToServer(list);
  }
  else if(switchState == HIGH){
    
    Serial.println("Door is open");
    if(Serial2.available() > 0) {          // if data available from reader 
    getRFIDReaderOne();
    //printList(list);
    } 
    if(Serial3.available() > 0) {          // if data available from reader 
    getRFIDReaderTwo(); 
    //printList(list);
    } 
  }
}

/**
* Prints all items in the struct item at *item
* @param *item  Head of the list
*/
void printList(struct item *item){
    while(item!=NULL) {
        Serial.print("Gefundene ID lautet:");
        Serial.println(item->id);
        Serial.print("zum Zeitpunkt:");
        Serial.println(item->registeredTime);
        item = item->next;
    }
}

/**
* searches for the given charsequence (registredCode[]) in the struct item *item
* @param registeredCode[] RFID-Tag
* @param *item  Head of the list
*/
int search_for(char registeredCode[], struct item *item){
  while (item != NULL) {
        if (strcmp(item->id, registeredCode) == 0){
            return 1;
        }
        item = item->next;     
    }
    return 0;
}

/**
* inserts the given charsequence (registredCode[]) in the struct item *item at the last position
* @param registeredCode[] RFID-Tag
* @param *item  Head of the list
*/
void insertNewItem(char registeredCode[11], struct item *item ){ 
  if(search_for(registeredCode,item)<1){
    struct item *new_item = (struct item*) calloc(1, sizeof(struct item));
    strncpy(new_item->id,registeredCode, 10);
    new_item->id[10] = '\0';
    new_item->next = NULL;
    new_item->registeredTime = millis();
    if(list!=NULL){
      while(item->next != NULL){
        Serial.println(item->id);
          item = item->next;
      }
      item->next = new_item;    
      blinkLed(greenLed);
      markersSend=false;     
    }
    else {
      list = new_item;
      blinkLed(greenLed);
     markersSend=false; 
    } 
    }
  else removeItem(registeredCode, item);  
}


/**
* removes the given charsequence (registredCode[]) in the struct item *item
* @param registeredCode[] RFID-Tag
* @param *item  Head of the list
*/
void removeItem(char registeredCode[11], struct item *item) {
    
  while (item->next != NULL) {
      if (strcmp(item->next->id, registeredCode) == 0) {
        if (millis() - item->next->registeredTime >= 4000) {
          struct item *freeMe = item->next;
          item->next = item->next->next;
          free(freeMe);
          
          blinkLed(yellowLed);
          markersSend = false;
        }
        else{
          blinkLed(redLed);
        }        
        return;
      }
      item = item->next;
    }
  if (millis() - list->registeredTime >= 4000){
  blinkLed(yellowLed);
  markersSend = false;
  struct item *freeMe = list;
  list = list->next;
  free(freeMe);
  }
  else blinkLed(redLed);
}

/**
* RFID-reader connected to Serial2 reads data and stores them in the code[].
* If the data is a whole RFID-Tag, the code will be inserted in the list of items.
*/
void getRFIDReaderOne(){
  int val;
  char code[11];
  int bytesread = 0;
    if((val = Serial2.read()) == 02) {   // check for header 
      bytesread = 0; 
      while(bytesread<10) {              // read 10 digit code 
        if( Serial2.available() > 0) { 
          val = Serial2.read(); 
          if((val == 10)||(val == 13)) { // if header or stop bytes before the 10 digit reading 
            break;                       // stop reading 
          } 
          code[bytesread] = val;         // add the digit           
          bytesread++;                   // ready to read next digit  
        } 
      } 
      if(bytesread == 10) {              // if 10 digit read is complete     
        code[10] = '\0';
        insertNewItem(code,list);
      } 
      bytesread = 0;
      while(Serial2.available()>0)Serial2.read();
      delay(50);                       
    } 
}
/**
* Same as in getRFIDReaderTwo(). The RFID-reader is connected to Serial3
*/
void getRFIDReaderTwo(){
  int val;
  char code[11];
  int bytesread = 0;
    if((val = Serial3.read()) == 02) {   // check for header 
      bytesread = 0; 
      while(bytesread<10) {              // read 10 digit code 
        if( Serial3.available() > 0) { 
          val = Serial3.read(); 
          if((val == 10)||(val == 13)) { // if header or stop bytes before the 10 digit reading 
            break;                       // stop reading 
          } 
          code[bytesread] = val;         // add the digit           
          bytesread++;                   // ready to read next digit  
        } 
      } 
      if(bytesread == 10) {              // if 10 digit read is complete        
       code[10] = '\0';
        insertNewItem(code,list);
      } 
      bytesread = 0;
      while(Serial3.available()>0)Serial3.read();
      delay(50);                       
    } 
}

/**
* A LED given by color blinks for a couple of times
* @param color
*/
void blinkLed(int color){
  for(int i=0;i<5;i++){
    digitalWrite(color, HIGH);  // turn the LED on (HIGH is the voltage level)
    delay(50);                  // wait for 50 ms
    digitalWrite(color, LOW);   // turn the LED off (LOW is the voltage level)
    delay(50);    
    digitalWrite(color, HIGH);   
    delay(50);                 
    digitalWrite(color, LOW);
  } 
}

/**
* Send all the RFID-Tags to the server by extracting the id of any item found in *item
* @param *item Head of the list
*/
void sendIDToServer(struct item *item){
  strcpy(data_SEND_RFID_Markers,data); 
  strcat(data_SEND_RFID_Markers,"&rfidmarkers=");
  while(item != NULL){
    strcat(data_SEND_RFID_Markers,item->id);
    if(item->next != NULL)strcat(data_SEND_RFID_Markers,",");
    item = item->next;
  }
  Serial.println(data_SEND_RFID_Markers);
  while (http.post(HTTP_POST_URL_server,data_SEND_RFID_Markers, 10000) < 0 ) { 
  }
  
  while (wifly.receive((uint8_t *)&get, 1, 1000) == 1) {
    Serial.print(get);
  }
   digitalWrite(greenLed, HIGH);
   digitalWrite(yellowLed, HIGH);
   digitalWrite(redLed, HIGH);
   delay(100);              
   digitalWrite(greenLed, LOW);
   digitalWrite(yellowLed, LOW);
   digitalWrite(redLed, LOW);
  markersSend = true;
}
