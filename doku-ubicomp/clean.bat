@echo off
del *.aux
del *.log
del *.bbl
del *.lof REM list of floats
del *.lol REM list of listings
del *.lot REM list of tables
del *.out
del *.blg
del *.gz
del *.toc
del *.dvi
del *.nav
del *.snm
REM acronyms
del *.acn
del *.acr
del *.alg
REM glossary
del *.glg
del *.glo
del *.gls
del *.ilg
del *.ist
