package de.uulm.levazu;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.dimen;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

import de.uulm.levazu.library.JSONParser;
import de.uulm.levazu.library.Recipe;
import de.uulm.levazu.library.WebAPI;
import de.uulm.levazu.service.YummlyAPI;

/**
 * Fragment Class for managing the recipes.
 * @author vlad
 *
 */
@SuppressLint("ValidFragment")
public class RecipeFragment extends SherlockFragment 
{
	public interface RecipeFragmentListener 
	{	
		/**
		 * Will be called, when the recipes are updated.
		 * This means when a new recipe response from the YummlyAPI
		 * is received. Or when the next recipe is selected.
		 * @param pRecipes
		 */
		public void onRecipesUpdated(Object pRecipe);
	}
	
	/** root layout of the fragment */
	private LinearLayout mLinearLayout;
    private int mColour;
    private float mWeight;
    private int marginLeft, marginRight, marginTop, marginBottom;
	
    private Object[] mRecipes = {};
    private Recipe mCurrRecipe;
    private int mCurrRecipeIndex = 0;
    
    private TextView mDebugView;
    
    private RecipeFragmentListener mListener;
    
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context pContext, Intent pIntent) 
        {
          Bundle bundle = pIntent.getExtras();
          if (bundle != null) 
          {
            int resultCode = bundle.getInt(YummlyAPI.RESULT);
            if (resultCode == Activity.RESULT_OK) 
            {
        		mRecipes = (Object[]) bundle.getSerializable(YummlyAPI.RECIPES);
        		if (mRecipes.length > 0)
        		{
        			mListener.onRecipesUpdated(mRecipes[0]);
        			//displayRecipes(bundle);
        			mCurrRecipe = getRecipeWithHigehestScore(mRecipes);
        			showRecipe((Recipe) mRecipes[mCurrRecipeIndex]);
        		
        			Toast.makeText(getSherlockActivity(), "Request to YummlyAPI complete.", Toast.LENGTH_SHORT).show();
        		}
        		else 
        			Toast.makeText(getSherlockActivity(), "No Recipes found", Toast.LENGTH_SHORT).show();
        	} 
            else 
              Toast.makeText(getSherlockActivity(), "Request to YummlyAPI failed!", Toast.LENGTH_SHORT).show();
          }
        }
      };
    
    
    public RecipeFragment(int pColour, float pWeight, int pMargin_left,
            int pMargin_right, int pMargin_top, int pMargin_bottom)
    {
    	mColour = pColour;
    	mWeight = pWeight;
    	marginLeft = pMargin_left;
    	marginRight = pMargin_right;
    	marginTop = pMargin_top;
    	marginBottom = pMargin_bottom;
    }
    
	@Override
	public void onCreate(Bundle pSavedInstanceState) 
	{
		super.onCreate(pSavedInstanceState);
		
		mLinearLayout = new LinearLayout(getSherlockActivity());
		
		LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, mWeight);
        layoutparams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        
        mLinearLayout.setLayoutParams(layoutparams);  
        mLinearLayout.setBackgroundColor(mColour);
        
        // add XML layout
        View view = getLayoutInflater(pSavedInstanceState).inflate(R.layout.fragment_recipes_view, mLinearLayout, false);        
        mLinearLayout.addView(view);
        
        Log.d(getClass().getSimpleName(), "RecipeFragment created");
	}
    
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return mLinearLayout;
	}
	
	@Override
	public void onStart() 
	{
		super.onStart();
		Log.d(getClass().getSimpleName(), "onStart()");

//		displayRecipes();
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		getSherlockActivity().registerReceiver(mReceiver, new IntentFilter(YummlyAPI.NOTIFICATION));
		Log.d(getClass().getSimpleName(), "onResume()");
	}

	@Override
	public void onPause() 
	{
		super.onPause();
		getSherlockActivity().unregisterReceiver(mReceiver);
		Log.d(getClass().getSimpleName(), "onPause()");
	}

	@Override
	public void onAttach(Activity pActivity) 
	{
		super.onAttach(pActivity);
		Log.d(getClass().getSimpleName(), "onAttach()");
		
		// ShoppingListActivity listens to RecipeFragment
		mListener = (RecipeFragmentListener) pActivity;
	}

	@Override
    public void onSaveInstanceState(Bundle outState) 
	{
        //super.onSaveInstanceState(outState);
	}
	
	
	/**
	 * XXX
	 * Returns the recipe with the highest score within the given recipes.
	 * On details how the score is computed (MAUT), look here: ...
	 * @param pRecipes
	 */
	public Recipe getRecipeWithHigehestScore(Object[] pRecipes)
	{
		if (pRecipes.length < 1) 
			return new Recipe("no recipes", new String[0], "-1", "-1", "-1");
		
		// TODO get recipe with highest score
		Recipe recipe = (Recipe) pRecipes[mCurrRecipeIndex];
		
		return recipe;
	}
	
	
	/**
	 * Shows the recipe on the recipe fragment.
	 * @param pRecipe
	 */
	private void showRecipe(Recipe pRecipe)
	{
		LinearLayout layout = (LinearLayout) getView().findViewById(R.id.layout_recipe_view);
		
		// button_next_recipe
		Button btn = (Button) layout.findViewById(R.id.btn_next_recipe);
		btn.setVisibility(View.VISIBLE);
		
		// clear everything on layout_recipe_view
		layout.removeAllViews();
		
		TextView head = new TextView(getSherlockActivity());
		head.setTypeface(null, Typeface.BOLD_ITALIC);
		head.setText(pRecipe.getName());
		
		LinearLayout body = new LinearLayout(getSherlockActivity());
		body.setOrientation(LinearLayout.HORIZONTAL);
		
		TextView bodyTxt = new TextView(getSherlockActivity());
		bodyTxt.setTypeface(null, Typeface.NORMAL);
//		bodyTxt.setText("ID: "+ pRecipe.getRecipeId()+"\n");
		bodyTxt.append(" Rating: "+ pRecipe.getRating());
		bodyTxt.append("\n Score: "+ pRecipe.getScore());
		bodyTxt.append("\n");
		
		// XXX
		// Display display = getWindowManager().getDefaultDisplay();
		// width..
		ImageView bodyImg = new ImageView(getSherlockActivity());
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(250, 180);
		bodyImg.setLayoutParams(layoutParams);
		
		body.addView(bodyImg);
		body.addView(bodyTxt);
		
		
//		Button btn = new Button(getSherlockActivity());
//		btn.setBackgroundResource(R.drawable.button_next_recipe);
//		// set width, height and margins
////		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(R.dimen.button_add_product_width, R.dimen.button_add_product_height);
//		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//		btn.getLayoutParams().width = 180;
//		btn.getLayoutParams().height = 45;
//		// margin for left, top, right, bottom
//		//params.setMargins(R.dimen.button_add_product_marginLeft, R.dimen.button_add_product_marginTop, R.dimen.button_add_product_marginRight, R.dimen.button_add_product_marginBottom);
//		btn.setLayoutParams(params);
		
		//btn.setText("Next Recipe");
		// click on "next recipe" button
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showNextRecipe();
			}
		});
		
		layout.addView(btn);
		layout.addView(head);
		layout.addView(body);
		
		// get the image from the recipe async and put it into the given ImageView
		new DownloadImageTask(bodyImg).execute(pRecipe.getSmallImageUrl());
        //.execute("http://java.sogeti.nl/JavaBlog/wp-content/uploads/2009/04/android_icon_256.png");
		
		// puts the details for the given recipe id into the passed layout
		new FetchRecipeDetails(layout).execute(pRecipe);
		
		
		
//		// XXX for debug purpose..
//        mDebugView = new TextView(getSherlockActivity());
//        for (String ingredient : pRecipe.getWhatToBuy())
//        	mDebugView.append(ingredient + "\n");
//        
//        layout.addView(mDebugView);
	}
	
	/**
	 * Shows the next recipe.
	 */
	private void showNextRecipe()
	{
		mCurrRecipeIndex++;
		if (mCurrRecipeIndex < mRecipes.length && mRecipes[mCurrRecipeIndex] != null)
		{
			showRecipe((Recipe) mRecipes[mCurrRecipeIndex]);

			// notify the ShoppingListActivity that a new recipe is selected
			mListener.onRecipesUpdated((Recipe) mRecipes[mCurrRecipeIndex]);
		}
		else
			mCurrRecipeIndex = -1;	
	}
	
	/**
	 *  Displays the recipes within the Bundle.
	 *  For debug purpose..
	 *  @param pBundle
	 *  @deprecated
	 */
	protected boolean displayRecipes(Bundle pBundle)
	{
		if (pBundle == null) return false;
		
		if (mRecipes.length < 1) return false;
		
		LinearLayout layout = (LinearLayout) getView().findViewById(R.id.layout_recipe_view);
		TextView[] textViews = new TextView[mRecipes.length];
		
    	for(int i = 0; i < mRecipes.length; i++)
    	{
    		Recipe curr_recipe = (Recipe) mRecipes[i];
    		Log.d("RecipeFragment (BroadcastReceiver)", "Recipe " + i + " " + curr_recipe.getName());
    		textViews[i] = new TextView(getSherlockActivity());
    		textViews[i].setText(curr_recipe.getName());
    		textViews[i].setTypeface(null, Typeface.BOLD_ITALIC);
    		layout.addView(textViews[i]);

    		String[] what_to_buy =  curr_recipe.getWhatToBuy();
    		TextView[] textViewsJ = new TextView[what_to_buy.length];
    		for (int j = 0; j < what_to_buy.length; j++)
    		{
    			textViewsJ[j] = new TextView(getSherlockActivity());
    			if (what_to_buy[j].contains("*"))  // this * means that the ingredient is maybe already in the fridge or on the list.  
    			{
    				what_to_buy[j] = what_to_buy[j].replace("*", "");
    				textViewsJ[j].setTextColor(Color.parseColor("#FF0000"));
    			}
    			else
    			{
    				textViewsJ[j].setTextColor(Color.parseColor("#000000"));
    			}
    			textViewsJ[j].setText(what_to_buy[j]);
    			layout.addView(textViewsJ[j]);
    		}
    	}    	
    	
		return true;
	}
	
	/**
	 * Returns the recipes which were hopefully received from the yummly API.
	 * @return null, if mRecipes is empty
	 */
	public Recipe[] getRecipes()
	{
		if (mRecipes.length > 0)
		{	
			Recipe[] recipes = Arrays.copyOf(mRecipes, mRecipes.length, Recipe[].class);
				
			return recipes;
		}
		else 
			return null;
	}
	
	
	/**
	 * Usage: new DownloadImageTask(pImageView).execute(pURL);
	 * Downloads an image from the URL and puts it into the
	 * given pImageView
	 */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> 
    {
    	ImageView bmImage;

    	public DownloadImageTask(ImageView bmImage) {
    		this.bmImage = bmImage;
    	}

    	protected Bitmap doInBackground(String... urls) {
    		String urldisplay = urls[0];
    		Bitmap mIcon11 = null;
    		try {
    			InputStream in = new java.net.URL(urldisplay).openStream();
    			mIcon11 = BitmapFactory.decodeStream(in);
    		} catch (Exception e) {
    			Log.e("Error", e.getMessage());
    			e.printStackTrace();
    		}
    		return mIcon11;
    	}

    	protected void onPostExecute(Bitmap result) {
    		bmImage.setImageBitmap(result);
    	}
    }
    
    
    /**
     * You can give this class a LinearLayout, wich will be filled async with
     * the recipe details from our Yummly service.
     * @author vlad
     *
     */
    private class FetchRecipeDetails extends AsyncTask<Recipe, Void, ArrayList<String>> 
    {
    	LinearLayout mLayout;
    	Recipe mRecipe;
    	
    	// pass a LinearLayout to fill with the recipe details
    	public FetchRecipeDetails(LinearLayout pLinearLayout) 
    	{
    		mLayout = pLinearLayout;
    	}

    	protected ArrayList<String> doInBackground(Recipe... pRecipe) 
    	{
    		mRecipe = pRecipe[0];
    		// the yummly unique recipe id
    		String recipeId = pRecipe[0].getRecipeId();
    		// the ArrayList which will be returned
    		ArrayList<String> recipeDetails = new ArrayList<String>();
    		
    		try {
    			// request to get_recipe.php
    			// Building Parameters
    			List<NameValuePair> params = new ArrayList<NameValuePair>();
    			params.add(new BasicNameValuePair("tag", "getrecipe"));
    			params.add(new BasicNameValuePair("recipe_id", recipeId.trim()));
    			
    			// Request to websrv
    			JSONParser jsonParser = new JSONParser();
    			JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);

    			if (json == null) 
    			{
    				Log.e(getClass().getSimpleName(), "json object is null :(");
    				return recipeDetails;
    			}
    			
    			try 
    			{
    				String res = json.getString("success");
    				if(Integer.parseInt(res) == 1)
    				{
    					String imageUrl = json.getString("imageUrl");
    					JSONArray ingredientLines = json.getJSONArray("ingredientLines");
    					
    					for(int i = 0; i < ingredientLines.length(); i++)
    					{
    						String ingrLine = (String) ingredientLines.get(i);
    						// add ingredient lines to the arraylist, which will be available in onPostExecute()
    						recipeDetails.add(ingrLine);
    					}
    				}
    			}
    			catch (JSONException e) 
    			{
    				e.printStackTrace();
    			}
    			
    			//recipeDetails.add(recipeId);
    		} 
    		catch (Exception e) 
    		{
    			Log.e("Error", e.getMessage());
    			e.printStackTrace();
    		}
    		
    		return recipeDetails;
    	}

    	// result contains the infos from the previous doInBackground() task
    	protected void onPostExecute(ArrayList<String> pResult) 
    	{    
    		mRecipe.getWhatToBuy();
    		
    		// fill something into the given LinearLayout
    		TextView head = new TextView(getSherlockActivity());
    		head.setTypeface(null, Typeface.BOLD_ITALIC);
    		head.append("Recipe Details");
    		//head.append("Recipe ID: ");
    		mLayout.addView(head);
    		
//    		TextView body = new TextView(getSherlockActivity());
//    		body.setTypeface(null, Typeface.NORMAL);
    		
    		// loop through results ArrayList
    		for (String line : pResult)
    		{
    			TextView txtView =  new TextView(getSherlockActivity());
    			txtView.setTypeface(null, Typeface.NORMAL);
    			txtView.setText(line);
    			// alter
    			
    			for (String ingr : mRecipe.getWhatToBuy())
    			{
    				if(line.contains(ingr))
    				{
    					txtView.setTypeface(null, Typeface.BOLD_ITALIC);
    					txtView.setText(line);
    					//body.append("*");    					
    				}
    			}
    			//body.append(line + "\n");
    			mLayout.addView(txtView);    
    		}
    		
    		// add everything to the given layout

    		//mLayout.addView(body);
    		
//    		Log.i(getClass().getSimpleName(), "The returned list contains " +pResult.size()+ "elements");
    	}
    }
    
}
