package de.uulm.levazu.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.uulm.levazu.MainActivity;
import de.uulm.levazu.library.JSONParser;
import de.uulm.levazu.library.Recipe;
import de.uulm.levazu.library.WebAPI;
import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Service which sends requests to our web-server Yummly API.
 * 
 * @author vlad
 *
 */
public class YummlyAPI extends IntentService 
{
	private int mResult = Activity.RESULT_CANCELED;
	public static final String FRIDGE_LIST = "fridge_list";
	public static final String GROCERY_LIST = "grocery_list";
	public static final String TO_BUY = "tobuy";
	public static final String RESULT = "result";
	public static final String RECIPES = "recipes";
	public static final String NOTIFICATION = "de.uulm.levazu.service.YummlyAPI";
	
	/**
	 * The recipes returned from the YummlyAPI Service.
	 */
	private Recipe[] mRecipes = {};

	public YummlyAPI()
	{
		super("YummlyAPI");
	}
	
	// will be called asynchronously by Android
	@Override
	protected void onHandleIntent(Intent pIntent) 
	{
		String fridge_list = pIntent.getStringExtra(FRIDGE_LIST);
		String grocery_list = pIntent.getStringExtra(GROCERY_LIST);
		
		WebAPI webAPI = MainActivity.WEBAPI;
		JSONParser jsonParser = webAPI.getJSONParser();

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", WebAPI.TO_BUY));
		params.add(new BasicNameValuePair("grocery_list", grocery_list));
		params.add(new BasicNameValuePair("fridge_list", fridge_list));
		
        Log.d(getClass().getSimpleName(), "--- SEND TO YUMMLY API:");
        Log.d(getClass().getSimpleName(), "grocery_list: " + grocery_list); 
        Log.d(getClass().getSimpleName(), "fridge_list:  " + fridge_list);
		
		// Request to WebAPI
		JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);

		if (json == null) 
		{
			Log.e(getClass().getSimpleName(), "json object is null :(");
			return;
		}

		try 
		{
			String res = json.getString("success");
			if(Integer.parseInt(res) == 1)
			{
				int num_of_recipes = json.length() - 4; // - number of other meta-infos in the payload
				mRecipes = new Recipe[num_of_recipes];
				for (int i = 0; i < num_of_recipes; i++)
				{
					JSONObject json_recipe = json.getJSONObject(Integer.toString(i));
					String recipe_name = json_recipe.getString("recipe");
					String recipe_id = json_recipe.getString("recipe_id");
					String recipe_rating = json_recipe.getString("rating");
					String recipe_score = json_recipe.getString("score");
					String recipe_img_url = json_recipe.getString("small_image");
					Log.d(getClass().getSimpleName(), "" + recipe_name);

					JSONArray jsonarr_tobuy = json_recipe.getJSONArray("what_to_buy");
					String[] ingredients = new String[jsonarr_tobuy.length()];
					// get missing ingredients, which have to be bought for the current recipe
					for(int j = 0; j < jsonarr_tobuy.length(); j++)
					{					
						ingredients[j] = jsonarr_tobuy.getString(j);
						// remove "*" from string
						if (ingredients[j].contains("*")) // this * means that the ingredient is maybe already in the fridge or on the list.   
							ingredients[j] = ingredients[j].replace("*", "").trim();
					}
					
					mRecipes[i] = new Recipe(recipe_name, ingredients, recipe_id, recipe_rating, recipe_score);
					mRecipes[i].setSmallImageURL(recipe_img_url);
				}
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
				
		// successfully finished
		mResult = Activity.RESULT_OK;

		publishResults(mResult, mRecipes);
	}
	
	
	private void publishResults(int pResult, Recipe[] pRecipes) 
	{
		Intent intent = new Intent(NOTIFICATION);
		intent.putExtra(RESULT, pResult);
		intent.putExtra(RECIPES, pRecipes);
		sendBroadcast(intent);
	}

}
