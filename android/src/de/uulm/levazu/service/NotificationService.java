package de.uulm.levazu.service;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.uulm.levazu.ShoppingListActivity;
import de.uulm.levazu.library.NotificationBuilder;
import de.uulm.levazu.library.WebAPI;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
/**
 * Service for notifications, like shopping notifications. 
 * @author Andreas Blezu
 *
 */
public class NotificationService extends IntentService{

	WebAPI webAPI;
	Context context;
	NotificationBuilder nBuilder;
	private HashMap <String, String> user = new HashMap<String, String>();
	private JSONObject shoppingList;
	private JSONObject loadedShoppingList;
	private JSONArray loggedInUserPreferences;
	public static enum ForwardActivity{SHOPPINGLIST,MAP,BUDGET,NORMAL};
	public static int shoppingNotificationSequenzNumber=0;
	public NotificationService(){
		super("NotificationService");
		
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		if(context == null) 
			context = this;
		if(webAPI == null) 
			webAPI = new WebAPI(context);
		setUserDetails(intent);
		if(loggedInUserPreferences == null)
			loggedInUserPreferences = webAPI.getPreferencesByUserId(user.get("id"));
		if (user.get("id") != null && !ShoppingListActivity.isActivityVisible() && "shoppingNotification".equals(user.get("category"))) 
			checkShoppingList();
//		if (user.get("id") != null && "gpsNotification".equals(user.get("category"))) 
//			checkLocation();
		
	}

	/**
	 * Loads shoppinglist of a user and starts a notification
	 */
	private void checkShoppingList() {
		JSONObject json = webAPI.askShoopinglistForUpdates(user.get("id"));
		try {
			if(json != null && "true".equals(json.getString("new_products"))){
				loadedShoppingList = webAPI.getShoppingListByUser(Integer.parseInt(user.get("id")),true);
				if(shoppingList==null)shoppingList = loadedShoppingList;
				
				// F�r Morgen: Check Updated at: Spalte ab und �ndere das PHP-Srikpt, sodass immer upgedatet wird, wenn Shoppingliste 
				//ge�ndert wird
				
				if (shoppingList != null && !shoppingList.equals(loadedShoppingList)) {
					
					shoppingList = loadedShoppingList;
					nBuilder= new NotificationBuilder(context,user,ForwardActivity.SHOPPINGLIST,loggedInUserPreferences);
				} else
					try {
						int productsOnShoppinglist=0;
						for(int i =0;i<shoppingList.getJSONArray("shoppinglist").length();i++){
							if(shoppingList.getJSONArray("shoppinglist").getJSONObject(i)!=null)
								productsOnShoppinglist++;
						}
						if (productsOnShoppinglist > 3 && shoppingNotificationSequenzNumber < 2) {
						//if (shoppingList.getJSONArray("shoppinglist").length()-1 > 3) {//debug
							nBuilder= new NotificationBuilder(context,user,ForwardActivity.SHOPPINGLIST,loggedInUserPreferences);
							shoppingNotificationSequenzNumber++;
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void checkBudget(){
		System.out.println("Budget service");
	}
	
	private void checkLocation(){
		int distance=0;
		//1. get distance to nearest shop
		//2. setup notification if user is 30m away from shop
		//3. set current shop if user is 5m away from shop
		nBuilder= new NotificationBuilder(context,user,ForwardActivity.MAP,loggedInUserPreferences);
		
	}
	
	
	/**
	 * Saving user details like email, password and id
	 * @param intent
	 */
	private void setUserDetails(Intent intent) {
		if(user.get("id") == null){
			user = new HashMap<String, String>();
			user.put("id", intent.getExtras().getString("id"));
			user.put("email", intent.getExtras().getString("email"));
			user.put("password", intent.getExtras().getString("password"));
			user.put("category", intent.getExtras().getString("category"));
		}
		else if(!user.get("id").equals(intent.getExtras().getString("id")));
		{
			user.put("id", intent.getExtras().getString("id"));
			user.put("email", intent.getExtras().getString("email"));
			user.put("password", intent.getExtras().getString("password"));
			user.put("category", intent.getExtras().getString("category"));
		}
	}
	
	
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		System.out.println("destroyed");
	}
	
}
