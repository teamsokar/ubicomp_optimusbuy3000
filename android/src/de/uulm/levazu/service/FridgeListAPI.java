package de.uulm.levazu.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import de.uulm.levazu.MainActivity;
import de.uulm.levazu.library.Product;
import de.uulm.levazu.library.WebAPI;

/**
 * 
 * @author vlad
 *
 */
public class FridgeListAPI extends IntentService 
{
	private int mResult = Activity.RESULT_CANCELED;
	public static final String FRIDGELIST = "fridgelist";
	public static final String RESULT = "result";
	public static final String NOTIFICATION = "de.uulm.levazu.service.FridgeListAPI";
	
	private Product[] mFridgeItems  = {};
	
	
	public FridgeListAPI()
	{
		super("FridgeListAPI");
	}
	
	// will be called asynchronously by Android
	@Override
	protected void onHandleIntent(Intent pIntent) 
	{
		WebAPI webAPI = MainActivity.WEBAPI;
		
		// Request to our websrv
		JSONObject json = webAPI.getItemsInFridge();
		

		try {
			String res = json.getString("success");
			if(Integer.parseInt(res) == 1)
			{
				// If it's a success create a new JSON object for the shopping element (aka JSONArray)
				JSONArray json_fridgelist = json.getJSONArray("fridgelist");
				mFridgeItems = new Product[json_fridgelist.length()];
				for (int i = 0; i < json_fridgelist.length(); i++)
				{
					String product_id = json_fridgelist.getJSONObject(i).getString("product_collection_id");
					String product_name = json_fridgelist.getJSONObject(i).getString("product_name");
					String product_ingredient = json_fridgelist.getJSONObject(i).getString("ingredient");
					String product_category = json_fridgelist.getJSONObject(i).getString("category");
					String user_id = json_fridgelist.getJSONObject(i).getString("user_id");
					String product_price = json_fridgelist.getJSONObject(i).getString("price");
					String product_tag[] = {json_fridgelist.getJSONObject(i).getString("tag")};
					
					// add to FridgeItems
					mFridgeItems[i] = new Product(product_id, "-1", "-1", product_name,product_category, product_ingredient, product_price, product_tag);
				}			
			}
			else
			{
				// if the returned json has no fridgelist items	
				String err_msg = json.getString("error_msg");
				Log.e(getClass().getSimpleName(), err_msg);
			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		// successfully finished
		mResult = Activity.RESULT_OK;

		publishResults(mResult, mFridgeItems);
	}
	
	/**
	 * Creates an Intent with the results.
	 * @param pResult
	 * @param pFridgeItems
	 */
	private void publishResults(int pResult, Product[] pFridgeItems) 
	{
		Intent intent = new Intent(NOTIFICATION);
		intent.putExtra(RESULT, pResult);
		intent.putExtra(FRIDGELIST, pFridgeItems);
		sendBroadcast(intent);
	}
}
