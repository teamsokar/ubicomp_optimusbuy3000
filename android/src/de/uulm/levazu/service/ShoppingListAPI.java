package de.uulm.levazu.service;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import de.uulm.levazu.MainActivity;
import de.uulm.levazu.library.Helper;
import de.uulm.levazu.library.Product;
import de.uulm.levazu.library.ShoppingListFunctions;
import de.uulm.levazu.library.WebAPI;

/**
 * 
 * @author vlad
 *
 */
public class ShoppingListAPI extends IntentService 
{
	private int mResult = Activity.RESULT_CANCELED;
	public static final String SHOPPINGLIST = "shoppinglist";
	public static final String RESULT = "result";
	public static final String NOTIFICATION = "de.uulm.levazu.service.ShoppingListAPI";
	public static final String ERRORMSG = "error_message";
	
	private String mErrorMsg = "";
	
	private Product[] mProducts  = {};
	

	public ShoppingListAPI()
	{
		super("ShoppingListAPI");
	}
	
	// Will be called asynchronously by Android.
	// The intent is sent from ShoppingListActivity.
	@Override
	protected void onHandleIntent(Intent pIntent) 
	{		
		WebAPI webAPI = MainActivity.WEBAPI;
		JSONObject json = webAPI.getShoppingListForCurrentUser();

		try {
			String res = json.getString("success");
			if(Integer.parseInt(res) == 1)
			{
				// If it's a success create a new JSON object for the shopping element (aka JSONArray)
				//JSONArray json_shoppinglist = json.getJSONArray("shoppinglist");
				Product[] product = ShoppingListFunctions.createProductFromJSON(json, "shoppinglist");
				mProducts = new Product[0];
				//for (int i = 0; i < json_shoppinglist.length(); i++)
				if(product !=null){
					for (int i = 0; i < product.length; i++)
					{
						if(product[i]!=null){
							mProducts = (Product[]) Helper.resizeArray(mProducts);
							mProducts[mProducts.length-1] = product[i];
						}
						
					}
				}
				//results.append("" + json_shoppinglist.getJSONObject(i).getString("product_name") + "\n");				
			}
			else
			{
				// if the shopping-list could not be found				
				String err_msg = json.getString("error_msg");
				Log.e(getClass().getSimpleName(), err_msg);
			}
		} 
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// successfully finished
		mResult = Activity.RESULT_OK;
		
		publishResults(mResult, mProducts, mErrorMsg);
	}
	
	
	private void publishResults(int pResult, Product[] pProducts, String pErrorMsg) 
	{
		Intent intent = new Intent(NOTIFICATION);
		intent.putExtra(RESULT, pResult);
		intent.putExtra(SHOPPINGLIST, pProducts);
		intent.putExtra(ERRORMSG, pErrorMsg);
		sendBroadcast(intent);
	}
}
