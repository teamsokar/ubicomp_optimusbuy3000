package de.uulm.levazu.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * 
 * @author vlad
 *
 */
public class WebAPIService extends Service 
{
	public static final String ACTION_BROADCAST = "de.uulm.levazu.intent.action.WEBAPISERVICE";
	
	private WebAPIServiceBinder mBinder = new WebAPIServiceBinder();
	
	
	public class WebAPIServiceBinder extends Binder {
		// Schnittstellenmethoden für den Service
	}
	
	@Override
	public IBinder onBind(Intent pIntent) 
	{
		return mBinder;
	}

}
