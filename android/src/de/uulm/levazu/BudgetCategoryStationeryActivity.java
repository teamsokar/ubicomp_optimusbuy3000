package de.uulm.levazu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;
//import de.uulm.levazu.R;

/**
 * stub class for testing, can be filled like BudgetCategoryGroceriesActivity
 * @author mike
 *
 */
public class BudgetCategoryStationeryActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_budget_stationery);
		TextView title = (TextView) findViewById(R.id.statioery_title_label);
		title.setText("Statioery"); 

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.budget_category_statioery, menu);
		return true;
	}

}
