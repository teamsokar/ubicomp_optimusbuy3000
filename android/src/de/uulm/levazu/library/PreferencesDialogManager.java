package de.uulm.levazu.library;
import org.json.JSONException;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import de.uulm.levazu.R;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.*;
import android.widget.SeekBar.OnSeekBarChangeListener;

/**
 * @author Andy
 *
 */
public class PreferencesDialogManager{
	private String categorie;
	private Builder builder;
	private Context context;
	public UserPreferences userPreferences;
	private Boolean changedPreferences = false;
	
    public Boolean getChangedPreferences() {
		return changedPreferences;
	}
    public void setChangedPreferences(Boolean changedPreferences) {
		this.changedPreferences = changedPreferences;
	}

	public PreferencesDialogManager(UserPreferences userPref){
		super();
		userPreferences = userPref;
	}
	
	public String getKategorie() {
		return categorie;
	}

	public void setKategorie(String kategorie) {
		this.categorie = kategorie;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
	int oldNut;
	int nutritionStyle;
	double distance;
	int maxCalories;
	int oldCalories;
	int notifications;
	int notificationRate;
	
	public AlertDialog createDialog(){
		
		builder = new AlertDialog.Builder(context);
		builder.setTitle(categorie);
		
		//GUIs for usersetting-dialogs
			//Distance
		if(categorie.equals(context.getString(R.string.distance))){
			try {
				distance = Double.parseDouble(UserPreferences.preferences.getJSONObject(0).getString("distance"))/10;
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = infalInflater.inflate(R.layout.settings_distance, null);
            
            final SeekBar distanceBar = (SeekBar) convertView.findViewById(R.id.distanceBar);
            distanceBar.setProgress((int) distance*10);
            
            TextView distanceLabel = (TextView) convertView.findViewById(R.id.distanceValueLabel);
            distanceLabel.setText(context.getString(R.string.settings_distance_label));
            
            final TextView distanceValue = (TextView) convertView.findViewById(R.id.distanceValue);
            distanceValue.setText(""+distance);
            
            final TextView distanceValueUnit = (TextView) convertView.findViewById(R.id.distanceValueUnit);
            distanceValueUnit.setText(context.getString(R.string.settings_distance_unit));
            
            distanceBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					// TODO Auto-generated method stub
					distanceValue.setText(""+(double)progress/10);
					
				}
			});
            builder.setView(convertView);
            builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                	distance = Double.parseDouble(distanceValue.getText().toString());
                	userPreferences.setPreferenceByCategorie("distance", ""+distance*10);
                	changedPreferences = true;
                }
            });
            	builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
						distanceBar.setProgress(Integer.parseInt(userPreferences.getPreferencesByCategory("distance"))/10);
                }
            });
		}
		
		//calorie Monitoring
		else if(categorie.equals(context.getString(R.string.calories))){
			try {
				maxCalories = Integer.parseInt(UserPreferences.preferences.getJSONObject(0).getString("calories"));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
			oldCalories = maxCalories;
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = infalInflater.inflate(R.layout.settings_calories, null);
            
            final SeekBar calorieBar = (SeekBar) convertView.findViewById(R.id.calorieBar);
            calorieBar.setMax(5000);
            calorieBar.setProgress(maxCalories);
            
            
            TextView calorieLabel = (TextView) convertView.findViewById(R.id.calorieValueLabel);
            calorieLabel.setText(context.getString(R.string.settings_calorie_label));
            
            final TextView calorieValue = (TextView) convertView.findViewById(R.id.calorieValue);
            calorieValue.setText(""+maxCalories);
            
            //Param for consumed calories needed!!
            final ProgressBar calorieStatusBar = (ProgressBar) convertView.findViewById(R.id.bar_calorie_status);
            calorieStatusBar.setMax(maxCalories);
            
            final TextView calorieStatus = (TextView) convertView.findViewById(R.id.settings_label_calorie_left);
            calorieStatus.setText("" + calorieStatusBar.getProgress() + "/" + maxCalories);
            
            calorieBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
					// TODO Auto-generated method stub
					maxCalories = progress;
					calorieValue.setText(""+maxCalories);
					calorieStatusBar.setMax(maxCalories);
					calorieStatus.setText("" + 2 + "/" + maxCalories);
					
				}
			});
            
            
            
            builder.setView(convertView);
            builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                	userPreferences.setPreferenceByCategorie("calories", ""+maxCalories);
                	calorieStatusBar.setMax(maxCalories);
                	changedPreferences = true;
                }
            });
            	builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                	calorieBar.setProgress(Integer.parseInt(userPreferences.getPreferencesByCategory("calories"))/10);
                }
            });
		}
		
			//Nutritionstyle
		else if(categorie.equals(context.getString(R.string.nutritionalStyle))){
			String[] items = {context.getString(R.string.nutritionCarn),
					context.getString(R.string.nutritionVeget),context.getString(R.string.nutritionVegan),
					context.getString(R.string.nutritionHal),context.getString(R.string.nutritionKos)};
			final RadioGroup radioGrp = new RadioGroup(context);
			LinearLayout.LayoutParams layoutParams = new RadioGroup.LayoutParams(
	                LayoutParams.MATCH_PARENT,
	                LayoutParams.MATCH_PARENT);
			for (int i = 0; i < 5; i++){
	            RadioButton newRadioButton = new RadioButton(context);
	            String label = items[i];
		    newRadioButton.setText(label);
		    newRadioButton.setId(i);
		    radioGrp.addView(newRadioButton, layoutParams);
	        }
			radioGrp.check(Integer.parseInt(userPreferences.getPreferencesByCategory("nutritionstyle")));
			builder.setMessage(R.string.nutritionalStyleQues);
			builder.setView(radioGrp);
			builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                	int index = radioGrp.getCheckedRadioButtonId();
                	userPreferences.setPreferenceByCategorie("nutritionstyle", ""+index);
                	changedPreferences = true;
                	
                }
            });
            builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                	dialog.cancel();
                	radioGrp.check(Integer.parseInt(userPreferences.getPreferencesByCategory("nutritionstyle")));
                }
            });
		}
		// notifications
		else if(categorie.equals(context.getString(R.string.shoppingNotification))){
			notifications = Integer.parseInt(userPreferences.getPreferencesByCategory("notifications"));
			LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = infalInflater.inflate(R.layout.settings_shopping_notifications, null);
            
            TextView audioNotification = (TextView) convertView.findViewById(R.id.shopping_notifications_audio);
            audioNotification.setText(context.getString(R.string.shopping_notifications_audio));
            final Switch audioNotificationSwitch = (Switch) convertView.findViewById(R.id.shopping_notifications_audio_switch);
            if(notifications>=100)audioNotificationSwitch.setChecked(true);
			audioNotificationSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
							// TODO Auto-generated method stub
							if(isChecked) notifications = notifications+100;
							if(!isChecked) notifications = notifications-100;
						}
					});
            TextView visualNotification = (TextView) convertView.findViewById(R.id.shopping_notifications_visual);
            visualNotification.setText(context.getString(R.string.shopping_notifications_visual));
            final Switch visualNotificationSwitch = (Switch) convertView.findViewById(R.id.shopping_notifications_visual_switch);
            if(notifications%100>=10)visualNotificationSwitch.setChecked(true);
            visualNotificationSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked) notifications = notifications+10;
					if(!isChecked) notifications = notifications-10;
				}
			});
            TextView tactileNotification = (TextView) convertView.findViewById(R.id.shopping_notifications_tactile);
            tactileNotification.setText(context.getString(R.string.shopping_notifications_tactile));
            final Switch tactileNotificationSwitch = (Switch) convertView.findViewById(R.id.shopping_notifications_tactile_switch);
            if(notifications%10>=1)tactileNotificationSwitch.setChecked(true);
            tactileNotificationSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					if(isChecked) notifications = notifications+1;
					if(!isChecked) notifications = notifications-1;
				}
			});
			builder.setMessage(R.string.shopping_notication_message);
			builder.setView(convertView);
			builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                	userPreferences.setPreferenceByCategorie("notifications", ""+notifications);
                	changedPreferences = true;
                	
                }
            });
            builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                	dialog.cancel();
                	if(notifications>=100)audioNotificationSwitch.setChecked(true);
                	if(notifications%100>=10)visualNotificationSwitch.setChecked(true);
                	if(notifications%10>=1)tactileNotificationSwitch.setChecked(true);
                }
            });
		}
		else if(categorie.equals(context.getString(R.string.shoppingNotificationRate))){
			try {
				notificationRate = Integer.parseInt(UserPreferences.preferences.getJSONObject(0).getString("notificationRate"));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View convertView = infalInflater.inflate(R.layout.settings_shopping_notification_rate, null);
            
            final SeekBar notificationRateBar = (SeekBar) convertView.findViewById(R.id.shoppingNotificationRateBar);
            notificationRateBar.setMax(120);
            notificationRateBar.setProgress(notificationRate);
            
            TextView notificationRateLabel = (TextView) convertView.findViewById(R.id.shoppingNotificationRateValueLabel);
            notificationRateLabel.setText(context.getString(R.string.settings_notification_rate_label));
            
            final TextView notificationRateValue = (TextView) convertView.findViewById(R.id.shoppingNotificationRateValue);
            notificationRateValue.setText(""+notificationRate);
            
            final TextView notificationRateValueUnit = (TextView) convertView.findViewById(R.id.shoppingNotificationRateValueUnit);
            notificationRateValueUnit.setText(context.getString(R.string.settings_notification_rate_unit));
            
            notificationRateBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					// TODO Auto-generated method stub
					notificationRateValue.setText(""+progress);
					
				}
			});
            builder.setView(convertView);
            builder.setPositiveButton(R.string.btnOk, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                	notificationRate = Integer.parseInt(notificationRateValue.getText().toString());
                	userPreferences.setPreferenceByCategorie("notificationRate", ""+notificationRate);
                	changedPreferences = true;
                }
            });
            	builder.setNegativeButton(R.string.btnCancel, new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                	notificationRateBar.setProgress(Integer.parseInt(userPreferences.getPreferencesByCategory("notificationRate")));
                }
            });
		}
		return builder.create();
	}

	

	
}
