/**
 * 
 */
package de.uulm.levazu.library;

/**
 * Representation of a shop
 * 
 * @author Michael Legner
 */
public class Shop {	
	
	private String name;
	private Double lat;
	private Double lng;
	
	// two dimensional array: first name of product,  second price of product
	private String products[][];
	
	/**
	 * empty constructor
	 */
	public Shop(){
	}
	
	/**
	 * Constructor
	 * @param name Name of Shop
	 * @param lat latitude
	 * @param lng longitude
	 * @param products Array of Strings with names of products in shops
	 */
	public Shop (String name, Double lat, Double lng, String products[][] ){
		this.name = name;
		this.lat = lat;
		this.lng = lng;
		this.products = products;
	}
	
	/**
	 * Getter for name attribute
	 * @return name
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * Getter for latitude attribute
	 * @return latitude
	 */
	public Double getLat(){
		return this.lat;
	}
	
	/**
	 * Getter for longitude attribute
	 * @return longitude
	 */
	public Double getLng(){
		return this.lng;
	}
	
	/**
	 * Getter for products
	 * @return Array of Strings with product names
	 */
	public String[][] getProducts(){
		return this.products;
	}
	
	/**
	 * Checks if shop has products
	 * @return true if shop has products, false if not
	 */
	public Boolean hasProducts(){
		if(this.products == null) return false;
		else return true;
	}
}
