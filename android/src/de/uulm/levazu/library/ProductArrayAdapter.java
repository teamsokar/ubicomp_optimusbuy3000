package de.uulm.levazu.library;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import de.uulm.levazu.R;

/**
 * Adapter for products.
 * @author vlad
 *
 */
public class ProductArrayAdapter extends ArrayAdapter<Product>
{

	private final Context mContext;
	private final ArrayList<Product> mProducts;
	private final LayoutInflater mInflater;

	public ProductArrayAdapter(Context pContext, ArrayList<Product> pProducts) 
	{
		super(pContext, R.layout.rowlayout, pProducts);
		mContext = pContext;
		mProducts = pProducts;
		mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int pPosition, View pConvertView, ViewGroup pParent) 
	{	
		if (pConvertView == null) {
			pConvertView = mInflater.inflate(R.layout.rowlayout, pParent, false);
		}
		
		TextView label = (TextView) pConvertView.findViewById(R.id.label);
		ImageView icon = (ImageView) pConvertView.findViewById(R.id.icon_product);
		TextView tvAmount = (TextView) pConvertView.findViewById(R.id.amount);
		CheckBox chBox = (CheckBox) pConvertView.findViewById(R.id.checkBox_products_for_recipes);
//		chBox.setChecked(false);
//		chBox.setSelected(false);
		if(pParent.getId()!= R.id.listview_checkout){
			chBox.setFocusable(false);
		}
		else {
			chBox.setVisibility(View.INVISIBLE);
			pConvertView.setClickable(false);
		}
		label.setText(mProducts.get(pPosition).getName());
		tvAmount.setFocusable(false);
		if (mProducts.get(pPosition).isIngredient())
			icon.setImageResource(R.drawable.ingredient);
		else
			icon.setImageResource(R.drawable.product);
		
		
		if(mProducts.get(pPosition).isInBasket()){
			label.setPaintFlags(label.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG );
			label.setTextColor(Color.GRAY);
		}
		else {
			label.setPaintFlags(label.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
			label.setTextColor(Color.BLACK);
		}
		return pConvertView;
	}


	@Override
	public void add(Product object) 
	{
		super.add(object);
		
//		// newly added items should be unchecked
//		int pos = ((ProductArrayAdapter) mShoppinglist.getAdapter()).getPosition(product);
//		View rowView = mShoppinglist.getChildAt(pos);
//		CheckBox chbox = (CheckBox) rowView.findViewById(R.id.checkBox_products_for_recipes);
//		// uncheck
//		if (chbox.isChecked())
//			chbox.setChecked(false);
		

//		this.notifyDataSetChanged();
	}
	
	
	/**
	 * Mark a product.
	 * @param pPosition
	 * @deprecated
	 */
	public void markProduct(int pPosition)
	{
		// TODO
		getItemViewType(pPosition);
		Product product = mProducts.get(pPosition);
		product.getName();

	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Product> getProducts() 
	{
		return mProducts;
	}
	
}
