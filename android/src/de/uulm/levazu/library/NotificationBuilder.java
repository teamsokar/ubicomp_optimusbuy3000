package de.uulm.levazu.library;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import de.uulm.levazu.LoginActivity;
import de.uulm.levazu.R;
import de.uulm.levazu.service.NotificationService.ForwardActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
//import android.net.Uri;
import android.support.v4.app.NotificationCompat;

public class NotificationBuilder {

	Intent nIntent;
	Context nContext;
	static int notificationIndex=0;
	JSONArray preferences;
	int notifications;
	HashMap <String, String> user = new HashMap<String, String>();;
	ForwardActivity category;
	NotificationCompat.Builder notification;
	NotificationManager mNotificationManager;
	
	/**
	 * Creates a notification initiated by NotificationService
	 * @param context Application context
	 * @param u User information like id, email and password
	 * @param c Which activity should open, by clicking the notification
	 * @param loggedInUserPreferences 
	 */
	public NotificationBuilder( Context context, HashMap <String, String> u, ForwardActivity c, JSONArray loggedInUserPreferences) {
		if (context == null) {
			return;
		}
		user = u;
		nContext = context;
		category = c;
		preferences = loggedInUserPreferences;
		
		initiateNotification();
	}
	/**
	 * Building a notification for a certain context, e.g. Shopping notification
	 */
	public void initiateNotification(){

		
		if(category == ForwardActivity.SHOPPINGLIST)
			setupShoppingListNotification();
		else if (category == ForwardActivity.MAP)
			setupMapNotification();
		if(notifications>=100){
			notification.setDefaults(Notification.DEFAULT_SOUND);
			notification.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		}
    	if(notifications%100>=10){
    		notification.setDefaults(Notification.DEFAULT_LIGHTS);
	    	notification.setLights(0xff00ff00, 300, 1000);
    	}
    	if(notifications%10>=1)
    		notification.setDefaults(Notification.DEFAULT_VIBRATE);
		mNotificationManager = (NotificationManager) nContext.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, notification.build());
        notificationIndex++;
	}
	
	private void setupMapNotification(){
		NotificationCompat.Builder notification;
		NotificationManager mNotificationManager;
		PendingIntent nPendingIntent = PendingIntent.getActivity(nContext, 0, new Intent(),PendingIntent.FLAG_UPDATE_CURRENT);
		notification = new NotificationCompat.Builder(nContext)
						.setSmallIcon(R.drawable.ic_launcher)
						.setContentTitle("Look out for: !")
						.setContentText("You are near a shop which is on your shoppingroute!")
						.setContentIntent(nPendingIntent);
		notification.setDefaults(Notification.DEFAULT_SOUND);
		notification.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));			
		notification.setAutoCancel(true);
		mNotificationManager = (NotificationManager) nContext.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, notification.build());
	}
	private void setupShoppingListNotification() {
		Intent intent = new Intent(nContext, LoginActivity.class);
		intent.putExtra("autologin", true);
		intent.putExtra("email", user.get("email"));
		intent.putExtra("password", user.get("password"));
		intent.putExtra("category", category);
		intent.putExtra("text", "You have more than 3 items on your Shoppinglist! How about buying shit?");
		
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP|Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent nPendingIntent = PendingIntent.getActivity(nContext, notificationIndex, intent,PendingIntent.FLAG_UPDATE_CURRENT);
		notification = new NotificationCompat.Builder(nContext)
						.setSmallIcon(R.drawable.ic_launcher)
						.setContentTitle("Shopping Alarm!")
						.setContentText(intent.getExtras().getString("text"))
						.setContentIntent(nPendingIntent);
		notification.setAutoCancel(true);
		try {
			notifications = Integer.parseInt(preferences.getJSONObject(0).getString("notifications"));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
