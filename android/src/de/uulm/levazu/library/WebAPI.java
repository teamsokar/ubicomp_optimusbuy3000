package de.uulm.levazu.library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;


/**
 * 
 * @author vlad
 *
 */
public class WebAPI 
{
	/** URL to the Web-API */
	public static String API_URL = "http://mobile1.informatik.uni-ulm.de/websrv/";
	
	private static String TOKEN = "";
	public static String TO_BUY = "tobuy";
	
	private Context mAppContext;
	private JSONParser mJsonParser;
	
	private UserFunctions mUserFunctions;
	private ShoppingListFunctions mShoppingListFunctions;
	
	
	/**
	 * Class which manages the available requests to the Web-API.
	 * @param pContext
	 */
	public WebAPI(Context pContext)
	{
		mAppContext = pContext;
		mJsonParser = new JSONParser();
		
		mUserFunctions = new UserFunctions();
		mShoppingListFunctions = new ShoppingListFunctions();
	}
	
	
	/**
	 * {@link UserFunctions#loginUser(String, String)}
	 * @param pEmail
	 * @param pPassword
	 * @return User as JSONObject
	 */
	public JSONObject loginUser(String pEmail, String pPassword)
	{
		JSONObject user = mUserFunctions.loginUser(pEmail, pPassword);
		
		if (user == null)
			user = new JSONObject();
		
		try {
            if (user.getString("success") != null) 
            {
                String res = user.getString("success");
                if(Integer.parseInt(res) == 1) 
                	Log.d(getClass().getSimpleName(), user.toString());
            }
            Log.d(getClass().getSimpleName(), "success = 0"); // <-- wird nicht erreicht der code wenn jsonObject.getString schief geht..
		} 
		catch (JSONException e)
		{
			Toast.makeText(mAppContext, "" + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
		}
		
		return user;
	}
	
	
	/**
	 * {@link UserFunctions#logoutUser(Context)}
	 * @return
	 */
	public boolean logoutUser()
	{
		return mUserFunctions.logoutUser(mAppContext);
	}
	
	/**Andy
	 * {@link UserFunctions#isUserLoggedIn(Context)}
	 * @return
	 */
	public boolean isUserLoggedIn(Context context)
	{
		return mUserFunctions.isUserLoggedIn(context);
	}
	
	
	/**
	 * {@link ShoppingListFunctions#getShoppingListById(int)}
	 * @return
	 */
	public JSONObject getShoppingListById(int pId)
	{
		if (!mUserFunctions.isUserLoggedIn(mAppContext))
			return new JSONObject();
		
		//mShoppingListFunctions = new ShoppingListFunctions();
		JSONObject shoppingList = mShoppingListFunctions.getShoppingListById(pId);
		
		return shoppingList;
	}
	
	
	/**
	 * {@link ShoppingListFunctions#getShoppingListByUser(int)}
	 * @param pUserId
	 * @return
	 */
	public JSONObject getShoppingListByUser(int pUserId)
	{
		if (!mUserFunctions.isUserLoggedIn(mAppContext))
			return new JSONObject();
		
		JSONObject shoppingList = mShoppingListFunctions.getShoppingListByUser(pUserId);
		
		return shoppingList;
	}
	
	/**
	 * {@link ShoppingListFunctions#getShoppingListByUser(int,booelan)}
	 * @param pUserId
	 * @param pTags
	 * @return
	 */
	public JSONObject getShoppingListByUser(int pUserId, boolean pTags)
	{
		if (!mUserFunctions.isUserLoggedIn(mAppContext))
			return new JSONObject();
		
		JSONObject shoppingList = mShoppingListFunctions.getShoppingListByUser(pUserId,pTags);
		
		return shoppingList;
	}
	
	
	/**
	 * Fetches the current user out of the SQLite DB 
	 * and returns the shopping list for his user id.
	 * @return ShoppingList as JSONObject 
	 */
	public JSONObject getShoppingListForCurrentUser()
	{
		SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(mAppContext);
        HashMap <String, String> user = db.getUserDetails();
        
        int uid = Integer.parseInt(user.get("uid"));
        Log.d(getClass().getSimpleName(), "User currently logged in: " + uid);
		
        return getShoppingListByUser(uid);
	}
	
	/**
	 * {@link ShoppingListFunctions#getItemsInFridge()}
	 * @return
	 */
	public JSONObject getItemsInFridge()
	{
		if (!mUserFunctions.isUserLoggedIn(mAppContext))
			return null;
		
		JSONObject fridgelist = mShoppingListFunctions.getItemsInFridge();
		
		return fridgelist;
	}
	
	/**
	 * Gets UserPreferences for a given user (uid) and returns a JSONArray 
	 * @param uid
	 * @return
	 */
	public JSONArray getPreferencesByUserId(String uid){
		JSONParser jsonParser = new JSONParser();
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "preferences"));
        params.add(new BasicNameValuePair("id", uid));
        
        JSONObject jsonObject = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
        JSONArray preferences;
        try {
			preferences = jsonObject.getJSONArray("preferences");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
        return preferences;
	}
	
	/**
	 * Trigger for server to update the shoppinglist for a User given by uid
	 * @param uid
	 * @return
	 */
	public JSONObject askShoopinglistForUpdates(String uid){
		JSONParser jsonParser = new JSONParser();
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "askforupdates"));
        params.add(new BasicNameValuePair("id", uid));
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
        return json;
	}
	
	
	/**
	 * {@link ShoppingListFunctions#deleteProductFromProductsTable(Product)}
	 * @param pProduct
	 * @return
	 */
	public boolean deleteProductFromProductsTable(Product pProduct)
	{
		return mShoppingListFunctions.deleteProductFromProductsTable(pProduct);
	}
	
    public JSONParser getJSONParser() 
    {
    	return mJsonParser;
    }
    
    /**
     *{@link ShoppingListFunctions#getUnknownProduct(String, String)}
     * @param pUnknownItem
     * @param pAction
     * @return
     */
    public Product[] getUnknownProduct(String pUnknownItem, String pAction){
    	return mShoppingListFunctions.getUnknownProduct(pUnknownItem, pAction);
    }
    
    /**
     *{@link ShoppingListFunctions#addProductToShoppinglist(Product)}
     * @param pUnknownItem
     * @param pAction
     * @return
     */
    public void addProductToShoppinglist(Product pProduct){
    	mShoppingListFunctions.addProductToShoppinglist(pProduct);
    }
    
    /**
     *{@link ShoppingListFunctions#getPriceOfProduct(Product, int)}
     * @param pUnknownItem
     * @param pAction
     * @return
     */
    public void getPriceOfProduct(Product pProduct, int pShop){
    	mShoppingListFunctions.getPriceOfProduct(pProduct,pShop);
    }
    
    /**
     *{@link ShoppingListFunctions#isProductInShop(Product, int)}
     * @param pUnknownItem
     * @param pAction
     * @return
     */
    public boolean isProductInShop(Product pProduct, int pShop){
    	return mShoppingListFunctions.isProductInShop(pProduct,pShop);
    }
    
    /**
     *{@link ShoppingListFunctions#isCheapestInShop(Product, int)}
     * @param pUnknownItem
     * @param pAction
     * @return
     */
    public boolean isCheapestInShop(Product pProduct, int pShop){
    	return mShoppingListFunctions.isCheapestInShop(pProduct,pShop);
    }
}
