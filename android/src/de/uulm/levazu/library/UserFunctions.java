package de.uulm.levazu.library;

import java.util.ArrayList;
import java.util.List;
 
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
 
import android.content.Context;
import android.util.Log;

/**
 * This class has functions to handle all user events like
 * loginUser()
 * registerUser()
 * getLoginStatus()
 * logoutUser().
 * 
 * @author vlad
 *
 */
public class UserFunctions {
     
    private JSONParser jsonParser;
    
    private static String REGISTER_URL = "http://mobile1.informatik.uni-ulm.de/websrv/";
     
    private static String LOGIN_TAG = "login";
    private static String REGISTER_TAG = "register";
     
    // constructor
    protected UserFunctions(){
        jsonParser = new JSONParser();
    }
     
    /**
     * function does Login Request
     * @param email
     * @param password
     * */
    protected JSONObject loginUser(String email, String password){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", LOGIN_TAG));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("password", password));
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
        // return json
        if (json == null) 
        	Log.e(getClass().getSimpleName(), "json object is null :(");
        // Log.e("JSON", json.toString());
        return json;
    }
     
    /**
     * function make Login Request
     * @param name
     * @param email
     * @param password
     * */
    protected JSONObject registerUser(String name, String email, String password){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", REGISTER_TAG));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("password", password));
         
        // getting JSON Object
        JSONObject json = jsonParser.getJSONFromUrl(REGISTER_URL, params);
        // return json
        return json;
    }
     
    /**
     * Function get Login status
     * */
    protected boolean isUserLoggedIn(Context context){
        SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(context);
        int count = db.getRowCount();
        if(count > 0){
            // user logged in
            return true;
        }
        Log.e(getClass().getSimpleName(), "No user is not logged in!");
        return false;
    }
     
    /**
     * Resets the SQLite table for the users. 
     * So there is no user available afterwards.
     * */
    protected boolean logoutUser(Context context){
    	SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(context);
        db.resetTables();
        return true;
    }
    
    
    protected JSONParser getJSONParser() 
    {
    	return this.jsonParser;
    }
     
}
