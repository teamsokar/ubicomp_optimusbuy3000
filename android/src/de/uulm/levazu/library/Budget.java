package de.uulm.levazu.library;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import de.uulm.levazu.MainActivity;

/**
 * Defines properties of a budget.
 * It consists of the user id and the budget in form of a JSONArray, similar to the response
 * from the server when queried.
 * The advantage is that it needs no additional parsing, can be accessed like a hashmap
 * and, when transferring it to a different activity it can be flattened to a String and from
 * it create a new JSONArray without any additional parsing.  
 * 
 * @author Michael Legner
 */
public class Budget{
	
	private String userid;
	private JSONArray budget;
	private WebAPI webAPI;
	private JSONParser jsonParser;
	
	/**
	 * Setter for budget
	 * @param b budget as JSONArray
	 */
	public void setBudget(JSONArray b){
		budget = b;
	}
	
	/**
	 * Getter for budget
	 * @return budget as JSONArray
	 */
	public JSONArray getBudget(){
		return budget;
	}
	
	/**
	 * empty constructor, does nothing
	 */
	public Budget(){
		
	}
	
	/**
	 * constructor
	 * @param userid id
	 */
	public Budget(int userid){
		new Budget("" + userid);
	}
	
	/**
	 * Getter for userId
	 * @return userid
	 */
	public String getUserId(){
		return userid;
	}
	
	/**
	 * Calculates total budget for category groceries.
	 * @param budget budget object
	 * @return total budget for category groceries as int; in case of an error -1
	 */
	public int getGroceries(Budget budget){
		try {
			return Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gVegetables")) + 
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gFruit")) + 
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gMeat")) + 
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gDairy")) + 
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gBread")) +
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gPasta")) +
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gBeverages")) + 
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gSpirits")) +
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gSpice")) +
			Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gOther"));
		} catch (NumberFormatException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * Constructor.
	 * Gets budget from database and assigns it to a Budget object
	 * @param userid id
	 */
	public Budget(String userid){
		this.userid = userid;
		webAPI = MainActivity.WEBAPI;
		jsonParser = webAPI.getJSONParser();
		
		//get budget from database
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "budget"));
		params.add(new BasicNameValuePair("user", userid)); 
		JSONObject json_object = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
		
		if(json_object == null){
			Log.e(getClass().getSimpleName(), "json_object is null");
		} else {
			try{
				String res = json_object.getString("success");
				if(Integer.parseInt(res) == 1){
					budget = json_object.getJSONArray("budget");
				} else {
					Log.e(getClass().getSimpleName(), "budget query was not successfull");
					this.userid = null;
					this.budget = null;
					return;
				}
				
			}catch(JSONException e){
				e.printStackTrace();
			}
		}
	}
}
