package de.uulm.levazu.library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import de.uulm.levazu.MainActivity;

/**
 * @author Andy
 *
 */
public class UserPreferences {
	
	public static String PREFERENCES_TAG = "preferences";
	public static String SAVE_ACTION = "saveSettings";
	
	private Context context;
	private static String id;
	private JSONObject jsonObject;
	public static JSONArray preferences;
	public static JSONArray oldPreferences;
	WebAPI webAPI;
    JSONParser jsonParser;
    
	public static String getId() {
		return id;
	}
	
	public UserPreferences(Context context){
		this.context = context;
		
		SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(context);
        HashMap <String, String> user = db.getUserDetails();
        
		webAPI = MainActivity.WEBAPI;
		jsonParser = webAPI.getJSONParser();
		
		id = user.get("uid");;
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "preferences"));
        params.add(new BasicNameValuePair("id", id));
        jsonObject = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
        
        if (jsonObject == null) 
        	Log.e(getClass().getSimpleName(), "json object is null :(");
        
        try {
        	//preferences = jsonObject.getJSONArray("preferences");
        	//oldPreferences = new JSONArray();
        	//oldPreferences.put(0, new JSONObject(preferences.getJSONObject(0),null));
        	preferences = jsonObject.getJSONArray("preferences");
        	String preferencesString = preferences.toString();
        	oldPreferences = new JSONArray(preferencesString);
        } catch (JSONException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        }
	}
	
	
	
	/**
	 * Gets the value of a preference given by category
	 * @param Category
	 * @return
	 */
	public String getPreferencesByCategory(String Category){
		try {
			if(preferences!=null)
				return preferences.getJSONObject(0).getString(Category);
		} catch (JSONException e) {
			e.printStackTrace();
			
			
		}
		return null;
	}
	/**
	 * Sets a preference given by category to the given value
	 * @param category
	 * @param value
	 */
	public void setPreferenceByCategorie(String category, String value){
		try {
			preferences.getJSONObject(0).put(category, value);
			System.out.println(preferences.getJSONObject(0));
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			System.out.println("Wert setzten fehlgeschlagen");
			e.printStackTrace();
		}
	}
	
	/**
	 * Resets preferences
	 */
	public void resetPreferences(){
		String oldPreferencesString = oldPreferences.toString();
		try {
			preferences = new JSONArray(oldPreferencesString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Saves new preferences on the Database
	 */
	public void savePreferencesInDB(){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", PREFERENCES_TAG));
        params.add(new BasicNameValuePair("action", SAVE_ACTION));
        params.add(new BasicNameValuePair("id", id));
        try {
			params.add(new BasicNameValuePair("preferences", preferences.getJSONObject(0).toString()));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(context, "Couldn`t send Settings", Toast.LENGTH_SHORT).show();
			return ; 
		}
       
        oldPreferences = preferences;
        
        // getting JSON Object
        jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
	}
	
}
	
