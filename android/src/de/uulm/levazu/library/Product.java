package de.uulm.levazu.library;

import java.io.Serializable;

/**
 * Represents a product.
 * @author vlad
 *
 */
public class Product implements Serializable
{
	private static final long serialVersionUID = 1L;

	/** unique ID of the product */
	private int mId;
	/** unique ID of the shoppinglist to which the product belongs */
	private int mShoppingListId;
	/** unique ID of the productList to which the product belongs */
	private int mProductListId;
	/** Name of the product */
	private String mName;
	/** Name of the product */
	private String mCategory;
	/** Is this product an ingredient ? [0: false 1: true] */
	private int mIsIngredient;
	/** The Price of the product */
	private double mPrice;
	/** The Tag of the product */
	private String[] mTags ;
	/** Array to save which tag was already registred*/
	private boolean[] mRegistredTagIndexes;
	/**True if product is in shopping basket */
	private boolean inBasket = false;
	/**Amount of pieces of a Product in the Basket */
	private int mAmount = 0;
	
	


	public int getmAmount() {
		return mAmount;
	}


	public void setmAmount(int mAmount) {
		this.mAmount = mAmount;
	}


	// Constructors
	public Product(String pName)
	{
		mName = pName;
	}
	
	
	public Product(String pProductId, String pShoppingListId, String pProductName, String pIsIngredient) 
	{
		mId = Integer.parseInt(pProductId);
		mShoppingListId = Integer.parseInt(pShoppingListId);
		mName = pProductName;
		mIsIngredient = Integer.parseInt(pIsIngredient);
	}
	
	public Product(String pProductId, String pShoppingListId, String pProductName, String pIsIngredient, String pPrice)
	{
		this(pProductId, pShoppingListId, pProductName, pIsIngredient);
		mPrice = Double.parseDouble(pPrice);
	}
	
	public Product(String pProductId, String pShoppingListId, String pProductName, String pIsIngredient, String pPrice, String[] pTags)
	{
		this(pProductId, pShoppingListId, pProductName, pIsIngredient, pPrice);
		mTags = pTags;
		//mRegistredTagIndexes = new boolean[mTags.length];
		//for(int i=0;i<mRegistredTagIndexes.length;i++)mRegistredTagIndexes[i]=false;
	}
	
	public Product(String pProductId, String pShoppingListId, String pProductListId, String pProductName, String pProductCategory, String pIsIngredient, String pPrice, String[] pTags)
	{
		this(pProductId, pShoppingListId, pProductName, pIsIngredient, pPrice, pTags);
		mProductListId = Integer.parseInt(pProductListId);
		mCategory = pProductCategory;
	}

	// Getters
	public int getId()
	{
		return mId;
	}
	
	public String getName()
	{
		return mName;
	}
	
	public String getmCategory() {
		return mCategory;
	}


	public void setmCategory(String mCategory) {
		this.mCategory = mCategory;
	}


	public int getmProductListId() 
	{
		return mProductListId;
	}

	public boolean isIngredient()
	{
		if (mIsIngredient == 1)
			return true;
		else
			return false;
	}
	
	public Double getPrice()
	{
		return mPrice;
	}
	
	public void setPrice(double pPrice)
	{
		this.mPrice = pPrice;
	}
	
	public String[] getTags()
	{
		return mTags;
	}
	
	public boolean[] getmRegistredTagIndexes() {
		return mRegistredTagIndexes;
	}

	public void setmRegistredTagIndexes(boolean[] mRegistredTagIndexes) {
		this.mRegistredTagIndexes = mRegistredTagIndexes;
	}
	
	public boolean isInBasket() {
		return inBasket;
	}

	public void setInBasket(boolean inBasket) {
		this.inBasket = inBasket;
	}


	public void addTag(String pProductTag) 
	{
		mTags = (String[]) Helper.resizeArray(mTags);
		mTags[mTags.length-1] = pProductTag;
	}
	
	public boolean containsTag(String pTag){
		for(int i = 0; i< this.mTags.length;i++){
			if(this.mTags[i].equals(pTag)) return true;
		}
		return false;
	}
}
