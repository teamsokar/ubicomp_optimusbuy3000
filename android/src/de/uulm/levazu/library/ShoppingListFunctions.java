package de.uulm.levazu.library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.JsonReader;
import android.util.Log;

/**
 * Provides functions to receive the shopping- and fridge-lists from the websrv
 * @author vlad
 *
 */
public class ShoppingListFunctions 
{	
	/** URL to the Web-API */
	private static String API_URL = "http://mobile1.informatik.uni-ulm.de/websrv/";
	
	private JSONParser jsonParser;
	private UserFunctions userFunctions;
	
	protected ShoppingListFunctions()
	{
//		jsonParser = new JSONParser();
//		userFunctions = new UserFunctions();
		// if I use the same JSONParser, which was used before, the stream is closed..
//		jsonParser = userFunctions.getJSONParser();
	}
	
	
	/**
	 * Returns the shopping list with the given id as JSONObject.
	 * @param pId
	 * @return JSONObject
	 */
	protected JSONObject getShoppingListById(int pId)
	{		
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "shoppinglist"));
        params.add(new BasicNameValuePair("id", ""+pId));
        
        jsonParser = new JSONParser();
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
        
        // return json
        if (json == null) 
        	Log.e(getClass().getSimpleName(), "json object is null :(");
        
		return json;
	}
	
	
	/**
	 * Returns the shopping list for the given user id as JSONObject.
	 * @param pUserId
	 * @return
	 */
	protected JSONObject getShoppingListByUser(int pUserId)
	{        
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "shoppinglist"));
        params.add(new BasicNameValuePair("user", ""+pUserId));

        jsonParser = new JSONParser();
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);

        // return json
        if (json == null) 
        	Log.e(getClass().getSimpleName(), "json object is null :(");
        
        return json;
	}
	
	/**
	 * Returns the shopping list for the given user id with/without tags of the products  as JSONObject.
	 * @param pUserId
	 * @param ptags
	 * @return
	 */
	protected JSONObject getShoppingListByUser(int pUserId, boolean pTags)
	{        
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "shoppinglist"));
        params.add(new BasicNameValuePair("user", ""+pUserId));
        params.add(new BasicNameValuePair("withouttags", ""+pTags));

        jsonParser = new JSONParser();
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);

        // return json
        if (json == null) 
        	Log.e(getClass().getSimpleName(), "json object is null :(");
        
        return json;
	}
	
	
	/**
	 * Returns the items which are located in the fridge.
	 * @return JSONObject
	 */
	protected JSONObject getItemsInFridge()
	{
		// Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "fridge"));
        
        jsonParser = new JSONParser();
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
        
        // return json
        if (json == null) 
        	Log.e(getClass().getSimpleName(), "json object is null :(");
		
        return json;
	}
	
	/**
	 * If the RFID-Tag, which was read, is unknown, you can use this method to start a request
	 * to our map_tags.php, in order to find a product with the given tag in our DB. 
	 * Or you can search for a product name (String). 
	 * @param pUnknownItem can be the name of a product or the value of a RFID-tag 
	 * @param pAction can be "manual" (for Strings) or "automatic" (for read tags)
	 * @return
	 */
	public Product[] getUnknownProduct(String pUnknownItem, String pAction){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "map_tags"));
		params.add(new BasicNameValuePair("action", pAction));
        params.add(new BasicNameValuePair("id", UserPreferences.getId()));
        params.add(new BasicNameValuePair("item", pUnknownItem));
        jsonParser = new JSONParser();
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
        
        // return json
        if (json == null) 
        	Log.e(getClass().getSimpleName(), "json object is null :(");
        
		return createProductFromJSON(json, "item");
    }

	

	/**
	 * Creates products out of the JSONObject from the request to our "shoppinglist_api" (on our websrv)
	 * or from the request to map_tags.php (the source of the JSONObject has to be specified)
	 * @param pJson
	 * @param pSource can be "shoppinglist" or "item" (from map_tags)
	 * @return
	 */
	public static Product[] createProductFromJSON(JSONObject pJson, String pSource) 
	{
		Product[] newProducts;
		try {
			
//			if("map_tags".equals(json.getString("tag")))
//			{	
			String source = pSource;
			
			// if json returns error, e.g. item could not be found 
			if (Integer.parseInt(pJson.getString("error")) == 1)
				return null;

			boolean new_product = true;
			newProducts = new Product[pJson.getJSONArray(source).length()];
			for(int i = 0; i < pJson.getJSONArray(source).length(); i++)
			{
				String product_id = pJson.getJSONArray(source).getJSONObject(i).getString("product_collection_id");
				String product_tag[] = {pJson.getJSONArray(source).getJSONObject(i).getString("tag")};

				for (Product product : newProducts)
				{
					if (product != null  && (product.getId() == Integer.parseInt(product_id)))
					{
						if(!product.containsTag(product_tag[0]))
							product.addTag(product_tag[0]);
						new_product = false;
					}
				}

				if (new_product)
				{
					String shoppinglist_id = "";
					String productlist_id = "";
					if("item".equals(source))
					{
						shoppinglist_id = pJson.getString("shoppinglist_id");
						productlist_id = pJson.getString("productlist_id");
					}
					else if("shoppinglist".equals(source))
					{
						shoppinglist_id = pJson.getJSONArray(source).getJSONObject(i).getString("shoppinglist_id");
						productlist_id = pJson.getJSONArray(source).getJSONObject(i).getString("productlist_id");
					}
					String product_name = pJson.getJSONArray(source).getJSONObject(i).getString("product_name");
					String product_ingredient = pJson.getJSONArray(source).getJSONObject(i).getString("ingredient");
					String product_category = pJson.getJSONArray(source).getJSONObject(i).getString("category");
					String product_price = pJson.getJSONArray(source).getJSONObject(i).getString("price");

					newProducts[i] = new Product(product_id, shoppinglist_id, productlist_id, product_name,product_category, product_ingredient, product_price, product_tag);
				}
				new_product = true;
			}
			for (Product product : newProducts)
			{
				if(product!=null)
					product.setmRegistredTagIndexes(new boolean[product.getTags().length]);
			}	
			return newProducts;
//			}
			// tag = shoppinglist
//			else 
//			{
//				String product_id = json.getString("id");
//				String shoppinglist_id = json.getString("shoppinglist_id");
//				String productlist_id = json.getString("productlist_id");
//				String product_name = json.getString("product_name");
//				String product_ingredient = json.getString("ingredient");
//				String product_price = json.getString("price");
//				String[] product_tags = {json.getString("tag")};
//				newProducts = new Product[1];
//				newProducts[0] = new Product(product_id, shoppinglist_id, productlist_id, product_name, product_ingredient, product_price, product_tags);
//				return newProducts;
//			}
		} 
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Deletes the given product from our products table in our DB.
	 * @return
	 */
	protected boolean deleteProductFromProductsTable(Product pProduct)
	{
		// delete request to our websrv
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "delete_product"));
		params.add(new BasicNameValuePair("product_collection_id", "" + pProduct.getId()));
		params.add(new BasicNameValuePair("id", UserPreferences.getId())); // user id
		
		jsonParser = new JSONParser();
		JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
		
		// check response, if deletion was successfull
		try {
			String res = json.getString("success");
			if(Integer.parseInt(res) == 1)
			{
				return true;
			}
			// error case:
			else
			{
				String err_msg = json.getString("error_msg");
				Log.e(getClass().getSimpleName(), err_msg);
				return false;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return false;
	}
	
	/**
	 * Adds the given product to the shoppinglist of the current user
	 * @param pProduct
	 */
	public void addProductToShoppinglist(Product pProduct){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "add_product"));
        params.add(new BasicNameValuePair("id", UserPreferences.getId()));
        params.add(new BasicNameValuePair("product_collection_id", ""+pProduct.getId()));
        jsonParser = new JSONParser();
        jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
	}
	
	/**
	 * Gets the price of the given product in a specific shop
	 * @param pProduct
	 * @param pShop
	 */
	public void getPriceOfProduct(Product pProduct, int pShop){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "get_price"));
        params.add(new BasicNameValuePair("shop", ""+pShop));
        params.add(new BasicNameValuePair("product_collection_id", ""+pProduct.getId()));
        jsonParser = new JSONParser();
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
        double newPrice;
		try {
			newPrice = Double.parseDouble(json.getString("price"));
			pProduct.setPrice(newPrice);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	/**
	 * Is the given product in the specific shop?
	 * @param pProduct
	 * @param pShop
	 * @return
	 */
	public boolean isProductInShop(Product pProduct, int pShop){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "is_product_in_shop"));
        params.add(new BasicNameValuePair("shop", ""+pShop));
        params.add(new BasicNameValuePair("product_collection_id", ""+pProduct.getId()));
        jsonParser = new JSONParser();
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
		try {
			if("true".equals(json.getString("is_in_shop")))
				return true;
			if("false".equals(json.getString("is_in_shop")))
				return false;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Is pShop the cheapest of all shops for the given product?
	 * @param pProduct
	 * @param pShop
	 * @return
	 */
	public boolean isCheapestInShop(Product pProduct, int pShop){
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "is_cheapest_in_shop"));
        params.add(new BasicNameValuePair("shop", ""+pShop));
        params.add(new BasicNameValuePair("product_collection_id", ""+pProduct.getId()));
        jsonParser = new JSONParser();
        JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
		try {
			if("true".equals(json.getString("is_cheapest_shop")))
				return true;
			if("false".equals(json.getString("is_cheapest_shop")))
				return false;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
}
