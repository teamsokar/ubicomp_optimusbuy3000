package de.uulm.levazu.library;

import java.lang.reflect.Array;

import com.google.android.gms.maps.model.LatLng;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Class of miscellaneous helper functions 
 * @author Michael Legner
 *
 */
public class Helper {
	
	/**
	 * empty constructor, does nothing
	 * only needed to access methods
	 */
	public Helper(){
		
	}	
	
	/**
	 * Method to display toasts at the bottom of the screen
	 * @param msgID id to string in strings.xml
	 * @param context application context
	 */
    public void displayToast(Integer msgID, Context context){
    	int offset = Math.round(35 * context.getResources().getDisplayMetrics().density); //for placement from bottom
    	
    	Toast t = Toast.makeText(context, msgID, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, offset); //displays toast bottom-center of the display
        t.show();
    }
    
    /**
     * Methods to display toasts at the bottom of the screen
     * @param str hard coded string
     * @param context application context
     */
    public void displayToast(String str, Context context){
    	int offset = Math.round(35 * context.getResources().getDisplayMetrics().density); //for placement from bottom
    	
    	Toast t = Toast.makeText(context, str, Toast.LENGTH_SHORT);
        t.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, offset); //displays toast bottom-center of the display
        t.show();
    }
    
    /**
	 * Calculates distance between two points
	 * @param curpos Current position as LatLng
	 * @param dest Destination as LatLng
	 * @return double value with distance in kilometers
	 */
	public Double calcDistance(LatLng curpos, LatLng dest) {
		// euclidean distance - would be correct if earth was a pane...
		/* 
		double dlat = curpos.latitude - dest.latitude;
		double dlng = curpos.longitude - dest.longitude;
		// negate negative distances
		if(dlat < 0) dlat = -dlat;
		if(dlng < 0) dlng = -dlng;
		
		return Math.sqrt((dlat*dlat)+(dlng*dlng)); // good ol' pythagoras
		*/
		
		// harvesine formula, adapted from: http://www.codecodex.com/wiki/Calculate_Distance_Between_Two_Points_on_a_Globe#Java
		double r = 6371.0; // roughly earths mean radius
		double dLat = Math.toRadians(dest.latitude - curpos.latitude);
		double dLng = Math.toRadians(dest.longitude - curpos.longitude);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	         Math.cos(Math.toRadians(curpos.latitude)) * Math.cos(Math.toRadians(dest.latitude)) *
	         Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.asin(Math.sqrt(a));
	    return r * c;
	}
    
	/**
	 * Resizes the given array by one slot.
	 * @param pArray
	 * @author levazu
	 * @return
	 */
	public static Object[] resizeArray(Object[] pArray){
		Object newArray = Array.newInstance(pArray.getClass().getComponentType(), Array.getLength(pArray)+1);
		System.arraycopy(pArray, 0, newArray, 0, Array.getLength(pArray));
		pArray = (Object[]) newArray;
		return pArray;
	}
	
	/**
	 * Builds a String out of an Array
	 * @param pStringArray
	 * @author levazu
	 * @return Array as String with "," separated
	 */
	public static String arrayToString(final String [] pStringArray) {

		// if string array is empty
		if (pStringArray.length < 1)
			return "";
		
		final StringBuilder b = new StringBuilder();

		for(final String str : pStringArray) 
		{
			b.append(str.trim()+",");
		}

		b.deleteCharAt(b.length()-1); // delete last ","
		String trimmed = b.toString().trim(); // remove whitespaces at the end
		
		return trimmed;
	}
}
