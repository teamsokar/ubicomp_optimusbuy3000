package de.uulm.levazu.library;

import java.io.Serializable;

/**
 * Represents a recipe.
 * @author vlad
 *
 */
public class Recipe implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String mRecipeId;
	private String mName;
	private String mRating;
	private String mScore;
	private String[] mWhatToBuy;
	private String mSmallImageURL;
	private int mIngredientsThatMatched;
	
	public Recipe(String pName, String[] pWhatToBuy, String pRecipeId, String pRating, String pScore)
	{
		mName = pName;
		mWhatToBuy = pWhatToBuy;
		mRecipeId = pRecipeId;
		mRating = pRating;
		mScore = pScore;
	}
	
	public String getName() 
	{
		return mName;
	}
	
	public String[] getWhatToBuy()
	{
		return mWhatToBuy;
	}
	
	public String getRecipeId()
	{
		return mRecipeId;
	}
	
	public int getRating()
	{
		return Integer.parseInt(mRating);
	}
	
	public int getScore()
	{
		return Integer.parseInt(mScore);
	}
	
	public String getSmallImageUrl()
	{
		return mSmallImageURL;
	}
	
	public void setSmallImageURL(String pUrl)
	{
		mSmallImageURL = pUrl;
	}
}
