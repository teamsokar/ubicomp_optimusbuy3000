package de.uulm.levazu;

import java.util.Timer;
import java.util.TimerTask;

import de.uulm.levazu.library.RBLService;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BluetoothActivity extends Activity {
	private final static String TAG = BluetoothActivity.class.getSimpleName();

	private Button connectBtn = null;
	private TextView rssiValue = null;
	private TextView rfidText = null;

	@SuppressWarnings("unused") 
	private BluetoothGattCharacteristic characteristicTx = null; // it is needed, removing it will cause errors - eclipse is just to dumb to notice it
	
	
	private RBLService mBluetoothLeService;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothDevice mDevice = null;
	private String mDeviceAddress;

	private boolean flag = true;
	private boolean connState = false;
	private boolean scanFlag = false;

	private byte[] data = new byte[30];
	private static final int REQUEST_ENABLE_BT = 1;
	private static final long SCAN_PERIOD = 2000;
	
	private Object mLeScanCallback = null;
	private boolean ble_suppored = false;

	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,	IBinder service) {
			mBluetoothLeService = ((RBLService.LocalBinder) service).getService();
			if (!mBluetoothLeService.initialize()) {
				Log.e(TAG, "Unable to initialize Bluetooth");
				finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBluetoothLeService = null;
		}
	};

	/**
	 *  receives messages from BLE Shield 
	 */
	private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();

			if (RBLService.ACTION_GATT_DISCONNECTED.equals(action)) {
				Toast.makeText(getApplicationContext(), "Disconnected",	Toast.LENGTH_SHORT).show();
				setButtonDisable();
			} else if (RBLService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
				Toast.makeText(getApplicationContext(), "Connected",Toast.LENGTH_SHORT).show();
				getGattService(mBluetoothLeService.getSupportedGattService());
			} else if (RBLService.ACTION_DATA_AVAILABLE.equals(action)) {
				data = intent.getByteArrayExtra(RBLService.EXTRA_DATA);
				readRFID(data);
			} else if (RBLService.ACTION_GATT_RSSI.equals(action)) {
				displayData(intent.getStringExtra(RBLService.EXTRA_DATA));
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		/* custom layout stuff */
//		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_bluetooth);
//		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.title);

		rssiValue = (TextView) findViewById(R.id.rssiValue);
		rfidText = (TextView) findViewById(R.id.rfidText);

		connectBtn = (Button) findViewById(R.id.connect);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
			ble_suppored = true;
			mLeScanCallback = (BluetoothAdapter.LeScanCallback) new BluetoothAdapter.LeScanCallback() {

				@Override
				public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (device != null) {
								if (device.getName().contains("Shield") || device.getName().contains("Biscuit")) {
									mDevice = device;
								}
							}
						}
					});
				}
			};
			
			
			connectBtn.setOnClickListener(new OnClickListener() {

				/* scan for devices and connect if found on click on button */
				@Override
				public void onClick(View v) {
					boolean on = ((ToggleButton) v).isChecked();
					
					if(on == true){
						connectBLE();
					} else {
						disconnectBLE();
					}					
				}
			});

			if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
				Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

			final BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			mBluetoothAdapter = mBluetoothManager.getAdapter();
			if (mBluetoothAdapter == null) {
				Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT).show();
				finish();
				return;
			}

			Intent gattServiceIntent = new Intent(BluetoothActivity.this, RBLService.class);
			bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
		} else {
			Toast.makeText(this, "BLE only on Android 4.3 or higher", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		

	}
	
	private void connectBLE() {
		if (scanFlag == false) {
			scanLeDevice();

			Timer mTimer = new Timer();
			mTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					if (mDevice != null) {
						mDeviceAddress = mDevice.getAddress();
						mBluetoothLeService.connect(mDeviceAddress);
						scanFlag = true;
					} else {
						runOnUiThread(new Runnable() {
							public void run() {
								Toast toast = Toast
										.makeText(
												BluetoothActivity.this,
												"Couldn't search Ble Shiled device!",
												Toast.LENGTH_SHORT);
								toast.setGravity(0, 0, Gravity.CENTER);
								toast.show();
							}
						});
					}
				}
			}, SCAN_PERIOD);
		}

		System.out.println(connState);
		if (connState == false) {
			Log.d(TAG, "Device Address: " + mDeviceAddress);
			mBluetoothLeService.connect(mDeviceAddress);
		} 
	}
	
	private void disconnectBLE(){
		if(connState == true){
			mBluetoothLeService.disconnect();
			mBluetoothLeService.close();
			setButtonDisable();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}

		registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		
	}

	/* display RSSI */
	private void displayData(String data) {
		if (data != null) {
			rssiValue.setText(data);
		}
	}

	/* get RFID id */
	private void readRFID(byte[] data) {
		String v = new String(data);
		System.out.println("Read string: " + v);
		rfidText.setText(v + "");
	}

	private void setButtonEnable() {
		flag = true;
		connState = true;

		connectBtn.setText("Disconnect");
	}

	private void setButtonDisable() {
		flag = false;
		connState = false;

		connectBtn.setText("Connect");
	}

	private void startReadRssi() {
		new Thread() {
			public void run() {

				while (flag) {
					mBluetoothLeService.readRssi();
					try {
						sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		}.start();
	}

	/* connect to BLE service */
	private void getGattService(BluetoothGattService gattService) {
		if (gattService == null)
			return;

		setButtonEnable();
		startReadRssi();

		characteristicTx = gattService.getCharacteristic(RBLService.UUID_BLE_SHIELD_TX);

		BluetoothGattCharacteristic characteristicRx = gattService.getCharacteristic(RBLService.UUID_BLE_SHIELD_RX);
		mBluetoothLeService.setCharacteristicNotification(characteristicRx,	true);
		mBluetoothLeService.readCharacteristic(characteristicRx);
	}

	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();

		intentFilter.addAction(RBLService.ACTION_GATT_CONNECTED);
		intentFilter.addAction(RBLService.ACTION_GATT_DISCONNECTED);
		intentFilter.addAction(RBLService.ACTION_GATT_SERVICES_DISCOVERED);
		intentFilter.addAction(RBLService.ACTION_DATA_AVAILABLE);
		intentFilter.addAction(RBLService.ACTION_GATT_RSSI);

		return intentFilter;
	}

	/* scan for BLE devices */
	private void scanLeDevice() {
		new Thread() {

			@Override
			public void run() {
				mBluetoothAdapter.startLeScan((BluetoothAdapter.LeScanCallback)mLeScanCallback);

				try {
					Thread.sleep(SCAN_PERIOD);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				mBluetoothAdapter.stopLeScan((BluetoothAdapter.LeScanCallback)mLeScanCallback);
			}
		}.start();
	}

	// callback function for scanning for BLE devices
	
//	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
//
//		@Override
//		public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
//			runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//					if (device != null) {
//						if (device.getName().contains("Shield") || device.getName().contains("Biscuit")) {
//							mDevice = device;
//						}
//					}
//				}
//			});
//		}
//	};
	

	@Override
	protected void onStop() {
		super.onStop();

		flag = false;

		unregisterReceiver(mGattUpdateReceiver);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (ble_suppored && (mServiceConnection != null))
			unbindService(mServiceConnection);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// User chose not to enable Bluetooth.
		if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
			finish();
			return;
		}

		super.onActivityResult(requestCode, resultCode, data);
	}
}
