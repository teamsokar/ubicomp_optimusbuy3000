package de.uulm.levazu.controller;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 
 * @author vlad
 *
 */
public class ShoppingListController implements OnItemClickListener
{

	@Override
	public void onItemClick(AdapterView<?> pParent, View pView, int pPos, long pId) 
	{
		Log.d(getClass().getSimpleName(), "Click on Item \n" + "Pos: " + pPos + " ID: " + pId);
	}

	
}
