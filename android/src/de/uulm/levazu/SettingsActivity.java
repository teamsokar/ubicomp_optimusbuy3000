package de.uulm.levazu;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.Build;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import de.uulm.levazu.library.PreferencesDialogManager;
import de.uulm.levazu.library.ExpandableListAdapter;
import de.uulm.levazu.library.UserPreferences;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

/**
 * Acitivty for setting manamgement
 * @author Andreas Blezu
 *
 */

public class SettingsActivity extends Activity {

	ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    PreferencesDialogManager dialogManager;
    UserPreferences userPreferences;
    
    
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userPreferences = MainActivity.userPreferences;
		setContentView(R.layout.activity_settings);
		// Show the Up button in the action bar.
		// get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
 
        // preparing list data
        prepareListData();
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
 
        // setting list adapter
        expListView.setAdapter(listAdapter);
        dialogManager = new PreferencesDialogManager(userPreferences);
        expListView.setOnChildClickListener(new OnChildClickListener() {
        	 
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {
                dialogManager.setContext(SettingsActivity.this);
                dialogManager.setKategorie(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                //if(userPreferences.getJsonObject() != null)dialogManager.setJson(userPreferences.getJsonObject());
                AlertDialog dialog = dialogManager.createDialog();
                dialog.show();
            	Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition)+ " : "+ listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition), Toast.LENGTH_SHORT).show();
                return false;
            }
        });
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("tag", "preferences"));
//        params.add(new BasicNameValuePair("id", "7"));
//        json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
//        // return json
//        if (json == null) 
//        	Log.e(getClass().getSimpleName(), "json object is null :(");
//        
//        try {
//			json_preferences = json.getJSONArray("preferences");
//			System.out.println(json_preferences.getJSONObject(0).getString("distance"));
//        } catch (JSONException e) {
//        	// TODO Auto-generated catch block
//        	System.out.println("Nix geht");
//        	e.printStackTrace();
//        }
		setupActionBar();
	}
	
	/**
	 * Adds all categories and subcategories to listDataHeader and listDataChild
	 */
	private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
 
        // Adding child data
        listDataHeader.add(getString(R.string.userPref));
        listDataHeader.add(getString(R.string.notification));
        //listDataHeader.add(getString(R.string.budget));
 
        // Adding child data 
        List<String> preferaenzen = new ArrayList<String>();
        preferaenzen.add(getString(R.string.nutritionalStyle));
//        preferaenzen.add(getString(R.string.calories));
        preferaenzen.add(getString(R.string.distance)); //in Kilometer
 
        List<String> nachrichten = new ArrayList<String>();
        nachrichten.add(getString(R.string.shoppingNotification));
        nachrichten.add(getString(R.string.shoppingNotificationRate));
        
        
        
//        List<String> budget = new ArrayList<String>();
//        budget.add(getString(R.string.budgetPlan));
//        budget.add(getString(R.string.budgetMon));
//        budget.add(getString(R.string.budgetTol));
       
 
        listDataChild.put(listDataHeader.get(0), preferaenzen); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nachrichten);
//        listDataChild.put(listDataHeader.get(2), budget);
    }
	
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_settings:
			if(dialogManager.getChangedPreferences()){
				userPreferences.savePreferencesInDB();
				Toast.makeText(getApplicationContext(), "Settings saved", Toast.LENGTH_SHORT).show();
				SettingsActivity.this.finish();
				dialogManager.setChangedPreferences(false);
			}
			else Toast.makeText(getApplicationContext(), "There were no changes made!", Toast.LENGTH_SHORT).show();
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Eventhandler for pressing the menu button "save". 
	 * Opens an AlertDialog in which one can choose to save all changed UserPreferences 
	 * or to reset them. 
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK && dialogManager.getChangedPreferences())
			//this.openOptionsMenu();
		{
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SettingsActivity.this);
	 
				// set title
				alertDialogBuilder.setTitle("Save Preferences");
	 
				// set dialog message
				alertDialogBuilder
					.setMessage("There are unsaved changes in your preferences! Do you like to save or reset them?")
					.setCancelable(false)
					.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							// current activity
							userPreferences.savePreferencesInDB();
							Toast.makeText(getApplicationContext(), "Settings saved", Toast.LENGTH_SHORT).show();
							dialogManager.setChangedPreferences(false);
							SettingsActivity.this.finish();
						}
					  })
					.setNegativeButton("Reset!",new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							userPreferences.resetPreferences();
							dialogManager.setChangedPreferences(false);
							Toast.makeText(getApplicationContext(), "Settings have been reset", Toast.LENGTH_SHORT).show();
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();
				}
		else if(keyCode == KeyEvent.KEYCODE_BACK && !dialogManager.getChangedPreferences())
			SettingsActivity.this.finish();
		else if(keyCode == KeyEvent.KEYCODE_MENU )
			this.openOptionsMenu();
		return true;
	}
}
