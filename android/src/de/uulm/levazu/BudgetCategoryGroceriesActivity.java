package de.uulm.levazu;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.uulm.levazu.library.Budget;
import de.uulm.levazu.library.Helper;
import de.uulm.levazu.library.JSONParser;
import de.uulm.levazu.library.BudgetExceededDialogFragment;
import de.uulm.levazu.library.BudgetExceededDialogFragment.BudgetExceededDialogListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.DialogFragment;
import android.view.Menu;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * activity with budget management for sub-category groceries
 * @author Michael Legner
 *
 */
public class BudgetCategoryGroceriesActivity extends Activity implements BudgetExceededDialogListener{
	
	private static String API_URL = "http://mobile1.informatik.uni-ulm.de/websrv/";
	Helper h = new Helper();
	int groceriesMax = 0;
	int groceriesBudget = 0;
	Budget budget;
	
	//@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		
		TextView title = null;
		setContentView(R.layout.activity_budget_groceries);
		title = (TextView) findViewById(R.id.groceries_title_label);
		title.setText("Groceries");
		
		Bundle extras = getIntent().getExtras();
		String uid = extras.getString("userid");
		
		budget = new Budget(uid);		
		
		final TextView total = (TextView) findViewById(R.id.groceries_total_budget); // at top
		final TextView totalText = (TextView) findViewById(R.id.groceries_total); // bottom before slash
		final TextView totalMaxText = (TextView) findViewById(R.id.groceries_total_max); // bottom after slash
		
		try {
			budget.setBudget(new JSONArray(getIntent().getExtras().getString("budget")));
			
			groceriesMax = Integer.parseInt(budget.getBudget().getJSONObject(0).getString("groceries"));
			//groceriesBudget = totalGroceriesByDBEntires();
			
			totalMaxText.setText(String.valueOf(groceriesMax));
			total.setText(String.valueOf(groceriesMax) + " �");
			
			//init text next to seekbars
			TextView tsbVegetables = (TextView) findViewById(R.id.groceries_vegetables_budget);
			tsbVegetables.setText("" + budget.getBudget().getJSONObject(0).getInt("gVegetables") + " �");
			
			TextView tsbFruit = (TextView) findViewById(R.id.groceries_fruit_budget);
			tsbFruit.setText("" + budget.getBudget().getJSONObject(0).getString("gFruit") + " �");
			
			TextView tsbMeat = (TextView) findViewById(R.id.groceries_meat_budget);
			tsbMeat.setText("" + budget.getBudget().getJSONObject(0).getString("gMeat") + " �");
			
			TextView tsbDairy = (TextView) findViewById(R.id.groceries_dairy_budget);
			tsbDairy.setText("" + budget.getBudget().getJSONObject(0).getString("gDairy") + " �");
			
			TextView tsbBread = (TextView) findViewById(R.id.groceries_bread_budget);
			tsbBread.setText("" + budget.getBudget().getJSONObject(0).getString("gBread") + " �");
			
			TextView tsbPasta = (TextView) findViewById(R.id.groceries_pasta_budget);
			tsbPasta.setText("" + budget.getBudget().getJSONObject(0).getString("gPasta") + " �");
			
			TextView tsbBeverages = (TextView) findViewById(R.id.groceries_beverages_budget);
			tsbBeverages.setText("" + budget.getBudget().getJSONObject(0).getString("gBeverages") + " �");
			
			TextView tsbSpirit = (TextView) findViewById(R.id.groceries_spirits_budget);
			tsbSpirit.setText("" + budget.getBudget().getJSONObject(0).getString("gSpirits") + " �");
			
			TextView tsbSpice = (TextView) findViewById(R.id.groceries_spice_budget);
			tsbSpice.setText("" + budget.getBudget().getJSONObject(0).getString("gSpice") + " �");
			
			TextView tsbOther = (TextView) findViewById(R.id.groceries_other_budget);
			tsbOther.setText("" + budget.getBudget().getJSONObject(0).getString("gSpice") + " �");
			
			// SeekBars + set values
			final SeekBar sbTotal = (SeekBar) findViewById(R.id.groceries_total_seek);
			sbTotal.setMax(groceriesMax * 2);
			//sbTotal.setProgress(totalGroceries()); // has to be done later or will give false results as not all SeekBars are initialized yet			
			sbTotal.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				 
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					if (fromUser) {
						TextView p = (TextView) findViewById(R.id.groceries_total_budget); // top
						p.setText("" + progress + " �");
						
						double div = (double) progress / groceriesBudget;
						//groceriesMax = progress;
						updateSeekBars(div);
						
						totalText.setText(String.valueOf(totalGroceriesBySeekBars()));
						totalMaxText.setText(String.valueOf(progress));
						total.setText(String.valueOf(totalGroceriesBySeekBars()) + " �");
						
						groceriesBudget = progress;
					}					
				}
			});
			
			SeekBar sbVegetables = (SeekBar) findViewById(R.id.groceries_vegetables_seek);
			sbVegetables.setMax((int)Math.floor(groceriesMax/2));
			sbVegetables.setProgress(budget.getBudget().getJSONObject(0).getInt("gVegetables"));
			sbVegetables.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {			
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_vegetables_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});
			
			SeekBar sbFruit = (SeekBar) findViewById(R.id.groceries_fruit_seek);
			sbFruit.setMax((int)Math.floor(groceriesMax/2));
			sbFruit.setProgress(budget.getBudget().getJSONObject(0).getInt("gFruit"));
			sbFruit.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_fruit_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});

			SeekBar sbMeat = (SeekBar) findViewById(R.id.groceries_meat_seek);
			sbMeat.setMax((int)Math.floor(groceriesMax/2));
			sbMeat.setProgress(budget.getBudget().getJSONObject(0).getInt("gMeat"));
			sbMeat.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_meat_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});
 
			SeekBar sbDairy = (SeekBar) findViewById(R.id.groceries_dairy_seek);
			sbDairy.setMax((int)Math.floor(groceriesMax/2));
			sbDairy.setProgress(budget.getBudget().getJSONObject(0).getInt("gDairy"));
			sbDairy.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_dairy_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);				
				}
			});

			SeekBar sbBread = (SeekBar) findViewById(R.id.groceries_bread_seek);
			sbBread.setMax((int)Math.floor(groceriesMax/2));
			sbBread.setProgress(budget.getBudget().getJSONObject(0).getInt("gBread"));
			sbBread.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_bread_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});
			
			SeekBar sbPasta = (SeekBar) findViewById(R.id.groceries_pasta_seek);
			sbPasta.setMax((int)Math.floor(groceriesMax/2));
			sbPasta.setProgress(budget.getBudget().getJSONObject(0).getInt("gPasta"));
			sbPasta.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_pasta_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});

			SeekBar sbBeverages = (SeekBar) findViewById(R.id.groceries_beverages_seek);
			sbBeverages.setMax((int)Math.floor(groceriesMax/2));
			sbBeverages.setProgress(budget.getBudget().getJSONObject(0).getInt("gBeverages"));
			sbBeverages.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_beverages_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});

			SeekBar sbSpirits = (SeekBar) findViewById(R.id.groceries_spirits_seek);
			sbSpirits.setMax((int)Math.floor(groceriesMax/2));
			sbSpirits.setProgress(budget.getBudget().getJSONObject(0).getInt("gSpirits"));
			sbSpirits.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_spirits_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});
			
			SeekBar sbSpice = (SeekBar) findViewById(R.id.groceries_spice_seek);
			sbSpice.setMax((int)Math.floor(groceriesMax/2));
			sbSpice.setProgress(budget.getBudget().getJSONObject(0).getInt("gSpice"));
			sbSpice.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_spice_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});
			
			SeekBar sbOther = (SeekBar) findViewById(R.id.groceries_other_seek);
			sbOther.setMax((int)Math.floor(groceriesMax/2));
			sbOther.setProgress(budget.getBudget().getJSONObject(0).getInt("gOther"));
			sbOther.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// Auto-generated method stub
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					TextView p = (TextView) findViewById(R.id.groceries_other_budget);
					updateSeekBarOnChange(total, totalText, sbTotal, progress, p);
				}
			});
			
			sbTotal.setProgress(totalGroceriesBySeekBars());
			groceriesBudget = sbTotal.getProgress();
		
		} catch (JSONException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}		

		totalText.setText("" + totalGroceriesBySeekBars());
		total.setText("" + totalGroceriesBySeekBars());				
	}

	/**
	 * updates SeekBars percentaged to new max value
	 * @param multi multiplier
	 */
	protected void updateSeekBars(double multi) {
		SeekBar sbVegetables = (SeekBar) findViewById(R.id.groceries_vegetables_seek);
		sbVegetables.setMax((int)Math.floor(groceriesMax/2));
		sbVegetables.setProgress((int)Math.floor(sbVegetables.getProgress() * multi));
		
		SeekBar sbFruit = (SeekBar) findViewById(R.id.groceries_fruit_seek);
		sbFruit.setMax((int)Math.floor(groceriesMax/2));
		sbFruit.setProgress((int)Math.floor(sbFruit.getProgress() * multi));
		
		SeekBar sbMeat = (SeekBar) findViewById(R.id.groceries_meat_seek);
		sbMeat.setMax((int)Math.floor(groceriesMax/2));
		sbMeat.setProgress((int)Math.floor(sbMeat.getProgress() * multi));
		
		SeekBar sbDairy = (SeekBar) findViewById(R.id.groceries_dairy_seek);
		sbDairy.setMax((int)Math.floor(groceriesMax/2));
		sbDairy.setProgress((int)Math.floor(sbDairy.getProgress() * multi));
		
		SeekBar sbBread = (SeekBar) findViewById(R.id.groceries_bread_seek);
		sbBread.setMax((int)Math.floor(groceriesMax/2));
		sbBread.setProgress((int)Math.floor(sbBread.getProgress() * multi));
		
		SeekBar sbPasta = (SeekBar) findViewById(R.id.groceries_pasta_seek);
		sbPasta.setMax((int)Math.floor(groceriesMax/2));
		sbPasta.setProgress((int)Math.floor(sbPasta.getProgress() * multi));
		
		SeekBar sbBeverages = (SeekBar) findViewById(R.id.groceries_beverages_seek);
		sbBeverages.setMax((int)Math.floor(groceriesMax/2));
		sbBeverages.setProgress((int)Math.floor(sbBeverages.getProgress() * multi));
		
		SeekBar sbSpirits = (SeekBar) findViewById(R.id.groceries_spirits_seek);
		sbSpirits.setMax((int)Math.floor(groceriesMax/2));
		sbSpirits.setProgress((int)Math.floor(sbSpirits.getProgress() * multi));
		
		SeekBar sbSpice = (SeekBar) findViewById(R.id.groceries_spice_seek);
		sbSpice.setMax((int)Math.floor(groceriesMax/2));
		sbSpice.setProgress((int)Math.floor(sbSpice.getProgress() * multi));
		
		SeekBar sbOther = (SeekBar) findViewById(R.id.groceries_other_seek);
		sbOther.setMax((int)Math.floor(groceriesMax/2));
		sbOther.setProgress((int)Math.floor(sbOther.getProgress() * multi));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.budget_category, menu);
		return true;
	}
	
	/**
	 * Calculates total of all groceries SeekBars
	 * @return total
	 */
	private int totalGroceriesBySeekBars(){
		int total = 0;
		
		SeekBar sbVegetables = (SeekBar) findViewById(R.id.groceries_vegetables_seek);
		total += sbVegetables.getProgress();
		
		SeekBar sbFruit = (SeekBar) findViewById(R.id.groceries_fruit_seek);
		total += sbFruit.getProgress();
		
		SeekBar sbMeat = (SeekBar) findViewById(R.id.groceries_meat_seek);
		total += sbMeat.getProgress();
		
		SeekBar sbDairy = (SeekBar) findViewById(R.id.groceries_dairy_seek);
		total += sbDairy.getProgress();
		
		SeekBar sbBread = (SeekBar) findViewById(R.id.groceries_bread_seek);
		total += sbBread.getProgress();
		
		SeekBar sbPasta = (SeekBar) findViewById(R.id.groceries_pasta_seek);
		total += sbPasta.getProgress();
		
		SeekBar sbBeverages = (SeekBar) findViewById(R.id.groceries_beverages_seek);
		total += sbBeverages.getProgress();
		
		SeekBar sbSpirits = (SeekBar) findViewById(R.id.groceries_spirits_seek);
		total += sbSpirits.getProgress();
		
		SeekBar sbSpice = (SeekBar) findViewById(R.id.groceries_spice_seek);
		total += sbSpice.getProgress();
		
		SeekBar sbOther = (SeekBar) findViewById(R.id.groceries_other_seek);
		total += sbOther.getProgress();
		
		groceriesBudget = total;
		
		return total;
	}
	
	/**
	 * Calculate total of entries retrieved from database 
	 * @return total
	 */
	@SuppressWarnings("unused")
	private int totalGroceriesByDBEntires(){
		int total = 0;
		
		try {
			total += budget.getBudget().getJSONObject(0).getInt("gVegetables");
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gFruit"));
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gMeat"));
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gDairy"));
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gBread"));
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gPasta"));
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gBeverages"));
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gSpirits"));
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gSpice"));
			total += Integer.parseInt(budget.getBudget().getJSONObject(0).getString("gOther"));
		} catch (JSONException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		
		groceriesBudget = total;
		return total;
	}
	
	/**
	 * callback method when save button in the UI is clicked
	 * gathers all settings from seek bars, puts them in a JSONarray and sends them to the database
	 * @param view View associated with current object
	 */
	public void onSaveButtonClicked(View view){

		TextView totalText = (TextView) findViewById(R.id.groceries_total);
		TextView totalMaxText = (TextView) findViewById(R.id.groceries_total_max);
		
		if(Integer.parseInt(totalText.getText().toString()) > Integer.parseInt(totalMaxText.getText().toString())){
			showBudgetExceededDialog();
		} else {
			saveBudgetToDatabase(false);
		}
	}
	
	/**
	 * shows dialog if current budget is exceeded
	 */
	public void showBudgetExceededDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new BudgetExceededDialogFragment();
        dialog.show(getFragmentManager(), "BudgetExceededDialogFragment");
    }

	/**
     * The dialog fragment receives a reference to this Activity through the
     * Fragment.onAttach() callback, which it uses to call the following methods
     * defined by the NoticeDialogFragment.NoticeDialogListener interface
     * @param dialog Associated DialogFragment
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
    	TextView totalText = (TextView) findViewById(R.id.groceries_total);
    	SeekBar sbTotal = (SeekBar) findViewById(R.id.groceries_total_seek);
    	sbTotal.setProgress(Integer.parseInt(totalText.getText().toString()));
    	saveBudgetToDatabase(true);
    }

    /**
     * Gathers all budget settings for this category and saves them to the database
     * @param newBudgetLimit specifies if a new budget limit was set by user
     */
	private void saveBudgetToDatabase(boolean newBudgetLimit) {
		SeekBar sbVegetables = (SeekBar) findViewById(R.id.groceries_vegetables_seek);
		SeekBar sbFruit = (SeekBar) findViewById(R.id.groceries_fruit_seek);
		SeekBar sbMeat = (SeekBar) findViewById(R.id.groceries_meat_seek);
		SeekBar sbDairy = (SeekBar) findViewById(R.id.groceries_dairy_seek);
		SeekBar sbBread = (SeekBar) findViewById(R.id.groceries_bread_seek);
		SeekBar sbPasta = (SeekBar) findViewById(R.id.groceries_pasta_seek);
		SeekBar sbBeverages = (SeekBar) findViewById(R.id.groceries_beverages_seek);
		SeekBar sbSpirits = (SeekBar) findViewById(R.id.groceries_spirits_seek);
		SeekBar sbSpice = (SeekBar) findViewById(R.id.groceries_spice_seek);
		SeekBar sbOther = (SeekBar) findViewById(R.id.groceries_other_seek);
		
		TextView total = (TextView) findViewById(R.id.groceries_total);		
		
		JSONArray b = budget.getBudget();
		
		try {
			if(newBudgetLimit){
				b.getJSONObject(0).remove("groceries");
				b.getJSONObject(0).put("groceries", Integer.parseInt(total.getText().toString()));
			}
			
			b.getJSONObject(0).remove("gVegetables");
			b.getJSONObject(0).put("gVegetables", sbVegetables.getProgress());
			
			b.getJSONObject(0).remove("gFruits");
			b.getJSONObject(0).put("gFruit", sbFruit.getProgress());
			
			b.getJSONObject(0).remove("gMeat");
			b.getJSONObject(0).put("gMeat", sbMeat.getProgress());
			
			b.getJSONObject(0).remove("gDairy");
			b.getJSONObject(0).put("gDairy", sbDairy.getProgress());
			
			b.getJSONObject(0).remove("gBread");
			b.getJSONObject(0).put("gBread", sbBread.getProgress());
			
			b.getJSONObject(0).remove("gPasta");
			b.getJSONObject(0).put("gPasta", sbPasta.getProgress());
			
			b.getJSONObject(0).remove("gBeverages");
			b.getJSONObject(0).put("gBeverages", sbBeverages.getProgress());
			
			b.getJSONObject(0).remove("gSpirits");
			b.getJSONObject(0).put("gSpirits", sbSpirits.getProgress());
			
			b.getJSONObject(0).remove("gSpice");
			b.getJSONObject(0).put("gSpice", sbSpice.getProgress());
			
			b.getJSONObject(0).remove("gOther");
			b.getJSONObject(0).put("gOther", sbOther.getProgress());
			
			JSONParser jsonparser = new JSONParser();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
	        params.add(new BasicNameValuePair("tag", "budget"));
	        params.add(new BasicNameValuePair("user", b.getJSONObject(0).getString("uid")));
	        params.add(new BasicNameValuePair("action", "save"));
	        params.add(new BasicNameValuePair("budget", b.getJSONObject(0).toString()));
	        
	        JSONObject res = jsonparser.getJSONFromUrl(API_URL, params);
	        
	        if (Integer.parseInt(res.getString("success")) == 1){
	        	h.displayToast(R.string.save_dialog_success, getApplicationContext());
	        } else {
	        	h.displayToast(R.string.save_dialog_failed, getApplicationContext());
	        }
	        
	        // update display of budget limit if it has changed
	        if(newBudgetLimit){
	        	TextView totalMax = (TextView) findViewById(R.id.groceries_total_max);
	        	totalMax.setText(String.valueOf(totalGroceriesBySeekBars()));
	        }
	        	
		} catch (NumberFormatException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
     * The dialog fragment receives a reference to this Activity through the
     * Fragment.onAttach() callback, which it uses to call the following methods
     * defined by the NoticeDialogFragment.NoticeDialogListener interface
     * @param dialog Associated DialogFramegnt
     */
    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    	//displayToast(R.string.save_dialog_failed);
    }

    /**
     * helper function to unify methods when SeekBar needs update after progress was changed
     * @param total TextView displaying total at top
     * @param totalText TextView displaying total at bottom
     * @param sbTotal SeekBar with total
     * @param progress new value to which circle has to be moved
     * @param p TextView indicating progress
     */
    private void updateSeekBarOnChange(final TextView total, final TextView totalText, final SeekBar sbTotal, int progress, TextView p) {
		p.setText("" + progress + " �");
		groceriesBudget = totalGroceriesBySeekBars();
		totalText.setText("" + groceriesBudget);
		sbTotal.setProgress(Integer.parseInt(totalText.getText().toString()));	
		total.setText(totalText.getText() + " �");
	}
}
