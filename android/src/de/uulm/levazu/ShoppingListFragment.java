package de.uulm.levazu;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

import de.uulm.levazu.library.Helper;
import de.uulm.levazu.library.Product;
import de.uulm.levazu.library.ProductArrayAdapter;
import de.uulm.levazu.library.Recipe;
import de.uulm.levazu.service.FridgeListAPI;
import de.uulm.levazu.service.ShoppingListAPI;

/**
 * 
 * @author vlad
 *
 */

@SuppressLint("ValidFragment")
public class ShoppingListFragment extends SherlockFragment implements OnItemClickListener, OnItemLongClickListener
{	
	public interface OnProductSelectedListener 
	{
		public void onProductSelected(int pIndex);
		public void onProductLongPressed(int pIndex);
	}
	
	/** root layout of the fragment */
	private LinearLayout mLinearLayout;
    private int mColour;
    private float mWeight;
    private int marginLeft, marginRight, marginTop, marginBottom;
	
    private ListView mShoppinglist;
    
    private Object[] mProducts = {};
    
    private ArrayList<Product> mProductsAddedForRecipe = new ArrayList<Product>();
    
    private OnProductSelectedListener mListener;
	
    public BroadcastReceiver mReceiver = new BroadcastReceiver() 
    {	
		@Override
		public void onReceive(Context pContext, Intent pIntent) 
		{		
			// hide loading GIF
			((ShoppingListActivity) pContext).showLoadingGif(false);
			
			
			// get fridge list from websrv
			((ShoppingListActivity) pContext).sendIntentTo(FridgeListAPI.class);
			
		
			/**
			 * comment this out, so that the shoppinglist is received every time;
			 * 
			// are there products received before ?
			if (MainActivity.mGlobalProducts != null)
			{
				// set mProducts to mGlobalProducts received before 
				mProducts = MainActivity.mGlobalProducts;
				
				// put mProducts into our ListView 
				displayProducts();
				
				// and the do nothing more
				return;
			}
			*/
			
			// else this is the first receive
			Bundle bundle = pIntent.getExtras();
			if (bundle != null) 
			{
				int resultCode = bundle.getInt(ShoppingListAPI.RESULT);
				if (resultCode == Activity.RESULT_OK) 
				{	
					// unpack bundle, which contains the serialized products from our websrv 
					// via the shoppinglistAPI service
					mProducts = (Object[]) bundle.getSerializable(ShoppingListAPI.SHOPPINGLIST);
					if (mProducts.length < 1) return;
					
					// put mProducts into our ListView 
					displayProducts();
					
					// save products for global access in our APP
					MainActivity.mGlobalProducts = mProducts;
					
					Toast.makeText(getSherlockActivity(), "Request to ShoppingListAPI complete.", Toast.LENGTH_SHORT).show();
				} 
				else 
					Toast.makeText(getSherlockActivity(), "Request to ShoppingListAPI failed!", Toast.LENGTH_SHORT).show();
			}
		}
	};
    
    public ShoppingListFragment() 
	{
		// need a public empty constructor for framework to instantiate
	}
	
	public ShoppingListFragment(int pColour, float pWeight, int pMargin_left,
            int pMargin_right, int pMargin_top, int pMargin_bottom)
	{
		mColour = pColour;
        mWeight = pWeight;
        marginLeft = pMargin_left;
        marginRight = pMargin_right;
        marginTop = pMargin_top;
        marginBottom = pMargin_bottom;
	}

	
	public ListView getmShoppinglist() {
		return mShoppinglist;
	}

	@Override
	public void onCreate(Bundle pSavedInstanceState) 
	{		
		super.onCreate(pSavedInstanceState);
		
		if (pSavedInstanceState != null)
		{
			Bundle savedBundle = pSavedInstanceState.getBundle("SAVED_BUNDLE");
			mProducts = (Object[]) savedBundle.getSerializable(ShoppingListAPI.SHOPPINGLIST);
		}
		
		mLinearLayout = new LinearLayout(getSherlockActivity());
		
		LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, mWeight);
        layoutparams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        
        mLinearLayout.setLayoutParams(layoutparams);  
        mLinearLayout.setBackgroundColor(mColour);
        
        // add XML layout
        View view = getLayoutInflater(pSavedInstanceState).inflate(R.layout.fragment_shoppinglist_view, mLinearLayout, false);        
        mLinearLayout.addView(view);     
        
        Log.d(getClass().getSimpleName(), "ShoppingListFragement created");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{

		return mLinearLayout;
	}	
	
	@Override
	public void onAttach(Activity pActivity) 
	{
		super.onAttach(pActivity);
		Log.d(getClass().getSimpleName(), "onAttach()");
		
		mListener = (OnProductSelectedListener) pActivity;
	}
	
	@Override
	public void onStart() 
	{
		super.onStart();
		Log.d(getClass().getSimpleName(), "onStart()");
		
		// TODO gif-stuff should be replaced by something better^^
		((ShoppingListActivity) getSherlockActivity()).showLoadingGif(true); 
//		Bundle bundle = getArguments();
//		if (bundle != null)
//		{
//			String[] values = (String[]) bundle.get("shoppinglist");
//
//			//Log.d()...
//			//getActivity().getParent();
//			//populateListView(ShoppingListActivity.getValues());
//			populateListView(values);
//			shoppinglist.setOnItemClickListener(this);
//		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		// stop listening to notifications from our shoppinglist service 
		getSherlockActivity().unregisterReceiver(mReceiver);
		
		// save mProducts for hole APP
		MainActivity.mGlobalProducts = mProducts;
		
		Log.d(getClass().getSimpleName(), "onPause()");
	}

	@Override
	public void onResume() {
		super.onResume();
		
		// listen to notifications from our shoppinglist service
		getSherlockActivity().registerReceiver(mReceiver, new IntentFilter(ShoppingListAPI.NOTIFICATION));
		
		Log.d(getClass().getSimpleName(), "onResume()");
	}

	
	/**
	 * Put the given products into our shoppinglist ListView.
	 * @param pProducts
	 */
	private void populateListView(ArrayList<Product> pProducts)
	{
        mShoppinglist = (ListView) mLinearLayout.findViewById(R.id.lstview_shoppinglist);
		final ProductArrayAdapter productAdapter = new ProductArrayAdapter(this.getSherlockActivity(), pProducts);
		((ListView) mShoppinglist).setAdapter(productAdapter);
	}
	
	
	/**
	 * Adds a new product to the shoppinglist.
	 */
	protected void searchNewProduct(View pView)
	{	
		Builder builder = new AlertDialog.Builder(getSherlockActivity());
		builder.setTitle("Add a new product..");
		
		LayoutInflater inflater = (LayoutInflater) getSherlockActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View xmlLayout = inflater.inflate(R.layout.shoppinglist_search_new_product, null);
				
		builder.setView(xmlLayout);
		
		final AlertDialog alertDialog = builder.create();
		
		Button btnSave = (Button) xmlLayout.findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View pView) 
			{ 
				// XXX View v here is just the fucking button..^^ 
				LinearLayout ll  = (LinearLayout) pView.getParent();
				EditText edt = (EditText) ll.findViewById(R.id.txtProduct);
//				((ProductArrayAdapter) mShoppinglist.getAdapter()).add(new Product("-1", "-1", "" + edt.getText(), "1"));
				
				Product[] products = MainActivity.WEBAPI.getUnknownProduct(""+edt.getText(), "manual");
				if (products != null) 
				{
					addNewProduct(products);
					alertDialog.dismiss();
				}
				else{
					Toast.makeText(getSherlockActivity(), "no products found in db", Toast.LENGTH_SHORT).show();
					alertDialog.dismiss();
				}
			}
		});
		
		alertDialog.show();
	}
	
	/**
	 * Dialog for "search new Product"
	 * @param pProducts
	 */
	protected void addNewProduct(final Product [] pProducts)
	{
		Builder builder = new AlertDialog.Builder(getSherlockActivity());
		builder.setTitle("Found some Products:");
		
		LayoutInflater inflater = (LayoutInflater) getSherlockActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View xmlLayout = inflater.inflate(R.layout.shoppinglist_add_new_products, null);
				
		builder.setView(xmlLayout);
		
		final AlertDialog alertDialog = builder.create();
		
		Button btnSave = (Button) xmlLayout.findViewById(R.id.btn_shoppinglist_add_new_products);
		
		final LinearLayout lv_addNewProducts = (LinearLayout) xmlLayout.findViewById(R.id.lv_shoppinglist_add_new_products);
		
		for (int i = 0; i < pProducts.length; i++)
		{
			if (pProducts[i] != null){ // because of duplicates which were deleted
				
				CheckBox chBox = new CheckBox(getSherlockActivity());
				chBox.setTextSize(16);
				chBox.setText(pProducts[i].getName());
				lv_addNewProducts.addView(chBox);
			}
		}
		
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View pView) 
			{ 
				int j =0;
				for (int i = 0; i < pProducts.length; i++)
				{
					if (pProducts[i] != null){ // because of duplicates which were deleted
						if(((CheckBox)lv_addNewProducts.getChildAt(i-j)).isChecked()){
							boolean notInList=true;
							for(int m = 0;m<mProducts.length;m++){
								if(pProducts[i].getId()==((Product)mProducts[m]).getId())
									notInList=false;
							}
							if(notInList){
								insertIntoMProducts(pProducts[i]);
								MainActivity.WEBAPI.addProductToShoppinglist(pProducts[i]);
							}
						}
					}
					else j++;
							
							
					
				}
				alertDialog.dismiss();
			}
		});
		
		alertDialog.show();
	}
	
	/**
	 * XXX TODO
	 * Adds the ingredients of the given recipes to the shoppinglist.
	 * @param pRecipe
	 */
	protected void addWhatToBuy(Object pRecipe)
	{
		Recipe currRecipe = (Recipe) pRecipe;
		Log.i(getClass().getSimpleName(), "add what to buy for : "+ currRecipe.getName());
		
		// remove old products, which belong to the predecessor recipe
		if (!mProductsAddedForRecipe.isEmpty())
		{
			for(Product product : mProductsAddedForRecipe) // XXX avoid duplicates 
			{
				// request to our websrv that this product should be deleted
				boolean deleted = MainActivity.WEBAPI.deleteProductFromProductsTable(product);
				
				if(deleted)
					removeFromMProducts(product);
			
				Log.i(getClass().getSimpleName(), "delete from shoppinglist: " + product.getId() + " " + product.getName());
			}
		}
		
		// clear memorized products, received from recipe
		mProductsAddedForRecipe.clear();
		
		// loop trough all ingredients which need to be bought
		for (String ingredient : currRecipe.getWhatToBuy()) 
		{
			// loops through the products on shoppinglist for the ingredient to check if its already on the shoppinglist
			boolean isAlreadyOnShoppinglist = false;
			for (Object productOnShoppingList : mProducts) 
			{
				// clean String for comparison
				String ingredientTrimmed = ingredient.trim();
				String productTrimmed = ((Product) productOnShoppingList).getName().trim();
				
				// ingredient is on shoppinglist
				if (ingredientTrimmed.equals((productTrimmed)))
					isAlreadyOnShoppinglist = true;
				
				// ingredient is maybe on shoppinglist
				if (ingredientTrimmed.contains((productTrimmed)))
					isAlreadyOnShoppinglist = true;
			}
			
			// get fridge items form fridgelistFragment
			Object[] fridgeItems = ((ShoppingListActivity) getSherlockActivity()).getFridgeItems();

			// loops through the products on fridgelist for the ingredient to check if its already on the shoppinglist
			boolean isAlreadyOnFridgelist = false;
			for (Object productOnFridgelist : fridgeItems) 
			{
				// clean String for comparison
				String ingredientTrimmed = ingredient.trim();
				String productTrimmed = ((Product) productOnFridgelist).getName().trim();

				// ingredient is on fridgelist
				if (ingredientTrimmed.equals((productTrimmed)))
					isAlreadyOnFridgelist = true;

				// ingredient is maybe on fridgelist
				if (ingredientTrimmed.contains((productTrimmed)))
					isAlreadyOnFridgelist = true;
			}
			
			
			// add ingredient if not on shoppinglist and not on fridgelist
			if (!isAlreadyOnShoppinglist && !isAlreadyOnFridgelist)
			{
				Log.w(getClass().getSimpleName(), "should be added to shoppinglist: " + ingredient);
				Product[] ingredientsToBuy = MainActivity.WEBAPI.getUnknownProduct(ingredient, "manual");  // !!! is null, if there is no ingredient with this name is in our DB
				if (ingredientsToBuy != null)
				{	
					//for (Product product : ingredientsToBuy)
					//{
					// ingredientsToBuy can contain duplicate products, so just take the first one
						Product product = ingredientsToBuy[0]; 
					
						// insert
						insertIntoMProducts(product);
						
						// add request for insert to our websrv 
						MainActivity.WEBAPI.addProductToShoppinglist(product);
						
						// memorize added products from the recipe
						mProductsAddedForRecipe.add(product);
						
						Log.i(getClass().getSimpleName(), "added to shoppinglist: " + product.getName());
					//}
				}				
			}
		}	
	}
	
	
	/**
	 * Makes several steps in order to show our products:
	 * Puts mProducts into an ArrayList and passes it to our shoppinglist ListView.
	 * And adds listeners for interactions on our ListView.
	 */
	private boolean displayProducts() 
	{	
		// put mProducts in an ArrayList
		ArrayList<Product> arrProducts = new ArrayList<Product>();
    	for(int i = 0; i < mProducts.length; i++)
    	{
    		Product curr_product = (Product) mProducts[i];
    		arrProducts.add(i, curr_product);
    	}		
    	
    	// put products in our shoppinglist ListView
    	populateListView(arrProducts);
    	
    	// add listener
    	mShoppinglist.setOnItemClickListener(this);
    	mShoppinglist.setOnItemLongClickListener(this);
    	
		return true;
	}
		
	
	@Override
    public void onSaveInstanceState(Bundle pState) 
	{
		super.onSaveInstanceState(pState);
		
		Log.d(getClass().getSimpleName(),"onSaveInstanceState()");
    }

	
	@Override
	public void onItemClick(AdapterView<?> pParent, View pView, int pPos, long pId) 
	{
		Log.d(getClass().getSimpleName(), "Click on Item \n" + "Pos: " + pPos + " ID: " + pId);
		mListener.onProductSelected(pPos);
	}
	
	
	@Override
	public boolean onItemLongClick(AdapterView<?> pAdapter, View pView, int pPos, long pId) 
	{
		Product product = (Product) pAdapter.getItemAtPosition(pPos);
		
		// send delete request to websrv
		boolean deleted = MainActivity.WEBAPI.deleteProductFromProductsTable(product);
		
		if (deleted)
		{
			// remove from mProducts
			removeFromMProducts(product);
			
			// delete from our list.
			((ProductArrayAdapter) mShoppinglist.getAdapter()).remove(product);
			
			// XXX also delete from memorized products from last recipe
			mProductsAddedForRecipe.remove(product);
			
			Toast.makeText(getSherlockActivity(), product.getName() + " (" + product.getId() + ") was deleted", Toast.LENGTH_SHORT).show();
		}
		else
			Toast.makeText(getSherlockActivity(), product.getName() + " could not be deleted", Toast.LENGTH_SHORT).show();
		
		mListener.onProductLongPressed(pPos);
		
		return true;
	}
	

	/**
	 * Returns the names of the products on the shoppinglist. 
	 * You can choose if the returned products should be (eatable) ingredients or not.
	 * @param pIsIngredient (true or false)
	 * @return Names as String array.
	 */
	protected String[] getShoppingListItemNames(boolean pIsIngredient)
	{
		String[] shoppinglistItems = {};
		if (mShoppinglist == null || mShoppinglist.getChildCount() < 1)
			return shoppinglistItems;
			
		ArrayList<String> productsForRecipe = new ArrayList<String>(); 	// names of the products (aka ingredients) for the recipe
		for (int i = 0; i < mShoppinglist.getChildCount(); i++) 
		{
			View rowView = mShoppinglist.getChildAt(i);
			CheckBox chbox = (CheckBox) rowView.findViewById(R.id.checkBox_products_for_recipes);
			if (chbox.isChecked())
			{
				Product currItem = ((ProductArrayAdapter) mShoppinglist.getAdapter()).getItem(i);
				if (currItem.isIngredient() == pIsIngredient)
					productsForRecipe.add(currItem.getName());				
			}
		}
		
		shoppinglistItems = productsForRecipe.toArray(new String[productsForRecipe.size()]);
		
		return shoppinglistItems;
	}
	
	
	/**
	 * Returns the products of the shoppinglist.
	 * @return Objects
	 */
	public Object[] getShoppingListProducts()
	{
		return mProducts;
	}
	
	/**
	 * Returns the products of the shoppinglist.
	 * @return Objects
	 */
	public boolean isShoppingListEmpty()
	{
		if(mProducts.length > 0)
			return false;
		else 
			return true;
	}
	
	
	/**
	 * Insert products into ListView shoppinglist and into mProducts Array.
	 * @param pProduct
	 */
	protected void insertIntoMProducts(Product pProduct)
	{

		mProducts = Helper.resizeArray(mProducts);
		mProducts[mProducts.length-1] = pProduct;
		
		if(mShoppinglist!=null)
		{
			((ProductArrayAdapter) mShoppinglist.getAdapter()).add(pProduct);
		}
		else 
		{
			ArrayList<Product> products = new ArrayList<Product>();
			products.add(pProduct);
			populateListView(products);
		}
	}
	
	/**
	 * Removes a product from our ListView shoppinglist and from our mProducts Array.
	 * @param pProduct
	 */
	public synchronized void removeFromMProducts(Product pProduct)
	{
		Product[] newProductArray = new Product[mProducts.length-1];
		
		int j = 0; // index of the new product array
		for(int i = 0; i < mProducts.length; i++)
		{
			if (!pProduct.equals((Product) mProducts[i])) 
			{
				newProductArray[j] = (Product) mProducts[i];
				j++;
			}
		}
		
		// the reduced new mProducts array
		mProducts = newProductArray;
		
		// delete from our ListView.
		((ProductArrayAdapter) mShoppinglist.getAdapter()).remove(pProduct);
		
		// XXX also delete from memorized products from last recipe !!! -> liefert concurrent exception wenn von what to buy aufgerufen
		//mProductsAddedForRecipe.remove(pProduct);
	}
}
