package de.uulm.levazu;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import pl.droidsonroids.gif.GifDrawable;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import de.uulm.levazu.FridgeListFragment.OnFridgeItemSelectedListener;
import de.uulm.levazu.RecipeFragment.RecipeFragmentListener;
import de.uulm.levazu.ShoppingListFragment.OnProductSelectedListener;
import de.uulm.levazu.library.Budget;
import de.uulm.levazu.library.Helper;
import de.uulm.levazu.library.JSONParser;
import de.uulm.levazu.library.Product;
import de.uulm.levazu.library.ProductArrayAdapter;
import de.uulm.levazu.library.RBLService;
import de.uulm.levazu.library.SQLiteDatabaseHandler;
import de.uulm.levazu.library.WebAPI;
import de.uulm.levazu.service.ShoppingListAPI;
import de.uulm.levazu.service.YummlyAPI;

/**
 * Class which manages the three fragments ShoppingListFragemnt, RecipeFragment and FridgeListFragment.
 * @author vlad
 *
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class ShoppingListActivity extends SherlockFragmentActivity implements OnTouchListener, ActionBar.TabListener, OnProductSelectedListener,
	OnFridgeItemSelectedListener, RecipeFragmentListener
{   
	private static ShoppingListFragment shoppingListFrag;
	private RecipeFragment recipeFrag;
	private FridgeListFragment fridgeListFrag;
	
    private ImageView mGif;
    
    private String[] mOldShoppinglistItems = {};
    private String[] mOldFridgeItems = {};
	
    private boolean useLogo = false;
    private boolean showHomeUp = true;
    
    /** Is needed to stay in landscape mode, when the app resumes from standby. */
    private boolean landscape_before = false;
    private boolean stopped = false;
    
    public static int shop_id = 5;
    private static boolean activityVisible;
    
    /** The current perspective. */
    private PERSPECTIVE mPerspective = PERSPECTIVE.SHOPPINGLIST;
    private static enum PERSPECTIVE 
    {
    	SHOPPINGLIST,
    	SHOPPINGLIST_AND_RECIPES,
    	FRIDGE,
    	RECIPES
    }
    
    // Bluetooth
    @SuppressWarnings("unused") // wird genutzt, blickt eclipse nur nicht
	private BluetoothGattCharacteristic characteristicTx = null;
	private RBLService mBluetoothLeService;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothDevice mDevice = null;
	private String mDeviceAddress;

	private boolean ble_flag = true; // indicate if bluetooth reading is used 
	private boolean connState = false; // indicates status of connection
	private boolean scanFlag = false; // indicates if currently scanning
	private byte[] data = new byte[30];
	private static final int REQUEST_ENABLE_BT = 1;
	private static final long SCAN_PERIOD = 2000;
	
	private boolean ble_supported = false;
	private Object mLeScanCallback = null; //everytime used cast it to  (BluetoothAdapter.LeScanCallback)
	ToggleButton connectBtn;
	Button scanRest;
	
	private Budget budget;
	boolean budgetExceeded = false;
	private Double price = 0.0;
	int currentExpenses=0;
	int maxBudget=0;
	double exceededValue;
	
	/**
	 * establishes connection to BLE service
	 */
	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,	IBinder service) {
			mBluetoothLeService = ((RBLService.LocalBinder) service).getService();
			if (!mBluetoothLeService.initialize()) {
				Log.e(ShoppingListActivity.class.getSimpleName(), "Unable to initialize Bluetooth");
				finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBluetoothLeService = null;
		}
	};
	
	public static boolean isActivityVisible() {
	    return activityVisible;
	  }  

	  public static void activityResumed() {
	    activityVisible = true;
	  }

	  public static void activityPaused() {
	    activityVisible = false;
	  }
	

	/**
	 *  receives messages from BLE Shield 
	 */
	  private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();

			if (RBLService.ACTION_GATT_DISCONNECTED.equals(action)) {
//				Toast.makeText(getApplicationContext(), "Disconnected",	Toast.LENGTH_SHORT).show();
			} else if (RBLService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
				Toast.makeText(getApplicationContext(), "Connected",Toast.LENGTH_SHORT).show();
				getGattService(mBluetoothLeService.getSupportedGattService());
			} else if (RBLService.ACTION_DATA_AVAILABLE.equals(action)) {
				data = intent.getByteArrayExtra(RBLService.EXTRA_DATA);
				readRFID(data);
			} else if (RBLService.ACTION_GATT_RSSI.equals(action)) {
				//displayData(intent.getStringExtra(RBLService.EXTRA_DATA)); // spams notifications with constant updates, only requiered for debugging
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle pSavedInstanceState) 
	{
		Log.d(getClass().getSimpleName(), "ShoppingListActivity on Create");
		super.onCreate(pSavedInstanceState);
		setContentView(R.layout.activity_shopping_list);
		
		// getSupportActionBar()... do something with actionbarsherlock
		final ActionBar actionBar = getSupportActionBar();
		
		// set defaults for logo & home up
		actionBar.setDisplayHomeAsUpEnabled(showHomeUp);
		actionBar.setDisplayUseLogoEnabled(useLogo);
		
		View root_view = findViewById(R.id.root_view);
		root_view.setOnTouchListener(this);
		
		Tab tabShoppinglist = actionBar.newTab().setText("Shopping List").setTabListener(this);
		Tab tabRecipes = actionBar.newTab().setText("Recipes").setTabListener(this);
		
		actionBar.addTab(tabShoppinglist);
		actionBar.addTab(tabRecipes);
		
		// create fragments
		final int MARGIN = 16;
		final int COLOR = Color.parseColor("#F5F5F5");
		
		shoppingListFrag = new ShoppingListFragment(COLOR, 1f, MARGIN, MARGIN, MARGIN, MARGIN);
		recipeFrag = new RecipeFragment(COLOR, 1f, MARGIN, MARGIN, MARGIN, MARGIN);
		fridgeListFrag = new FridgeListFragment(COLOR, 1f, MARGIN, MARGIN, MARGIN, MARGIN);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.root, shoppingListFrag);
        ft.add(R.id.root, recipeFrag);
        ft.add(R.id.root, fridgeListFrag);
        ft.hide(recipeFrag);
        ft.hide(fridgeListFrag);
//      ft.addToBackStack("SHOPPINGLIST");
        ft.commit();  
		
        showTabsNav();
		
        // send intent to our shoppinglist service to get the shoppinglist for the current user
        sendIntentTo(ShoppingListAPI.class);
        	
        
        // Budget
        Bundle extras = getIntent().getExtras();
		String uid = extras.getString("userid");
		budget = new Budget(uid);
		
//		try {
//			Toast.makeText(this, "Budget: " + budget.getBudget().getJSONObject(0).getString("groceries"), Toast.LENGTH_SHORT).show();
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        
        
        // Bluetooth        
        // check if version 4.3 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
        	ble_supported = true;
        	
        	mLeScanCallback = (BluetoothAdapter.LeScanCallback) new BluetoothAdapter.LeScanCallback() {

        			@Override
        			public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
        				runOnUiThread(new Runnable() {
        					@Override
        					public void run() {
        						if (device != null) {
        							if (device.getName().contains("Shield") || device.getName().contains("Biscuit")) {
        								mDevice = device;
        							}
        						}
        					}
        				});
        			}
        		};
        	
			//Bluetooth
        	connectBtn = (ToggleButton) findViewById(R.id.ble_btn);
        	connectBtn.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					boolean on = ((ToggleButton) v).isChecked();
					
					if(on == true){
						connectBLE();
					} else {
						disconnectBLE();
					}
				}
			});			
			
			if (!getPackageManager().hasSystemFeature(
					PackageManager.FEATURE_BLUETOOTH_LE)) {
				Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT)
						.show();
				finish();
			}
			final BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			mBluetoothAdapter = mBluetoothManager.getAdapter();
			if (mBluetoothAdapter == null) {
				Toast.makeText(this, "Ble not supported", Toast.LENGTH_SHORT)
						.show();
				finish();
				return;
			}
			Intent gattServiceIntent = new Intent(ShoppingListActivity.this, RBLService.class);
			bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
		} else {
			Toast.makeText(this, "BLE only on Android 4.3 or higher", Toast.LENGTH_LONG).show();
			connectBtn = (ToggleButton) findViewById(R.id.ble_btn);
			connectBtn.setEnabled(false);
			connectBtn.setFocusable(false);
		}
        
        //Toast.makeText(this, "debug", Toast.LENGTH_SHORT).show();
        initiateCheckout();
        scanRest();
	}
	
	/**
	 * display RSSI value
	 * @param data rssi as string
	 */
	private void displayData(String data) {
		if (data != null) {
			//Toast.makeText(this, "RSSI: " + data, Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * reads id of RFID tag
	 * @param data
	 */
	private void readRFID(byte[] data) {
		String rfidTag= new String(data);
		//System.out.println("Read string: " + rfidTag);
		//Toast.makeText(this, "RFID Tag: " + rfidTag, Toast.LENGTH_SHORT).show();
		markProductWithTag(rfidTag);
	}
	
	/**
	 * starts to periodically read RSSI value
	 */
	private void startReadRssi() {
		new Thread() {
			public void run() {

				while (ble_flag) {
					mBluetoothLeService.readRssi();
					try {
						sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			};
		}.start();
	}
	
	/**
	 * Connect to BLE service
	 * @param gattService 
	 */
	private void getGattService(BluetoothGattService gattService) {
		if (gattService == null)
			return;

		startReadRssi();

		characteristicTx = gattService.getCharacteristic(RBLService.UUID_BLE_SHIELD_TX);

		BluetoothGattCharacteristic characteristicRx = gattService.getCharacteristic(RBLService.UUID_BLE_SHIELD_RX);
		mBluetoothLeService.setCharacteristicNotification(characteristicRx,	true);
		mBluetoothLeService.readCharacteristic(characteristicRx);
	}

	/**
	 * Intent filter for actions from RBLService
	 * @return intent filter
	 */
	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();

		intentFilter.addAction(RBLService.ACTION_GATT_CONNECTED);
		intentFilter.addAction(RBLService.ACTION_GATT_DISCONNECTED);
		intentFilter.addAction(RBLService.ACTION_GATT_SERVICES_DISCOVERED);
		intentFilter.addAction(RBLService.ACTION_DATA_AVAILABLE);
		intentFilter.addAction(RBLService.ACTION_GATT_RSSI);

		return intentFilter;
	}

	/**
	 * scan for BLE devices
	 */
	private void scanLeDevice() {
		new Thread() {

			@Override
			public void run() {
				mBluetoothAdapter.startLeScan((BluetoothAdapter.LeScanCallback)mLeScanCallback);

				try {
					Thread.sleep(SCAN_PERIOD);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				mBluetoothAdapter.stopLeScan((BluetoothAdapter.LeScanCallback)mLeScanCallback);
			}
		}.start();
	}

	/*
	 *  callback function for scanning for BLE devices 
	 */
	/*
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (device != null) {
						if (device.getName().contains("Shield") || device.getName().contains("Biscuit")) {
							mDevice = device;
						}
					}
				}
			});
		}
	};
	*/
	
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (ble_supported && (mServiceConnection != null))
			unbindService(mServiceConnection);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// User chose not to enable Bluetooth.
		if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
			finish();
			return;
		}

		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * Starts the given service and sends an Intent to it.
	 * @param pService
	 */
	protected void sendIntentTo(Class<?> pService)
	{
		// request to get fridge list from websrv
		Intent intent = new Intent(this, pService);
		startService(intent);
		Log.i(getClass().getSimpleName(), "Start " + pService.getSimpleName() + " Service");
	}


	@Override
	public boolean onCreateOptionsMenu(Menu pMenu) 
	{
//		pMenu.add("FridgeList");
		
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.shopping_list, pMenu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem pItem) 
	{
		switch (pItem.getItemId()) 
		{
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
			
		case R.id.action_fridgelist:
			
			Log.d(getClass().getSimpleName(), "action_fridgelist");
			
			showFridgeList();
			
			return true;
		}
		return super.onOptionsItemSelected(pItem);
	}

	@Override
	public boolean onTouch(View v, MotionEvent pEvent) 
	{
		Log.d(getClass().getSimpleName(), "MotionEvent: " + pEvent);
		//getSupportFragmentManager().beginTransaction().hide(shoppingListFrag).commit();
		return false;
	}
	
	
	/**
	 * Displays our Tab navigation on top of the app.
	 */
    private void showTabsNav() 
    {
        ActionBar ab = getSupportActionBar();
        if (ab.getNavigationMode() != ActionBar.NAVIGATION_MODE_TABS) {
            ab.setDisplayShowTitleEnabled(true);
            ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        }
    }
    
    
    /**
     * Arranges our ShoppingListFragment and RecipeFragment in a different way, 
     * when the device is in landscape mode.
     * @return
     */
    private boolean createLandscapeUI()
    {
    	if(mPerspective.equals(PERSPECTIVE.FRIDGE))
    		return true;
    	
    	mPerspective = PERSPECTIVE.SHOPPINGLIST_AND_RECIPES;
    	Log.i(getClass().getSimpleName(), ""+mPerspective);
    	
    	ActionBar ab = getSupportActionBar();
    	ab.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    	
    	FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    	ft.hide(fridgeListFrag);
    	ft.show(shoppingListFrag);
    	ft.show(recipeFrag);
    	ft.commit();
    	
        return true;
    }
    
    /**
     * Shows our ShoppingListFragment.
     * Other Fragments will be hidden.
     * @return
     */
    private boolean showShoppingList()
    {
    	mPerspective = PERSPECTIVE.SHOPPINGLIST;
    	Log.i(getClass().getSimpleName(), ""+mPerspective);
    	
    	FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    	ft.hide(fridgeListFrag);
    	ft.hide(recipeFrag);
    	ft.show(shoppingListFrag);
    	ft.commit();
    	 
    	showTabsNav();
    	
    	return true;
    }
    
    /**
     * Shows our FridgelistFragment.
     * Other Fragments will be hidden.
     * @return
     */
    private void showFridgeList()
    {
    	mPerspective = PERSPECTIVE.FRIDGE;
    	Log.i(getClass().getSimpleName(), ""+mPerspective);
    	
		ActionBar ab = getSupportActionBar();
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    	
    	FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    	ft.hide(shoppingListFrag);
    	ft.hide(recipeFrag);
    	ft.show(fridgeListFrag);
    	ft.commit();		
    }

	
	@Override
	public void onConfigurationChanged(Configuration pNewConfig) 
	{
		Log.d(getClass().getSimpleName(), "onConfigurationChanged(...)");
		super.onConfigurationChanged(pNewConfig);
		
		//if(stopped) return;
		
		int display_pixels = -1;
		
		// Checks the orientation of the screen for landscape and portrait
        if (pNewConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) 
        {        	
        	Log.d(getClass().getSimpleName(), "Orientation -> LANDSCAPE");
        	        	        	
        	landscape_before = createLandscapeUI();
            
        	display_pixels = getResources().getDisplayMetrics().widthPixels;
        	Toast.makeText(this, "landscape " + display_pixels, Toast.LENGTH_SHORT).show();
        }

        else if (pNewConfig.orientation == Configuration.ORIENTATION_PORTRAIT) 
        {
        	Log.d(getClass().getSimpleName(), "Orientation -> PORTRAIT");
        	
        	mPerspective = PERSPECTIVE.SHOPPINGLIST;
        	Log.i(getClass().getSimpleName(), ""+mPerspective);
        	
        	if(!stopped) 
        		showTabsNav();
        	
        	if (mPerspective.equals(PERSPECTIVE.FRIDGE))
        		showFridgeList();

        	display_pixels = getResources().getDisplayMetrics().widthPixels;
        	Toast.makeText(this, "portrait " + display_pixels, Toast.LENGTH_SHORT).show();
            
            landscape_before = false;
        }        
	}

	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		Log.d(getClass().getSimpleName(), "onRestoreInstanceState(...)");
	}
	

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(getClass().getSimpleName(), "onPause()");
		ShoppingListActivity.activityPaused();
	}
	

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(getClass().getSimpleName(), "onStop()");
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
			// Bluetooth
			ble_flag = false;
			unregisterReceiver(mGattUpdateReceiver);
		}
		stopped = true;
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.d(getClass().getSimpleName(), "onSaveInstanceState(...)");
	}
	

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(getClass().getSimpleName(), "onResume()");
		ShoppingListActivity.activityResumed();
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
			// Bluetooth
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableBtIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
			registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		}
		
		stopped = false;
	
		if(landscape_before) 
		{
			Log.d(getClass().getSimpleName(), "orientation was landscape before");
			createLandscapeUI();
		}
		else
			showTabsNav();
	}


	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) 
	{
		ActionBar ab = getSupportActionBar();
        if (ab.getNavigationMode() != ActionBar.NAVIGATION_MODE_TABS)
        	return;
        	
		if(tab.getPosition() == 1) // tab recipes 
		{
			Log.d(getClass().getSimpleName(), "Tab " + tab.getPosition() + " is selected" );
			
			// show the recipe fragment
			ft.hide(shoppingListFrag);
			ft.hide(fridgeListFrag);
			ft.show(recipeFrag);

			// get current items in fridge and from shoppinglist
			String[] fridgeItems = fridgeListFrag.getFridgeItemNames(); // !!! maybe not available here, because they have to be loaded (async) before..
			String[] shoppinglistItems = shoppingListFrag.getShoppingListItemNames(true);
			
			// if nothing has changed, then do nothing
			if (Arrays.equals(mOldShoppinglistItems, shoppinglistItems) && Arrays.equals(mOldFridgeItems, fridgeItems))
			{
				Toast.makeText(getApplicationContext(), "selected ingredients have not been changed", Toast.LENGTH_SHORT).show();
				return;
			}
			
			// if there are not enough ingredients selected, then dismiss
			// so there has to be at least one item in the fridge and on the shopplist
			if (shoppinglistItems.length < 1 || fridgeItems.length < 1)
			{
				Toast.makeText(getApplicationContext(), "not enough ingredients selected", Toast.LENGTH_SHORT).show();
				//ab.selectTab(ab.getTabAt(0));
				return;
			}

			// update shoppinglist and fridgeitems
			mOldShoppinglistItems = shoppinglistItems;
			mOldFridgeItems = fridgeItems;

			// Intent request to YummlyAPI service
			Intent intent = new Intent(this, YummlyAPI.class);

			// add infos for the service 		        
			intent.putExtra(YummlyAPI.FRIDGE_LIST, Helper.arrayToString(fridgeItems));
			intent.putExtra(YummlyAPI.GROCERY_LIST, Helper.arrayToString(shoppinglistItems));	        

			startService(intent);
		}
		
		else if(tab.getPosition() == 0) // tab shopping-list
		{
			Log.d(getClass().getSimpleName(), "Tab " + tab.getPosition() + " is selected" );

			ft.hide(recipeFrag);
			ft.hide(fridgeListFrag);
			ft.show(shoppingListFrag);
		}
	}


	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) 
	{
		// 
	}


	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) 
	{
		//	
	}


	@Override
	public void onProductSelected(int pIndex) 
	{
		Log.d(getClass().getSimpleName(), "Product Index: " + pIndex);
		
        // access to shoppinglist
     	ListView lvShoppinglist = (ListView) shoppingListFrag.getView().findViewById(R.id.lstview_shoppinglist);
     	ProductArrayAdapter shoppinglistAdapter = (ProductArrayAdapter) lvShoppinglist.getAdapter();
		
		// selected product on shoppinglist
		Product selectedProduct = shoppinglistAdapter.getItem(pIndex);
		String[] tags = selectedProduct.getTags();
		String tag = "";
		for(int i=0;i<tags.length;i++ ){
			tag += tags[i]+" ";
		}
		// debug
		//Toast.makeText(getApplicationContext(), tag, Toast.LENGTH_SHORT).show();	
	}
	
	@Override
	public void onProductLongPressed(int pIndex) 
	{
		// on long pressed..
	}
	
	/**
	 * Marks a product in the shoppinglist with the given tag.
	 * The tag is received via bluetooth.
	 * @param pTag
	 */
	private void markProductWithTag(String pTag)
	{
		// access to shoppinglist
     	ListView lvShoppinglist = (ListView) shoppingListFrag.getView().findViewById(R.id.lstview_shoppinglist);
     	ProductArrayAdapter shoppinglistAdapter = (ProductArrayAdapter) lvShoppinglist.getAdapter();
     	boolean unknownProduct = true;
     	if(shoppinglistAdapter!=null){
     		for (Product currProduct : shoppinglistAdapter.getProducts())
     		{
     			String[] tags = currProduct.getTags();
     			int amount=0;
     			for(int i=0;i<tags.length;i++){
     				if(pTag.equals(tags[i])){
     					if(!MainActivity.WEBAPI.isProductInShop(currProduct, shop_id)){
     						Toast.makeText(getApplicationContext(), "This product is not available in this shop. You cheated! Get back to start and don`t take 400 Gulden!", Toast.LENGTH_LONG).show();
     						return;
     					}
     					if(!MainActivity.WEBAPI.isCheapestInShop(currProduct, shop_id))
     						Toast.makeText(getApplicationContext(), "This product is cheaper in another shop on your shopping route!", Toast.LENGTH_LONG).show();
 						
     					if(currProduct.getmRegistredTagIndexes()[i]){

     						currProduct.getmRegistredTagIndexes()[i]=false;
     					}
     					else{
     						amount++;
     						currProduct.getmRegistredTagIndexes()[i]=true;
     					}
     					for(int j=0;j<currProduct.getmRegistredTagIndexes().length;j++){
     						if(currProduct.getmRegistredTagIndexes()[j]&& j!=i ){
     							amount++;
     						}
     					}
     					if(amount<=0){
     						currProduct.setInBasket(false);
     						currProduct.setmAmount(amount);
     						highlightProduct(shoppinglistAdapter.getPosition(currProduct),currProduct.isInBasket(),amount);
     					}
     					else {
     						currProduct.setInBasket(true);
     						currProduct.setmAmount(amount);
     						highlightProduct(shoppinglistAdapter.getPosition(currProduct),currProduct.isInBasket(),amount);
     						if(isBudgetExceeded("warning"))budgetWarningNotification();
     					}
     					Log.d(getClass().getSimpleName(), "Tag " + currProduct.getTags() + " = " + pTag);
     					unknownProduct = false;
     				}
     				//else if(currProduct.getmRegistredTagIndexes()[i])amount++;
     			}

     		}
     	}
     	if (unknownProduct)
     	{
     		
     		Product[] product = MainActivity.WEBAPI.getUnknownProduct(pTag, "automatic");
     		if(product != null&&!MainActivity.WEBAPI.isProductInShop(product[0], shop_id)){
     			MainActivity.WEBAPI.deleteProductFromProductsTable(product[0]);
				Toast.makeText(getApplicationContext(), "This product is not available in this shop. You cheated! Get back to start and don`t take 400 Gulden!", Toast.LENGTH_LONG).show();
				return;
     		}
     		if(product != null){
     			shoppingListFrag.insertIntoMProducts(product[0]);
     			product[0].setInBasket(true);
     			product[0].setmAmount(1);
     			//highlightProduct(index, true);
     			if(isBudgetExceeded("warning"))budgetWarningNotification();
     		}
     	}
     	
	}
	
	
	
	
	/**
	 * Highlights the product at the given position on our shoppinglist (ListView).
	 * @param pIndex
	 * @param pIsInBasket
	 */
	private void highlightProduct(int pIndex, boolean pIsInBasket, int amount){
		RelativeLayout ll = (RelativeLayout) findViewById(R.id.root_view);
		ListView shoppinglist = (ListView) ll.findViewById(R.id.lstview_shoppinglist);
		View rowView = shoppinglist.getChildAt(pIndex);

		TextView someText = (TextView)
				rowView.findViewById(R.id.label);
		TextView tvAmount = (TextView) rowView.findViewById(R.id.amount);

		if(pIsInBasket){
			someText.setPaintFlags(someText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG );
			someText.setTextColor(Color.GRAY);
			if(amount>1)
				tvAmount.setText(""+amount+"x");
		}
		else {
			someText.setPaintFlags(someText.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
			someText.setTextColor(Color.BLACK);
			tvAmount.setText("");
		}
	}

	@Override
	public void onFridgeItemSelected(int pIndex) 
	{
		Log.d(getClass().getSimpleName(), "FridgeItem Index: " + pIndex);
	}

	
	@Override
	public void onBackPressed() 
	{
		if (mPerspective.equals(PERSPECTIVE.FRIDGE))
			showShoppingList();
		else
			super.onBackPressed();
	}
	
	/**	
	 * {@link ShoppingListFragment#searchNewProduct(View)}
	 */
	public void addNewProduct(View pView)
	{
		shoppingListFrag.searchNewProduct(pView);
	}
	
	
	/**
	 * Shows or hides a GIF on the ShoppingListActivity main layout.
	 * @param pIsVisible
	 */
	protected void showLoadingGif(boolean pIsVisible)
	{
		if (mGif == null) 
		{
			try {
				// Log.i( getClass().getSimpleName(), "show loading GIF" );
				GifDrawable gifFromAssets = new GifDrawable( getApplicationContext().getAssets(), "loading.gif" );
				
				//  mGif = (ImageView) getView().findViewById(R.id.gif_container);
				mGif = new ImageView(getApplicationContext());
				// mGif.setBackgroundColor(Color.parseColor("#FFFFFF"));
				mGif.setImageDrawable(gifFromAssets);
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		LinearLayout layout = (LinearLayout) findViewById(R.id.frag_shoppinglist_layout);
//		LinearLayout layout = (LinearLayout) getSherlockActivity().findViewById(R.id.root);
//		FrameLayout layout = (FrameLayout) getSherlockActivity().findViewById(R.id.root_framelayout);
//		LinearLayout layout = (LinearLayout) getSherlockActivity().findViewById(R.id.root_linearlayout);
//		RelativeLayout layout = (RelativeLayout) findViewById(R.id.root_view);

		if (pIsVisible && (layout.findViewById(mGif.getId()) == null) && shoppingListFrag.getmShoppinglist() == null)
			layout.addView(mGif);
		else
		{
			// Log.i( getClass().getSimpleName(), "hide loading GIF" );
			layout.removeView(mGif);
		}
	}
	

	@Override
	public void onRecipesUpdated(Object pRecipe) 
	{
		shoppingListFrag.addWhatToBuy(pRecipe);
	}

	/**
	 * 
	 */
	private void connectBLE() {
		if (scanFlag == false) {
			scanLeDevice();

			Timer mTimer = new Timer();
			mTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					if (mDevice != null) {
						mDeviceAddress = mDevice.getAddress();
						mBluetoothLeService.connect(mDeviceAddress);
						scanFlag = true;
					} else {
						runOnUiThread(new Runnable() {
							public void run() {
//								Toast toast = Toast
//										.makeText(
//												ShoppingListActivity.this,
//												"Couldn't search Ble Shiled device!",
//												Toast.LENGTH_SHORT);
//								toast.setGravity(0, 0, Gravity.CENTER);
//								toast.show();
							}
						});
					}
				}
			}, SCAN_PERIOD);
		}
		System.out.println(connState);
		if(connState == false){
			Log.d(ShoppingListActivity.class.getSimpleName(), "Device Address: " + mDeviceAddress); mBluetoothLeService.connect(mDeviceAddress);
		}
		
	}

	/**
	 * 
	 */
	private void disconnectBLE() {
		if (connState == true) {
			mBluetoothLeService.disconnect();
			mBluetoothLeService.close();
		}
	}
	
	/**
	 * dummy for presentation
	 */
	private void scanRest(){
		Button btnScanRest = (Button) findViewById(R.id.btn_scan_rest);
		btnScanRest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String[] tags ={"#C","#B","#G"};
				for(int i =0; i < 3; i++){
					markProductWithTag(tags[i]);
				}
			}
			
		});
	}
	/**
	 * If a user is in the checkout area, an AlertDialog indicates the possibility to buy all the scanned products.
	 * The user will be told, if he forgot any product in the current shop. 
	 * Also the price and an sign for exceeding the budget will be shown.
	 */
	private void initiateCheckout(){
		Button btnCheck = (Button) findViewById(R.id.btn_checkout);
		btnCheck.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View pView) {
				// TODO Auto-generated method stub
				Builder builder = new AlertDialog.Builder(
						ShoppingListActivity.this);
				builder.setTitle("Checkout");
				LayoutInflater infalInflater = (LayoutInflater) ShoppingListActivity.this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View convertView = infalInflater.inflate(R.layout.shoppinglist_checkout, null);
				builder.setView(convertView);
				TextView tvCheckoutAnnotations = (TextView) convertView.findViewById(R.id.tv_checkaout_annotations);
				ListView lvCheckout = (ListView) convertView.findViewById(R.id.listview_checkout);
				TextView tvCheckoutPrice = (TextView) convertView.findViewById(R.id.tv_checkout_price);
				ArrayList<Product> products = new ArrayList<Product>();
				Object[] arrProducts = shoppingListFrag.getShoppingListProducts();
				price = 0.0;
				boolean boughtEverything=true;
				for(int i = 0; i < arrProducts.length; i++)
		    	{
					
		    		Product curr_product = (Product) arrProducts[i];
		    		if(!curr_product.isInBasket()){
		    			if(MainActivity.WEBAPI.isProductInShop(curr_product, shop_id) && MainActivity.WEBAPI.isCheapestInShop(curr_product, shop_id)){
		    				products.add(curr_product);
		    				boughtEverything = false;
		    			}
		    		}
		    		else
		    		{
		    			price +=curr_product.getPrice()*curr_product.getmAmount();
		    		}
		    	}
				
				if(boughtEverything && !shoppingListFrag.isShoppingListEmpty())
					tvCheckoutAnnotations.setText(R.string.checkout_annotations_bought_all_in_shop);
				else if(!boughtEverything)
					tvCheckoutAnnotations.setText(R.string.checkout_annotations_still_things_to_buy);
				else 
					tvCheckoutAnnotations.setText(R.string.checkout_annotations_shopping_list_is_empty);
				
				tvCheckoutPrice.setText(""+price);
				//Add information if Budget is exceeded!
				if(isBudgetExceeded("checkout")) {
					TextView tv_checkout_budget = (TextView) convertView.findViewById(R.id.tv_checkout_budget_warnings);
					tv_checkout_budget.setText("You have exceeded your budget of "+maxBudget+" by "+String.format("%.2f", exceededValue)+". If you continue your Budgets will be updated to the new value");
					tv_checkout_budget.setTextColor(Color.RED);
					budgetExceeded=true;
				}
				else budgetExceeded = false;
				final ProductArrayAdapter productAdapter = new ProductArrayAdapter(ShoppingListActivity.this, products);
				((ListView) lvCheckout).setAdapter(productAdapter);
				builder.setPositiveButton(R.string.checkout_confirm, new DialogInterface.OnClickListener() {
	                @Override
					public void onClick(DialogInterface dialog, int id) {
	                    // User clicked OK button
	                	checkOutConfirm();
	                }

					
	            });
				AlertDialog alertDialog = builder.create();
				alertDialog.show();
				

				// XXX alter first element of shopping list listview.. first step..
				// RelativeLayout ll = (RelativeLayout) pView.getParent();
				// ListView shoppinglist = (ListView)
				// ll.findViewById(R.id.lstview_shoppinglist);
				// View rowView =
				// shoppinglist.getChildAt(shoppinglist.getFirstVisiblePosition());
				//
				// TextView someText = (TextView)
				// rowView.findViewById(R.id.label);
				// someText.setText("Hi! I updated you manually!");
			}
		}); 			
	}
	
	/**
	 * If an user confirms a checkout dialog, the system sends data of the bought products to the server
	 * in order to create a new payment and update the shoppinglist of the user.
	 */
	private void checkOutConfirm() {
		SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(getApplicationContext());	
		
		// create a new payment
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("tag", "payments"));
		params.add(new BasicNameValuePair("action", "save_payment"));
		params.add(new BasicNameValuePair("id", db.getUserDetails().get("uid")));
		
		// gather all needed data of every product
		Object[] mProducts = shoppingListFrag.getShoppingListProducts();
		Product[] productsToDelete = new Product[mProducts.length];
		String[] productIds = new String[mProducts.length]; 
		String[] productAmounts = new String[mProducts.length]; 
		String[] productPrices = new String[mProducts.length]; 
		String[] productCategories = new String[mProducts.length];
		int i = 0;
		for (Object obj : mProducts) {
			Product curr_product = (Product) mProducts[i];
			if(curr_product.isInBasket()){
				productIds[i] = "" + ((Product) obj).getId();
				productsToDelete[i] =  (Product) obj;
				productPrices[i] = ""+((Product) obj).getPrice();
				if(budgetExceeded){
					int oldCategoryBudget;
					int oldBudget;
					try {
						oldCategoryBudget = Integer.parseInt(budget.getBudget().getJSONObject(0).getString(((Product) obj).getmCategory()));
						budget.getBudget().getJSONObject(0).remove(((Product) obj).getmCategory());
						budget.getBudget().getJSONObject(0).put(((Product) obj).getmCategory(), oldCategoryBudget+((Product) obj).getPrice().intValue());
						
						oldBudget = Integer.parseInt(budget.getBudget().getJSONObject(0).getString("groceries"));
						budget.getBudget().getJSONObject(0).remove("groceries");
						budget.getBudget().getJSONObject(0).put("groceries", oldBudget+((Product) obj).getPrice().intValue());
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				int amount=0;
				for(int j=0;j<((Product) obj).getmRegistredTagIndexes().length;j++){
     				if(((Product) obj).getmRegistredTagIndexes()[j] ){
     					amount++;
     				}
     			}
				productAmounts[i] = ""+amount;
				
			}
			else {
				productIds[i] = "" ;
				productAmounts[i] = "";
				productPrices[i]= "";
			}
			i++;
		}
		// if budget is exceeded, save new budget
		if(budgetExceeded) 
			saveNewBudget();
		String ids = Helper.arrayToString(productIds);
		String amounts = Helper.arrayToString(productAmounts);
		String prices="";
		for(int j=0;j<productPrices.length;j++){
			if(j==0) prices +=productPrices[j];
			else prices += ","+productPrices[j];
		}
//		String prices = Helper.arrayToString(productAmounts);
		params.add(new BasicNameValuePair("product_collection_ids", ids));
		params.add(new BasicNameValuePair("product_amounts", amounts));
		params.add(new BasicNameValuePair("product_prices", prices));
		Log.i(getClass().getSimpleName(), ids);

		JSONParser jsonParser = MainActivity.WEBAPI.getJSONParser();
		JSONObject json = jsonParser.getJSONFromUrl(WebAPI.API_URL, params);
		for(int j = 0; j< productsToDelete.length;j++){
			if(productsToDelete[j]!= null){
				shoppingListFrag.removeFromMProducts(productsToDelete[j]);
				
			}
		}
	}
	/**
	 * If an user exceeds the budget, he can save the latest expenses as the new budget.
	 */
	private void saveNewBudget(){
		
        try {
        	JSONParser jsonparser = new JSONParser();
    		List<NameValuePair> params = new ArrayList<NameValuePair>();
        	params.add(new BasicNameValuePair("tag", "budget"));
			params.add(new BasicNameValuePair("user", budget.getBudget().getJSONObject(0).getString("uid")));
			params.add(new BasicNameValuePair("action", "save"));
		    params.add(new BasicNameValuePair("budget",  budget.getBudget().getJSONObject(0).toString()));
		    JSONObject json = jsonparser.getJSONFromUrl(MainActivity.WEBAPI.API_URL, params);
		    System.out.println("hallo");
        } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
        
        
	}
	/**
	 * Creates a notification and notifies the user of an exceeded budget.
	 */
	private void budgetWarningNotification(){

		PendingIntent nPendingIntent = PendingIntent.getActivity(this, 0, new Intent(),PendingIntent.FLAG_UPDATE_CURRENT);
		
		String msg= "You have exceeded your budget of "+maxBudget+" by "+String.format("%.2f", exceededValue)+".";
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		Notification notification = builder.setSmallIcon(R.drawable.ic_launcher)
						.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
						.setContentTitle("Budget warning!")
						.setAutoCancel(true)
						.setDefaults(Notification.DEFAULT_SOUND)
						.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
						.setContentText("Budget is exceeded!")
						.setContentIntent(nPendingIntent).build();
		
		NotificationManager mNotificationManager;
		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, notification);
	}
	
	
	/**
	 * Checks if a recently added product exceeds the budget for its category
	 */
	private boolean isBudgetExceeded(String pMessage){
		price = 0.0;
		exceededValue = 0.0;
		ListView lvShoppinglist = (ListView) shoppingListFrag.getView().findViewById(R.id.lstview_shoppinglist);
     	ProductArrayAdapter shoppinglistAdapter = (ProductArrayAdapter) lvShoppinglist.getAdapter();
		for (Product currProduct : shoppinglistAdapter.getProducts())
     	{
     		if(currProduct.isInBasket()){
     			MainActivity.WEBAPI.getPriceOfProduct(currProduct, shop_id);
     			price +=currProduct.getPrice();
     		}
     	}
     	try {
     		currentExpenses = budget.getGroceries(budget);
     		maxBudget =Integer.parseInt(budget.getBudget().getJSONObject(0).getString("groceries"));
     		exceededValue = (price + currentExpenses) - maxBudget; 
			if(exceededValue>0 ){
				
				return true;
			}
			else return false;
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     	return false;
	}
	
	/**
	 * Returns the Items in the Fridge.
	 * @return
	 */
	public Object[] getFridgeItems()
	{
		return fridgeListFrag.getFridgeItems();
	}
	
	
}
