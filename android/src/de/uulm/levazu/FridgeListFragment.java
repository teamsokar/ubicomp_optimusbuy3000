package de.uulm.levazu;

import java.util.ArrayList;
import java.util.Arrays;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

import de.uulm.levazu.library.Product;
import de.uulm.levazu.library.ProductArrayAdapter;
import de.uulm.levazu.service.FridgeListAPI;

/**
 * Fragment which show the items in our fridge.
 * @author vlad
 *
 */

@SuppressLint("ValidFragment")
public class FridgeListFragment extends SherlockFragment implements OnItemClickListener
{	
	public interface OnFridgeItemSelectedListener 
	{
		public void onFridgeItemSelected(int pIndex);
	}
	
	/** root layout of the fragment */
	private LinearLayout mLinearLayout;
    private int mColour;
    private float mWeight;
    private int marginLeft, marginRight, marginTop, marginBottom;
	
    private ListView mFridgelist;
    
    private Object[] mFridgeItems = {};
    
    private OnFridgeItemSelectedListener mListener;
	
    public BroadcastReceiver mReceiver = new BroadcastReceiver() 
    {	
		@Override
		public void onReceive(Context pContext, Intent pIntent) 
		{
			Bundle bundle = pIntent.getExtras();
			if (bundle != null) 
			{
				int resultCode = bundle.getInt(FridgeListAPI.RESULT);
				if (resultCode == Activity.RESULT_OK) 
				{
					displayProducts(bundle);
					Toast.makeText(getSherlockActivity(), "Request to FridgeListAPI complete.", Toast.LENGTH_SHORT).show();
				} 
				else 
					Toast.makeText(getSherlockActivity(), "Request to FridgeListAPI failed!", Toast.LENGTH_SHORT).show();
			}
		}
	};
    
    public FridgeListFragment() 
	{
		// need a public empty constructor for framework to instantiate
	}
	
	public FridgeListFragment(int pColour, float pWeight, int pMargin_left,
            int pMargin_right, int pMargin_top, int pMargin_bottom)
	{
		mColour = pColour;
        mWeight = pWeight;
        marginLeft = pMargin_left;
        marginRight = pMargin_right;
        marginTop = pMargin_top;
        marginBottom = pMargin_bottom;
	}

	
	@Override
	public void onCreate(Bundle pSavedInstanceState) 
	{		
		super.onCreate(pSavedInstanceState);
		mLinearLayout = new LinearLayout(getSherlockActivity());
		
		LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, mWeight);
        layoutparams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        
        mLinearLayout.setLayoutParams(layoutparams);  
        mLinearLayout.setBackgroundColor(mColour);
        
        // add XML layout
        View view = getLayoutInflater(pSavedInstanceState).inflate(R.layout.fragment_fridgelist_view, mLinearLayout, false);        
        mLinearLayout.addView(view);
        
        Log.d(getClass().getSimpleName(), "" + getClass().getSimpleName() + " created");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		return mLinearLayout;
	}	
	
	@Override
	public void onAttach(Activity activity) 
	{
		super.onAttach(activity);
		Log.d(getClass().getSimpleName(), "onAttach()");
	}
	
	@Override
	public void onStart() 
	{
		super.onStart();
		Log.d(getClass().getSimpleName(), "onStart()");
		
		mListener = (OnFridgeItemSelectedListener) getSherlockActivity();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		getSherlockActivity().unregisterReceiver(mReceiver);
		Log.d(getClass().getSimpleName(), "onPause()");
	}

	@Override
	public void onResume() {
		super.onResume();
		getSherlockActivity().registerReceiver(mReceiver, new IntentFilter(FridgeListAPI.NOTIFICATION));
		Log.d(getClass().getSimpleName(), "onResume()");
	}

	
	private void populateListView(ArrayList<Product> pFridgeItems)
	{
        mFridgelist = (ListView) mLinearLayout.findViewById(R.id.lstview_fridgelist);
		final ProductArrayAdapter productAdapter = new ProductArrayAdapter(this.getSherlockActivity(), pFridgeItems);
		((ListView) mFridgelist).setAdapter(productAdapter);
	}
	
	/**
	 * 
	 * @param pBundle
	 */
	private boolean displayProducts(Bundle pBundle) 
	{
		if (pBundle == null) return false;
		
		mFridgeItems = (Object[]) pBundle.getSerializable(FridgeListAPI.FRIDGELIST);
		if (mFridgeItems.length < 1) return false;
		
		ArrayList<Product> fridgeItems = new ArrayList<Product>();
    	for(int i = 0; i < mFridgeItems.length; i++)
    	{
    		Product curr_fridgeItem = (Product) mFridgeItems[i];
    		fridgeItems.add(i, curr_fridgeItem);
    		//Log.d("FridgeListFragment (BroadcastReceiver)", "FridgeItem " + curr_fridgeItem.getId() + " " + curr_fridgeItem.getName() + " is ingredient ? " + curr_fridgeItem.isIngredient());
    	}
    	
    	populateListView(fridgeItems);
    	
    	mFridgelist.setOnItemClickListener(this);
		
		return true;
	}
	
	@Override
    public void onSaveInstanceState(Bundle outState) 
	{
		//super.onSaveInstanceState(outState);
    }

	@Override
	public void onItemClick(AdapterView<?> pParent, View pView, int pPos, long pId) 
	{
		Log.d(getClass().getSimpleName(), "Click on Item \n" + "Pos: " + pPos + " ID: " + pId);
		mListener.onFridgeItemSelected(pPos);
	}
	
	
	/**
	 * Returns the names of the items in the fridge as a String array.
	 * @return String[]
	 */
	protected String[] getFridgeItemNames()
	{
		String[] fridgeItemNames = {};										// returned array with names as String
		ArrayList<String> fridgeItemsForRecipe = new ArrayList<String>(); 	// names of the products (aka ingredients) selected for the recipe
		
		// !!! if fridgelist has no items/children then it will be null at the moment..
		if (mFridgelist.getChildCount() > 0) // !!! mFridelist has no children in its ListView when it was not painted before
		{
			for (int i = 0; i < mFridgelist.getChildCount(); i++) 				
			{
				View rowView = mFridgelist.getChildAt(i);
				CheckBox chbox = (CheckBox) rowView.findViewById(R.id.checkBox_products_for_recipes);
				if (chbox.isChecked())
					fridgeItemsForRecipe.add(((ProductArrayAdapter) mFridgelist.getAdapter()).getItem(i).getName());
			}
			
			fridgeItemNames = fridgeItemsForRecipe.toArray(new String[fridgeItemsForRecipe.size()]);
		}
		else
		// if fridgelist ListView was not painted before 
		// and therefor no item was selected or de-selected, then just take all fridge items:
		{
			fridgeItemNames = new String[mFridgeItems.length];
			for (int i = 0; i < mFridgeItems.length; i++)
				fridgeItemNames[i] = ((Product) mFridgeItems[i]).getName();
			
			Toast.makeText(getSherlockActivity(), "all item from fride list selected", Toast.LENGTH_SHORT).show();
		}
		
		// the names of the items within the fridge
		return fridgeItemNames;
	}
	
	/**
	 * returns the items on the fridgelist
	 */
	public Object[] getFridgeItems()
	{
		return mFridgeItems;
	}
}
