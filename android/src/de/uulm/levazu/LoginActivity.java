package de.uulm.levazu;

import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import de.uulm.levazu.library.SQLiteDatabaseHandler;
import de.uulm.levazu.library.UserFunctions;
import de.uulm.levazu.library.UserPreferences;
import de.uulm.levazu.library.WebAPI;
import de.uulm.levazu.service.NotificationService;
import de.uulm.levazu.service.NotificationService.ForwardActivity;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity 
{
	Button btnLogin;
	Button btnLoginWithoutInternet;
	EditText txtEmail;
	EditText txtPassword;
	TextView lblErrMsg;
	Bundle autoLoginInformation = new Bundle();
	public static UserPreferences userPreferences;
    // JSON Response node names
    private static String KEY_SUCCESS = "success";
    private static String KEY_ERROR = "error";
    private static String KEY_ERROR_MSG = "error_msg";
    private static String KEY_UID = "id";	// ist hier keine spezielle ID, sondern der primäre Schlüssel (id) aus der table 'users'
    private static String KEY_NAME = "name";
    private static String KEY_EMAIL = "email";
    private static String KEY_CREATED_AT = "created_at";
    private static String KEY_TOKEN = "token";
    private static String KEY_LAT = "lat"; // Wohnung Latitude
    private static String KEY_LNG = "lng"; // Wohnung Longitude
    private static String KEY_DEMOSTEP = "demoStep";
    private static int SHOPPING_NOTIFICATION= 1111;
    private static int GPS_NOTIFICATION= 2222;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		// NetworkOnMainThreadException
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		
		if (MainActivity.WEBAPI == null)
			MainActivity.WEBAPI = new WebAPI(getApplicationContext());
		
        autoLoginInformation = getIntent().getExtras();
		if (autoLoginInformation != null && autoLoginInformation.getBoolean("autologin"))
			loginUser(autoLoginInformation.getString("email"), autoLoginInformation.getString("password"),(ForwardActivity)autoLoginInformation.get("category"));
		
		// Importing all assets like buttons, text fields
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLoginWithoutInternet = (Button) findViewById(R.id.btnLoginWithoutInternet);
        lblErrMsg = (TextView) findViewById(R.id.lblErrorMsg);
        lblErrMsg.setText("");
	
        txtEmail.setText("mail@test.de");
        txtPassword.setText("password");

        
      //start without inet
        btnLoginWithoutInternet.setOnClickListener(new View.OnClickListener() {
        	
			
			@Override
			public void onClick(View view) {
				Intent main_activity = new Intent(getApplicationContext(), MainActivity.class);
                
                // Close all views before launching MainActivity
                main_activity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(main_activity);
                 
                // Close Login Screen
                finish();
			}
		});
        
        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View view) 
            {

                loginUser(txtEmail.getText().toString(),txtPassword.getText().toString(),ForwardActivity.NORMAL);

            }
        });
        
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		setIntent(intent);
		autoLoginInformation = intent.getExtras();
		if (autoLoginInformation != null && autoLoginInformation.getBoolean("autologin"))
			loginUser(autoLoginInformation.getString("email"), autoLoginInformation.getString("password"),(ForwardActivity)autoLoginInformation.get("category"));
	}
	
	/**
	 * Login of a user given by eMail and passWord. Also forwards the user to a certain activity
	 * @param eMail
	 * @param passWord
	 * @param forwardActivity
	 */
	private void loginUser(String eMail, String passWord, ForwardActivity forwardActivity) {
		
		WebAPI webAPI = MainActivity.WEBAPI;
		JSONObject json = webAPI.loginUser(eMail, passWord);
 
		// check for login response
		try {
		    if (json.getString(KEY_SUCCESS) != null) {
		        if(lblErrMsg!=null)lblErrMsg.setText("");
		        String res = json.getString(KEY_SUCCESS);
		        if(Integer.parseInt(res) == 1){
		            // user successfully logged in
		            // Store user details in SQLite Database
		            SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(getApplicationContext());
		            JSONObject json_user = json.getJSONObject("user");
		             
		            // Clear all previous data in database
		            webAPI.logoutUser();
		            db.addUser(json_user.getString(KEY_NAME), json_user.getString(KEY_EMAIL), json.getString(KEY_UID), json_user.getString(KEY_CREATED_AT),json_user.getString(KEY_LAT),json_user.getString(KEY_LNG), "0");                       
		            
		            userPreferences = new UserPreferences(getApplicationContext());
		            
		            Intent main_activity = new Intent(this, MainActivity.class);
		            main_activity.putExtra("email", eMail);
		            main_activity.putExtra("password", passWord);
		            if(autoLoginInformation == null || !autoLoginInformation.getBoolean("autologin")){ 
						startNotificationService(eMail, passWord, json.getString(KEY_UID));
					}
		            else main_activity.putExtra("forward", forwardActivity);
		            // Launch MainActivity Screen
		            // Close all views before launching MainActivity
		            main_activity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		            startActivity(main_activity);
		             
		            // Close Login Screen
		            finish();
		        }else{
		            // Error in login
		        	if(lblErrMsg!=null)lblErrMsg.setText("Incorrect username/password");
		        }
		    }
		} catch (JSONException e) {
		    e.printStackTrace();
		}
	}

	/**
	 * Starts the NotificationService for a user given by eMail, passWord and uId
	 * @param eMail
	 * @param passWord
	 * @param uId
	 */
	private void startNotificationService(String eMail, String passWord, String uId)
	{
		Intent notificationIntent = new Intent(
				getApplicationContext(),
				NotificationService.class);
		notificationIntent.putExtra("id", uId);
		notificationIntent.putExtra("email", eMail);
		notificationIntent.putExtra("password", passWord);
		notificationIntent.putExtra("category", "shoppingNotification");
		PendingIntent shoppingNotificationPendingIntent = PendingIntent
				.getService(this, SHOPPING_NOTIFICATION, notificationIntent,
						PendingIntent.FLAG_UPDATE_CURRENT);
		// registering our pending intent with alarmmanager
		AlarmManager shoppingAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		shoppingAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				10*1000, Long.parseLong(userPreferences.getPreferencesByCategory("notificationRate"))*1000*60, shoppingNotificationPendingIntent);
//		shoppingAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
//				10*1000, 5000, shoppingNotificationPendingIntent);

//		notificationIntent.putExtra("category", "gpsNotification");
//		PendingIntent budgetNotificationPendingIntent = PendingIntent
//				.getService(this, GPS_NOTIFICATION, notificationIntent,
//						PendingIntent.FLAG_UPDATE_CURRENT);
//		AlarmManager budgetAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//		budgetAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
//				10*1000, 15000, budgetNotificationPendingIntent);
		
	}

}
