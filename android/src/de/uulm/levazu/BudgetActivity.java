package de.uulm.levazu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import de.uulm.levazu.library.Budget;
import de.uulm.levazu.library.Helper;
import de.uulm.levazu.library.JSONParser;
import de.uulm.levazu.library.NewBudgetDialogFragment;
import de.uulm.levazu.library.SQLiteDatabaseHandler;
import de.uulm.levazu.library.NewBudgetDialogFragment.NewBudgetDialogListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.DialogFragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;

/**
 * Acitivty for budget manamgement
 * @author Michael Legner
 *
 */
public class BudgetActivity extends Activity implements NewBudgetDialogListener {
	
	private static String API_URL = "http://mobile1.informatik.uni-ulm.de/websrv/";
	
	Budget budget;
	Helper h = new Helper();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_budget);
		// Show the Up button in the action bar.
		setupActionBar();
		
		// get budget from db		
		SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(getApplicationContext());
		HashMap <String, String> user = db.getUserDetails();
		String uid = user.get("uid"); 
		        
		budget = new Budget(uid);
		if(budget.getUserId() == null || budget.getBudget() == null){
			showNewBudgetDialog();		        	
		} else{
		    setup();
		}
	}

	/**
	 * extracted method that sets progress bars to values from database
	 */
	private void setup() {
		try{		        	
			ProgressBar pGroceries = (ProgressBar) findViewById(R.id.bar_groceries);
		    pGroceries.setMax(Integer.parseInt(budget.getBudget().getJSONObject(0).getString("groceries")));
		    pGroceries.setProgress(budget.getGroceries(budget));
		    TextView tGroceries = (TextView) findViewById(R.id.label_groceries_left);
		    tGroceries.setText("" + pGroceries.getProgress() + "/" + Integer.parseInt(budget.getBudget().getJSONObject(0).getString("groceries")) + " �");
		    
		    ProgressBar pHyginie = (ProgressBar) findViewById(R.id.bar_hyginies);
		    pHyginie.setMax(Integer.parseInt(budget.getBudget().getJSONObject(0).getString("hyginie")));
		    TextView tHyginie = (TextView) findViewById(R.id.label_hyginie_left);
		    tHyginie.setText("" + pHyginie.getProgress() + "/" + Integer.parseInt(budget.getBudget().getJSONObject(0).getString("hyginie")) + " �");
		    
		    ProgressBar pHousekeeping = (ProgressBar) findViewById(R.id.bar_housekeeping);
		    pHousekeeping.setMax(Integer.parseInt(budget.getBudget().getJSONObject(0).getString("housekeeping")));
		    TextView tHousekeeping = (TextView) findViewById(R.id.label_housekeeping_left);
		    tHousekeeping.setText("" + pHousekeeping.getProgress() + "/" + Integer.parseInt(budget.getBudget().getJSONObject(0).getString("housekeeping")) + " �");
		    
		    ProgressBar pStationery = (ProgressBar) findViewById(R.id.bar_statioery);
		    pStationery.setMax(Integer.parseInt(budget.getBudget().getJSONObject(0).getString("stationery")));
		    TextView tStationery = (TextView) findViewById(R.id.label_statioery_left);
		    tStationery.setText("" + pStationery.getProgress() + "/" + Integer.parseInt(budget.getBudget().getJSONObject(0).getString("stationery")) + " �");
		    
		    ProgressBar pShoes = (ProgressBar) findViewById(R.id.bar_shoes);
		    pShoes.setMax(Integer.parseInt(budget.getBudget().getJSONObject(0).getString("shoes")));
		    TextView tShoes = (TextView) findViewById(R.id.label_shoes_left);
		    tShoes.setText("" + pShoes.getProgress() + "/" + Integer.parseInt(budget.getBudget().getJSONObject(0).getString("shoes")) + " �");     	
			
		}catch(JSONException e){
			e.printStackTrace();
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.budget, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Display dialog fragment when no budget for user set
	 */
	public void showNewBudgetDialog(){
		DialogFragment dialog = new NewBudgetDialogFragment();
        dialog.show(getFragmentManager(), "NewBudgetDialogFragment");
	}
	
	/**
     * The dialog fragment receives a reference to this Activity through the
     * Fragment.onAttach() callback, which it uses to call the following methods
     * defined by the NoticeDialogFragment.NoticeDialogListener interface
     */
	@Override
	public void onDialogPositiveClick(DialogFragment dialog){	
		SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(getApplicationContext());
		HashMap <String, String> user = db.getUserDetails();
		String uid = user.get("uid"); 
		
		try {
			JSONParser jsonparser = new JSONParser();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("tag", "budget"));
			params.add(new BasicNameValuePair("user", uid));
			params.add(new BasicNameValuePair("action", "new"));
			
			JSONObject res = jsonparser.getJSONFromUrl(API_URL, params);
			
			if (Integer.parseInt(res.getString("success")) == 1){
				h.displayToast(R.string.new_budget_dialog_success, getApplicationContext());
				budget = new Budget(uid);				
			} else {
				h.displayToast(R.string.new_budget_dialog_failed, getApplicationContext());
				finish();
				return;
			}
		} catch (NumberFormatException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		
		setup();
	}
	
	/**
     * The dialog fragment receives a reference to this Activity through the
     * Fragment.onAttach() callback, which it uses to call the following methods
     * defined by the NoticeDialogFragment.NoticeDialogListener interface
     */
	@Override
	public void onDialogNegativeClick(DialogFragment dialog){
		finish();
		return;		
	}
	
	/*  Categories:
	 *  groceries    = 1
	 *  hyginie      = 2
	 *  housekeeping = 3
	 *  stationery   = 4
	 *  shoes        = 5
	 */
	
	/**
	 * start activity to manage sub-catergory groceries
	 * @param view View associated with current object
	 */
	public void openGroceries(View view){
		Intent intent = new Intent(this, BudgetCategoryGroceriesActivity.class);
		intent.putExtra("userid", budget.getUserId());
		intent.putExtra("budget", budget.getBudget().toString());
		startActivity(intent);
	}
	
	/**
	 * stub, should start activity to manage sub-catergory hyginie
	 * @param view View associated with current object
	 */
	public void openHyginie(View view){
		// Method stub
	}
	
	/**
	 * stub, should start activity to manage sub-catergory house keeping
	 * @param view View associated with current object
	 */
	public void openHousekeeping(View view){
		// Method stub
	}
	
	/**
	 * start activity to manage sub-catergory stationery
	 * @param view View associated with current object
	 */
	public void openStationery(View view){
		Intent intent = new Intent(this, BudgetCategoryStationeryActivity.class);
		//intent.putExtra("budget", bStationery);
		startActivity(intent);
	}
	
	/**
	 * stub, should start activity to manage sub-catergory shoes
	 * @param view View associated with current object
	 */
	public void openShoes(View view){
		// Method stub
	}

}
