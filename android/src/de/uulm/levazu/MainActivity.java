package de.uulm.levazu;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import de.uulm.levazu.library.Budget;
import de.uulm.levazu.library.UserPreferences;
import de.uulm.levazu.library.WebAPI;
import de.uulm.levazu.service.NotificationService;
import de.uulm.levazu.service.NotificationService.ForwardActivity;

/**
 * 
 * @author vlad
 *
 */
public class MainActivity extends Activity 
{

	public static WebAPI WEBAPI;
	public static UserPreferences userPreferences;
	
	/**
	 * The current products in our APP and on our shoppinglist.
	 * Accessible from everywhere.
	 */
	public static Object[] mGlobalProducts = null;
	
	Budget budget;
	Bundle forwardInformation;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		userPreferences = LoginActivity.userPreferences;
		budget = new Budget(userPreferences.getId());
		
		
		// NetworkOnMainThreadException
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}
		
		if (MainActivity.WEBAPI == null)
			MainActivity.WEBAPI = new WebAPI(getApplicationContext());
		
		
		forwardInformation = getIntent().getExtras();
		if(forwardInformation != null && forwardInformation.get("forward") != null)
			switch((ForwardActivity)forwardInformation.get("forward")){
				case SHOPPINGLIST: 
					openShoppingList();
					break;
			default:
				break;
			}
//		Drawable drawable = getResources().getDrawable(R.drawable.button_list);
//		drawable.setBounds(0, 0, (int)(drawable.getIntrinsicWidth()*0.5), 
//		                         (int)(drawable.getIntrinsicHeight()*0.5));
//		ScaleDrawable sd = new ScaleDrawable(drawable, 0, 1.0f, 1.0f);
//		Button btn = (Button) findViewById(R.id.button_list);
//		btn.setCompoundDrawables(sd.getDrawable(), null, null, null);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_main:
			Intent notificationIntent = new Intent(getApplicationContext(),NotificationService.class);
//			notificationIntent.putExtra("email", forwardInformation.getString("email"));
//			notificationIntent.putExtra("password", forwardInformation.getString("password"));
			PendingIntent notificationPendingIntent = PendingIntent
					.getService(this, 2222, notificationIntent,
							PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
			am.cancel(notificationPendingIntent);
			NavUtils.navigateUpFromSameTask(this);
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void openShoppingList(View view) {
		openShoppingList();
	}

	private void openShoppingList() {
		Intent intent = new Intent(this, ShoppingListActivity.class);
		intent.putExtra("userid", budget.getUserId());
		startActivity(intent);
	}
	
	public void openMap(View view) {
		openMap();
	}

	private void openMap() {
		Intent intent = new Intent(this, MapActivity.class);
		startActivity(intent);
	}

	public void openBudget(View view) {
		openBudget();
	}

	private void openBudget() {
		Intent intent = new Intent(this, BudgetActivity.class);
		startActivity(intent);
	}

	public void openSettings(View view) {
		openSettings();
	}

	private void openSettings() {
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivity(intent);
	}

	public void openScan(View view) {
		openScan();
	}

	private void openScan() {
		Intent intent = new Intent(this, ScanActivity.class);
		startActivity(intent);
	}
	
	public void openFist(View view) {
		openFist();
	}

	private void openFist() {
		Intent intent = new Intent(this, BluetoothActivity.class);
		startActivity(intent);
	}
}
