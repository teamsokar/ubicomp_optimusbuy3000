package de.uulm.levazu;

// for downloadFromUrl
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
//import java.util.Locale;
//import java.util.Iterator;
import java.util.List;
//import java.util.Map;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
//import org.apache.http.params.CoreConnectionPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.uulm.levazu.library.Helper;
import de.uulm.levazu.library.JSONParser;
import de.uulm.levazu.library.LocationUtils;
import de.uulm.levazu.library.SQLiteDatabaseHandler;
import de.uulm.levazu.library.Shop;
import de.uulm.levazu.library.UserPreferences;
//import de.uulm.levazu.library.WebAPI;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
//import com.google.android.gms.internal.es;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory; //needed if someone feels to mess with the look of markers
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.NotificationCompat;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;

/**
 * activity to display a map of current surroundings of the user.
 * can display shops with items on the users' shopping list and generate a route
 * @author Michael Legner
 *
 */
@SuppressLint("UseSparseArrays")
public class MapActivity extends FragmentActivity 
								implements LocationListener,
						        GooglePlayServicesClient.ConnectionCallbacks,
						        GooglePlayServicesClient.OnConnectionFailedListener {
	
	private LocationClient mLocationClient;
	private LocationRequest mLocationRequest;
	private GoogleMap map;

	private boolean mUpdatesRequested;
	
	// save preferences
	private SharedPreferences mPrefs;
	private SharedPreferences.Editor mEditor;
	
	/* 
	 * done update products_in_shops table to work with product_collection 
	 * done change code to work with table product_collection instead of products
	 * notification if near a shop -> ShoppinglistActivity
	 */
	
	 //static markers for testing
	static final LatLng CoordEselsbergCenter = new LatLng(48.422786, 9.953097); //center of eselsberg, used to set camera in beginning for tests
	static final LatLng CoordMuensterPlatz = new LatLng(48.398526,9.991276);
	double maxdistance = 0.0; // maximum distance, only shops with lower are considered
	double tmpdistance = 0.0; // temporary distance, same as maxdistance but can be set explicitly by user
	
	/* distance scheme:
	 * done - primarily uses home of user as starting point
	 * done alternative: users can explicitly choose a new max distance from their current location
	 */	
	private LatLng home = null;	
	private LatLng CurrentLocation = null;
	private Marker CurrentLocationMarker = null; // to remove and redraw it later
	private HashMap<Integer, Shop> AllShops = null;
	private HashMap<Integer, Shop> UserShops = null; // shops with products of user
	
	private ToggleButton toggleUpdatesButton/*, toggleDrawMarkersButton*/;
	
	Helper h = new Helper();
	private static String API_URL = "http://mobile1.informatik.uni-ulm.de/websrv/";
	public static String DIRECTIONS_API_URL = "http://maps.googleapis.com/maps/api/directions/json";
	//public static String DISTANCE_API_OUTPUT = "json";
	
	Location nearestShopLocation;
	float distanceToNearestShop;
	Shop nearestShop;
	boolean nearestShopNotificationSend = false;
	int demoStep = 0;
	
	
	/**
	 * status codes returned by google directions api
	 * @author mike
	 *
	 */
	private static enum DIRECTIONS_STATUS_CODES {
		OK,
		NOT_FOUND,
		ZERO_RESULTS,
		MAX_WAYPOINTS_EXCEEDED,
		INVALID_REQUEST,
		OVER_QUERY_LIMIT,
		REQUEST_DENIED,
		UNKNOWN_ERROR
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		// Show the Up button in the action bar.
		setupActionBar();
		
		// Create a new global location parameters object
        mLocationRequest = LocationRequest.create();

        // Set the update interval
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        // Note that location updates are off until the user turns them on
        mUpdatesRequested = false;

        // Open Shared Preferences
        mPrefs = getSharedPreferences(LocationUtils.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        // Get an editor
        mEditor = mPrefs.edit();

        // Create a new location client, using the enclosing class to handle callbacks.
        mLocationClient = new LocationClient(this, this, this);
        
        // set toggle buttons to initial values -> off
        toggleUpdatesButton = (ToggleButton) findViewById(R.id.toggleupatesbutton);
        toggleUpdatesButton.setChecked(false);
	}
	
    @Override
    public void onStop() {

        // If the client is connected
        if (mLocationClient.isConnected()) {
            stopPeriodicUpdates();
        }

        // After disconnect() is called, the client is considered "dead".
        mLocationClient.disconnect();

        super.onStop();
    }

    @Override
    public void onPause() {

        // Save the current setting for updates
        mEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, mUpdatesRequested);
        mEditor.commit();

        super.onPause();
    }

    @Override
    public void onStart() {

        super.onStart();

        //Connect the client. Don't re-start any requests here; instead, wait for onResume()
        mLocationClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();

        // If the app already has a setting for getting location updates, get it
        if (mPrefs.contains(LocationUtils.KEY_UPDATES_REQUESTED)) {
            mUpdatesRequested = mPrefs.getBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);

        // Otherwise, turn off location updates until requested
        } else {
            mEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);
            mEditor.commit();
        }
    }
    
    /**
     * Invoked by "Updates" toggle button - makes the UI a bit smoother
     * Starts or stops location updates
     * 
     * @param v The view object associated with this method, in this case a ToggleButton.
     */
    public void onToggleUpdatesClicked(View v){
    	// Is the toggle on?
        boolean on = ((ToggleButton) v).isChecked();
        
        if (on) {
            mUpdatesRequested = true;
            if (servicesConnected()) startPeriodicUpdates();
        } else {
            mUpdatesRequested = false;
            if(servicesConnected()) stopPeriodicUpdates();
        }

    }
    
    /**
     * Invoked by "Markers" toggle button
     * Stars markers or clears map of them
     * @param start starting point as LatLng
     * @param distance distance set
     */
    private void drawMarkers(LatLng start, Double distance){
    		// only display shops with products, otherwise all of them
    		if(!(UserShops == null) && !UserShops.isEmpty()){
    			createMarkers(UserShops);
    		} else {
    			HashMap <Integer, Shop> hm = new HashMap<Integer, Shop>();
    			hm = filterShopsByDistance(AllShops, distance, start);
    			
    			if(hm == null || hm.isEmpty()){
    				h.displayToast(R.string.display_all_shops, getApplicationContext());
    				createMarkers(AllShops);
    			} else {
    				h.displayToast(R.string.display_all_shops_in_reach, getApplicationContext());
    				createMarkers(hm);
    			}
    			
    		}
    }
    
    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    private void startPeriodicUpdates() {

        mLocationClient.requestLocationUpdates(mLocationRequest, this);
        //h.displayToast(R.string.location_requested);
    }

    /**
     * In response to a request to stop updates, send a request to
     * Location Services
     */
    private void stopPeriodicUpdates() {
        mLocationClient.removeLocationUpdates(this);
        //h.displayToast(R.string.location_updates_stopped);
    }
	
	/**
     * Verify that Google Play services is available before making a request.
     *
     * @return true if Google Play services is available, otherwise false
     */
    private boolean servicesConnected() {

        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d(LocationUtils.APPTAG, getString(R.string.play_services_available));

            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
            // Display an error dialog
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0);
            if (dialog != null) {
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(dialog);
                errorFragment.show(getSupportFragmentManager(), LocationUtils.APPTAG);
            }
            return false;
        }
    }
    
    /**
     * Show a dialog returned by Google Play services for the
     * connection error code
     *
     * @param errorCode An error code returned from onConnectionFailed
     */
    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
            errorCode,
            this,
            LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            errorFragment.show(getSupportFragmentManager(), LocationUtils.APPTAG);
        }
    }
    
    /**
     * Handle results returned to this Activity by other Activities started with
     * startActivityForResult(). In particular, the method onConnectionFailed() in
     * LocationUpdateRemover and LocationUpdateRequester may call startResolutionForResult() to
     * start an Activity that handles Google Play services problems. The result of this
     * call returns here, to onActivityResult.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        // Choose what to do based on the request code
        switch (requestCode) {

            // If the request code matches the code sent in onConnectionFailed
            case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST :

                switch (resultCode) {
                    // If Google Play services resolved the problem
                    case Activity.RESULT_OK:

                        // Log the result
                        Log.d(LocationUtils.APPTAG, getString(R.string.resolved));

                        // Display the result
                        h.displayToast(R.string.connected, getApplicationContext());
                        h.displayToast(R.string.resolved, getApplicationContext());
                    break;

                    // If any other result was returned by Google Play services
                    default:
                        // Log the result
                        Log.d(LocationUtils.APPTAG, getString(R.string.no_resolution));

                        // Display the result
                        h.displayToast(R.string.disconnected, getApplicationContext());
                        h.displayToast(R.string.no_resolution, getApplicationContext());

                    break;
                }

            // If any other request code was received
            default:
               // Report that this Activity received an unknown requestCode
               Log.d(LocationUtils.APPTAG,
                       getString(R.string.unknown_activity_request_code, requestCode));

               break;
        }
    }
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	 /**
     * Called by Location Services if the attempt to
     * Location Services fails.
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {

                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */

            } catch (IntentSender.SendIntentException e) {

                // Log the error
                e.printStackTrace();
            }
        } else {

            // If no resolution is available, display a dialog to the user with the error.
            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    /**
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, the current location can be requested
     * and periodic updates are possible
     * 
     * @param bundle bundle thingy
     */
    @Override
    public void onConnected(Bundle bundle) {
    	//h.displayToast(R.string.connected, getApplicationContext());

    	// automatically get location and set center of map to it
    	
    	// If Google Play Services is available
        if (servicesConnected()) {

            // Get the current location
            Location curLoc = mLocationClient.getLastLocation();
            CurrentLocation = new LatLng(curLoc.getLatitude(), curLoc.getLongitude());
            
            // get user details from local sqlite db created after login
            SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(getApplicationContext());
            HashMap <String, String> user = db.getUserDetails();
            
            // Get distance from user preferences
            UserPreferences up = new UserPreferences(this);
            maxdistance = Double.parseDouble(up.getPreferencesByCategory("distance")) / 10; //distance saved as hundred of meters, divide by 10 get kilometers required for google directions api
            
            //fake current location for testing
//            int uid = Integer.parseInt(user.get("uid"));
//            switch(uid){
//            	case 4: CurrentLocation = CoordMuensterPlatz; break;
//            	case 5:	CurrentLocation = CoordEselsbergCenter;	/*maxdistance = 0.2*/; break;
//            	case 6: CurrentLocation = CoordMuensterPlatz; /*maxdistance = 0.5*/; break;
//            	default: CurrentLocation = CoordMuensterPlatz; maxdistance = 1;
//            }

            Double user_lat = Double.parseDouble(user.get("lat")); 
            Double user_lng = Double.parseDouble(user.get("lng"));
            
            if(user_lat != 0 && user_lng != 0){
            	home = new LatLng(user_lat, user_lng);
            } else {
            	//home = new LatLng(CoordEselsbergCenter.latitude, CoordEselsbergCenter.longitude);
            	home = new LatLng(CoordMuensterPlatz.latitude, CoordMuensterPlatz.longitude);
            }
            
            // move camera to current location in UI
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(CurrentLocation, 15));
        }
    	
        if (mUpdatesRequested) {
            startPeriodicUpdates();
        }
        
        if(CurrentLocation == null){
        	h.displayToast(R.string.location_unknown, getApplicationContext());
        	return;
        }
        
        // get shops, draw markers and generate and draw route
        AllShops = getShops();
        /*
        UserShops = filterShopsWithoutProducts(AllShops);
        UserShops = filterShopsWithoutProductsOnShoppingList(UserShops);
        //UserShops = filterShopsByDistance(UserShops, maxdistance, home);
        */
        UserShops = filterShopsByShoppinglist(AllShops);
        
        drawMarkers(home, maxdistance);
        generateRoute(home, maxdistance);
    }

    /**
     * gets shops from database and puts them in a hashmap
     * index is same as id in database, not a continuous number!
     * Use an iterator to go through hashmap
     * 
     * @return hashmap with shops
     */
	private HashMap<Integer, Shop> getShops() {
		
		HashMap <Integer, Shop> hm = new HashMap<Integer, Shop>(); 
		
		// prepare JSON Parser and parameters to get list of hops
        JSONParser jsonParser = new JSONParser();        
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "shops"));
        
        // get shops
        JSONObject shops = jsonParser.getJSONFromUrl(API_URL, params);
        
        if (shops == null){
        	Log.e(getClass().getSimpleName(), "json shops is null");
        }
        
        // get products
        List<NameValuePair> params2 = new ArrayList<NameValuePair>();
        params2.add(new BasicNameValuePair("tag", "products"));
        JSONObject products = jsonParser.getJSONFromUrl(API_URL, params2);
        
        if(products == null){
        	Log.e(getClass().getSimpleName(), "json products is null");
        }
        
        try{
        	String res = shops.getString("success");
        	String res2 = products.getString("success");
        	
        	// check if product is any of the shops
        	if(Integer.parseInt(res) == 1 && Integer.parseInt(res2) == 1){
        		JSONArray json_shops = shops.getJSONArray("shops");
        		JSONArray json_products = products.getJSONArray("products");
        		
        		for(int i = 0; i < json_shops.length(); i++){        			
        			JSONObject shop = json_shops.getJSONObject(i);
        		
        			String n = "";
        			Double lat, lng;
        			String p[][] = null;
        		
        			n = shop.getString("name");
        			lat = Double.parseDouble(shop.getString("lat"));
        			lng = Double.parseDouble(shop.getString("lng"));
        		
        			int numProducts = 0;
        			for(int j = 0; j < json_products.length(); j++){
        				JSONObject product = json_products.getJSONObject(j);
        		
        				// check if product is in currently selected shop
        				if(Integer.parseInt(product.getString("shop_id")) == Integer.parseInt(shop.getString("id"))){
        					numProducts++;
        					String[][] tempArray = new String[numProducts][2];
        					if(p != null){
        						System.arraycopy(p, 0, tempArray, 0, p.length);
        					} else {
        						tempArray = new String[1][2];
        					}
        				
        					tempArray[tempArray.length-1][0] = product.getString("product_name");
        					tempArray[tempArray.length-1][1] = product.getString("price");
        				
        					p = tempArray;
        				}       			
        			}	
        		
        			Shop s = new Shop(n, lat, lng, p);
        			hm.put(Integer.parseInt(shop.getString("id")), s);
        		}
        		
        	} else {
        		// shops not found
        		h.displayToast(R.string.no_shops_found, getApplicationContext());
        		Log.d(getClass().getSimpleName(), shops.getString("error_msg"));
        	}        	
        }
        catch(JSONException e){
        	e.printStackTrace();
        }
        
        return hm;
	}
	
	/**
	 * Creates markers on map for shops from data in hash map
	 * @param hm hash map with shops
	 */
	private void createMarkers(HashMap<Integer, Shop> hm){
		if(!(hm == null) &&!hm.isEmpty()){
			Set<Integer> ks = hm.keySet();
			for(Integer i : ks ){
					Shop s = hm.get(i);				
					if(s.hasProducts()){
						String products = "";
						String [][] productsArray = s.getProducts();
						for(int j = 0; j < productsArray.length; j++){
							if(j != 0){
								products += ", " + productsArray[j][0] /*+ "[" + productsArray[j][1] + "]" */; //display prices for debugging
							} else {
								products = productsArray[j][0] /*+ "[" + productsArray[j][1] + "]"*/;
							}
						}
						
						map.addMarker(new MarkerOptions()
						   .position( new LatLng (s.getLat(), s.getLng()))
						   .title(s.getName())
						   .snippet(products));
					} else {
						map.addMarker(new MarkerOptions()
						   .position( new LatLng (s.getLat(), s.getLng()))
						   .title(s.getName()));					
					}				
			}
		}
		
		// create marker for current location
		CurrentLocationMarker = map.addMarker(new MarkerOptions()
		   						  .position(CurrentLocation)
		   						  .title(getString(R.string.user_current_location))
		   						  .icon(BitmapDescriptorFactory.fromResource(R.drawable.superstar_32x32)));
	}
	
	/**
	 * creates route for shopping tour
	 * origin is current location of user, destination is the shop with the largest distance to origin
	 * all shops in between are used as waypoints depending on their distance  
	 * 
	 * @param hm hash map with shops
	 * @param startPosition LatLng object with current position
	 */
	private void getRoute(HashMap<Integer, Shop> hm, LatLng startPosition){
		Double [][] positions = null;
		Set<Integer> ks = hm.keySet();
		int arraySize = 0;
		
		// create array from shops with additional distance
		for(Integer i : ks){
			Shop s = hm.get(i);			
			if(s.hasProducts()){
				arraySize++;
				Double [][] tempArray = new Double [arraySize][4];
				if(positions != null){					
					System.arraycopy(positions, 0, tempArray, 0, positions.length);					
				} else {
					positions = new Double [1][4];				
				}
				
				tempArray[arraySize-1][0] = i.doubleValue(); // shop id / key in hashmap
				tempArray[arraySize-1][1] = s.getLat();
				tempArray[arraySize-1][2] = s.getLng();
				tempArray[arraySize-1][3] = 0.0; // distance to current position
				
				positions = tempArray;				
			}
		}
		
		// calculate distances
		for(int i = 0; i < positions.length; i++){
			h.calcDistance(startPosition, new LatLng(positions[i][1].doubleValue(), positions[i][2].doubleValue()));
		}
		
		// sort by distance to current position descending
		Arrays.sort(positions, new Comparator<Double[]>(){
			@Override
			//comperator
			public int compare(final Double[] entry1, final Double[] entry2){
				final Double d1 = entry1[3];
				final Double d2 = entry2[3];
				return d1.compareTo(d2);
			}
		});
		
		String params = "";
		params += "?origin=" + startPosition.latitude + "," + startPosition.longitude;
		params += "&destination=" + positions[0][1] + "," + positions[0][2];
		String wp = "";
		// arrays is sorted descending by distance, therefore go through it backwards
		for (int i = positions.length-1; i >= 1; i--){
			if (i != positions.length-1){
				wp += "|" + positions[i][1] + "," + positions[i][2];
			} else {
				wp += positions[i][1] + "," + positions[i][2];
			}
		}
		
		params += "&waypoints=" + wp;
		params += "&sensor=true"; // this has to be added in any case
		params += "&mode=walking"; // works fine, but mostly along streets 
		
		String url = DIRECTIONS_API_URL + params;
		String data = "";
		
		try{
			// using this function because getting the JSON like before results in an error I could not solve
			data = downloadFromUrl(url);
		} catch(IOException e){
			Log.d("downloading form url",  e.toString());
			e.printStackTrace();
		}
		
		JSONObject jDirections = null;
		List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;
        //JSONObject jDistance = null;
        //JSONObject jDuration = null;
        
        /* following code is courtesy of with slight modifications: http://wptrafficanalyzer.in/blog/driving-distance-and-travel-time-duration-between-two-locations-in-google-map-android-api-v2/         */
		try{
			jDirections = new JSONObject(data);
			
	        String jStatus = jDirections.getString("status");
	        
	        if(jStatus.equals("OK")){
	        	jRoutes = jDirections.getJSONArray("routes");
	        	
	        	/** Traversing all routes */
	            for(int i=0;i<jRoutes.length();i++){
	                jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");

	                List<HashMap<String, String>> path = new ArrayList<HashMap<String, String>>();

	                /** Traversing all legs */
	                for(int j=0;j<jLegs.length();j++){

	                	/* that info would be interesting but results in errors because its not properly 
	                	 * split across multiple hashmaps and then parsing while creating the polygon line fails
	                	 * Also there is no space to display it
	                	 */
	                	/*
	                    // Getting distance from the json data
	                    jDistance = ((JSONObject) jLegs.get(j)).getJSONObject("distance");
	                    HashMap<String, String> hmDistance = new HashMap<String, String>();
	                    hmDistance.put("distance", jDistance.getString("text"));

	                    // Getting duration from the json data
	                    jDuration = ((JSONObject) jLegs.get(j)).getJSONObject("duration");
	                    HashMap<String, String> hmDuration = new HashMap<String, String>();
	                    hmDuration.put("duration", jDuration.getString("text"));

	                    // Adding distance object to the path
	                    path.add(hmDistance);

	                    // Adding duration object to the path
	                    path.add(hmDuration);
						*/
	                    jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");

	                    // Traversing all steps
	                    for(int k=0;k<jSteps.length();k++){
	                        String polyline = "";
	                        polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
	                        List<LatLng> list = decodePoly(polyline);

	                        // Traversing all points
	                        for(int l=0;l<list.size();l++){
	                            HashMap<String, String> hm2 = new HashMap<String, String>();
	                            hm2.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
	                            hm2.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
	                            path.add(hm2);
	                        }
	                    }
	                }
	                routes.add(path);
	            }
	            
	            ArrayList<LatLng> points = null;
				PolylineOptions lineOptions = null;
	            //String distance = "";
				//String duration = "";
	            
	         // Traversing through all the routes
				for(int i=0;i<routes.size();i++){
					points = new ArrayList<LatLng>();
					lineOptions = new PolylineOptions();
					
					// Fetching i-th route
					List<HashMap<String, String>> path = routes.get(i);
					
					// Fetching all the points in i-th route
					for(int j=0;j<path.size();j++){
						HashMap<String,String> point = path.get(j);	
						/*
						if(j==0){	// Get distance from the list
							distance = (String)point.get("distance");						
							continue;
						}else if(j==1){ // Get duration from the list
							duration = (String)point.get("duration");
							continue;
						}
						*/
						double lat = Double.parseDouble(point.get("lat"));
						double lng = Double.parseDouble(point.get("lng"));
						LatLng position = new LatLng(lat, lng);	
						
						points.add(position);						
					}
					
					// Adding all the points in the route to LineOptions
					lineOptions.addAll(points);
					lineOptions.width(2);
					lineOptions.color(Color.RED);	
					
				}
				
				//tvDistanceDuration.setText("Distance:"+distance + ", Duration:"+duration);
				
				// Drawing polyline in the Google Map for the i-th route
				map.addPolyline(lineOptions);	
				
				/* end of foreign code */
	        	
	        } else {
	        	DIRECTIONS_STATUS_CODES status = DIRECTIONS_STATUS_CODES.valueOf(jStatus);
		        switch(status){
		        	case NOT_FOUND: Log.d("One of the points supplied was not found", jDirections.getString("error_message")); return;
		        	case ZERO_RESULTS: Log.d("No routes found", jDirections.getString("error_message")); return;
		        	case MAX_WAYPOINTS_EXCEEDED: Log.d("to many waypoints supplied", jDirections.getString("error_message")); return;
		        	case INVALID_REQUEST: Log.d("invalid request", jDirections.getString("error_message")); return;
		        	case OVER_QUERY_LIMIT: Log.d("query limit reached", jDirections.getString("error_message")); return;
		        	case REQUEST_DENIED: Log.d("request denied", jDirections.getString("error_message")); return;
		        	case UNKNOWN_ERROR: Log.d("unknown error occured", jDirections.getString("error_message")); return;
		        	default: Log.d("unknown error code", jDirections.getString("error_message")); return;
		        }	         
	        }
			
		} catch(JSONException e){
			Log.d("something went horribly wrong with the JSON strcuture", e.toString());
		}		
	}
	
	/**
	 * Generates a route and displays it on map as red polyline
	 * @param start starting point as LatLng
	 * @param distance distance set
	 */
	private void generateRoute(LatLng start, Double distance){
		/*
		HashMap<Integer, Shop> fs = filterShopsWithoutProducts(AllShops);
		fs = filterShopsWithoutProductsOnShoppingList(fs);
		fs = filterShopsByDistance(fs, distance, start);
		*/
		if(!(UserShops == null) && !UserShops.isEmpty()){
			getRoute(filterShopsByDistance(UserShops, maxdistance, start), CurrentLocation);
		} else {
			h.displayToast(R.string.route_calc_not_possible, getApplicationContext());
		}
	}
	
	/**
	 * Generates new route from current location
	 * and draws it on the map
	 * @param v view associated with current object
	 */
	public void onManualRouteButtonClicked(View v){
		// stuff defined as final to access them in sub-methods such as onClick and onSeekBarChange
		final Dialog tempDistanceDialog = new Dialog(this);
		LayoutInflater inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.dialog_tempdistance, (ViewGroup)findViewById(R.id.dialog_tempdistance));
		tempDistanceDialog.setContentView(layout);
		tempDistanceDialog.setTitle(R.string.temp_distancedialog_title);
		
		TextView label = (TextView) layout.findViewById(R.id.dialog_tempdistance_label);
		label.setText("0 km");
		
		final SeekBar sbDis = (SeekBar) layout.findViewById(R.id.dialog_tempdistance_seek);
		sbDis.setMax((int)(maxdistance*10));
		sbDis.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if(fromUser){
					TextView label = (TextView) layout.findViewById(R.id.dialog_tempdistance_label);
					label.setText(String.valueOf((double)progress / 10) + "km");
				}
				
			}
		});
		
		
		// OK button
		Button btnpos = (Button) layout.findViewById(R.id.dialog_tempdistance_btnpos);
		btnpos.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tmpdistance = (double)sbDis.getProgress() / 10;
				map.clear();
				drawMarkers(CurrentLocation, tmpdistance);
				generateRoute(CurrentLocation, tmpdistance);
				tempDistanceDialog.dismiss(); //close dialog
			}
		});
		
		// cancel button
		Button btnneg = (Button) layout.findViewById(R.id.dialog_tempdistance_btnneg);
		btnneg.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tempDistanceDialog.dismiss(); //close dialog
			}
		});		
		
		tempDistanceDialog.show();
	}
	
	/**
	 * Method to decode polyline points
	 * Courtesy : jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
	 * 
	 * @param encoded polyline encoded as string
	 * @return list of points as pairs of latitude and longitude
	 */
	 private List<LatLng> decodePoly(String encoded) {

	    List<LatLng> poly = new ArrayList<LatLng>();
	    int index = 0, len = encoded.length();
	    int lat = 0, lng = 0;

	    while (index < len) {
	        int b, shift = 0, result = 0;
	        do {
	            b = encoded.charAt(index++) - 63;
	            result |= (b & 0x1f) << shift;
	            shift += 5;
	        } while (b >= 0x20);
	            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	            lat += dlat;

	        shift = 0;
	        result = 0;
	        do {
	            b = encoded.charAt(index++) - 63;
	            result |= (b & 0x1f) << shift;
	            shift += 5;
	        } while (b >= 0x20);
	        int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	        lng += dlng;

	        LatLng p = new LatLng((((double) lat / 1E5)), (((double) lng / 1E5)));
	        poly.add(p);
	    }
	    return poly;
	}
	
	/**
	 * Downloads output of an URL into a String
	 * Using a WebAPI call results in empty response, no idea why
	 * Source: http://wptrafficanalyzer.in/blog/driving-distance-and-travel-time-duration-between-two-locations-in-google-map-android-api-v2/
	 * 
	 * @param strUrl complete URL to get, including parameters and formatting 
	 * @return String with result
	 * @throws IOException
	 */
	private String downloadFromUrl(String strUrl) throws IOException{
		String data = "";
		InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
	}

//	/**
//	 * Filter HashMap with shops to let only those with products in them
//	 * 
//	 * @param shops hashmap containing shops
//	 * @return hashmap with shops that have products
//	 */
//	private HashMap<Integer, Shop> filterShopsWithoutProducts (HashMap<Integer, Shop> shops) {
//		if((shops == null) || shops.isEmpty()){
//			return null;
//		}
//		
//		HashMap<Integer, Shop> hm = new HashMap<Integer, Shop>();		
//		
//		Set<Integer> ks = shops.keySet();
//		for(Integer i : ks){
//			Shop s = shops.get(i);
//			if(s.hasProducts()){
//				hm.put(i, s);
//			}
//		}
//		
//		return hm;
//	}
//	
//	/**
//	 * Filters shops from hashmaps which don't have products currently on users shopping list
//	 * 
//	 * @param shops hashmap containing shops
//	 * @return filtered hashmap
//	 */
//	private HashMap<Integer, Shop> filterShopsWithoutProductsOnShoppingList(HashMap<Integer, Shop> shops){
//		if(shops == null || shops.isEmpty()){
//			return null;
//		}
//		
//        // get shopping list to user id
//        SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(getApplicationContext());
//        HashMap <String, String> user = db.getUserDetails();
//        int uid = Integer.parseInt(user.get("uid"));
//		
//		HashMap<Integer, Shop> hm =  new HashMap<Integer, Shop>();
//		String[] products = getProductsFromProductListOfUser(uid);
//		
//		if(products == null){
//			h.displayToast(R.string.user_has_no_shoppinglist, getApplicationContext());
//			hm = null;
//			return null;
//		}
//		
//		Set<Integer> ks = shops.keySet();
//		for(Integer i : ks){
//			Shop s = shops.get(i);
//			String [][] shop_products = s.getProducts();
//			for(int j = 0; j < products.length; j++){
//				for( int k = 0; k < shop_products.length; k++){
//					if(products[j].equals(shop_products[k][0])){
//						hm.put(i, s);
//					}
//				}
//			}
//		}
//
//		return hm;
//	}
	
	/**
	 * Filters shops which are not with products on shopping list associated,
	 * leaving only those which match all criteria
	 * @param shops hashmap with shops
	 * @return filtered hashmap
	 */
	private HashMap <Integer, Shop> filterShopsByShoppinglist(HashMap<Integer, Shop> shops){
		if(shops == null || shops.isEmpty()){
			return null;
		}
		
		HashMap <Integer, Shop> hm = new HashMap<Integer, Shop>();
		// get shopping list to user id
        SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(getApplicationContext());
        HashMap <String, String> user = db.getUserDetails();
        int uid = Integer.parseInt(user.get("uid"));
        JSONArray shoppinglist = getShoppinglist(uid);
        
        try {
        	
        	if(shoppinglist != null){
        		for(int i=0; i < shoppinglist.length(); i++){
    				JSONObject p = shoppinglist.getJSONObject(i);
    				
    				int shop_id = Integer.parseInt(p.getString("shop_id"));
    				Shop s = shops.get(shop_id);
    				hm.put(shop_id, s);
        		}
			
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
       
		
		
		return hm;
	}
	
	/**
	 * Filters HashMap with shops by distance, eliminating shops exceeding maximal distance set by user
	 * 
	 * @param shops HashMap with shops
	 * @param maxdis Maximal distance set by user in kilometers
	 * @param start starting point as LatLng
	 * @return filtered HashMap
	 */
	private HashMap <Integer, Shop> filterShopsByDistance(HashMap<Integer, Shop> shops, double maxdis, LatLng start){
		if(shops == null || shops.isEmpty() || start == null){
			return null;
		}
		
		HashMap <Integer, Shop> hm = new HashMap<Integer, Shop>();
	
		Set<Integer> ks = shops.keySet();		
		for(Integer i : ks){
			Shop s = shops.get(i);
			Double distance = h.calcDistance(start, new LatLng(s.getLat(), s.getLng()));
			if(distance <= maxdis){
				hm.put(i, s);
			}
		}
		
		return hm;
	}
	
//	/**
//	 * gets products on current users shopping list
//	 * 
//	 * @param uid user id
//	 * @return string array with names of products
//	 */
//	private String [] getProductsFromProductListOfUser(int uid){			
//		String [] productsArray = null;
//		
//		JSONArray shoppinglist = getShoppinglist(uid);
//        
//        try{
//        		productsArray = new String[shoppinglist.length()];
//        		
//        		for(int i = 0; i < shoppinglist.length(); i++){
//        			JSONObject p = shoppinglist.getJSONObject(i);
//        			productsArray[i] = p.getString("product_name");
//        		}
//        	
//        } catch(JSONException e){
//        	Log.e(getClass().getSimpleName(), "json exeption: " + e.getMessage());
//        }
//		
//		return productsArray;
//	}

	/**
	 * get shoppinglist of user from database
	 * @param uid User ID
	 * @return shoppinglist as JSONArray
	 */
	private JSONArray getShoppinglist(int uid) {
		// prepare JSON Parser and parameters to get list of hops
        JSONParser jsonParser = new JSONParser();        
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", "shoppinglist"));
        params.add(new BasicNameValuePair("user", Integer.toString(uid)));
        params.add(new BasicNameValuePair("withouttags", "true")); //and all because andy has no clue about his code
        
        // get shops
        JSONObject shoppinglist = jsonParser.getJSONFromUrl(API_URL, params);
		
        if(shoppinglist == null){
        	Log.e(getClass().getSimpleName(), "json shoppinglist is null");
        	return null;
        }
        
        JSONArray json_shoppinglist = null;
        
        try {
			String res = shoppinglist.getString("success");
			if(Integer.parseInt(res) == 1){
				json_shoppinglist = shoppinglist.getJSONArray("shoppinglist");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
        
		return json_shoppinglist;
	}
	
	/**
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
    	h.displayToast(R.string.disconnected, getApplicationContext());
    }


    /**
     * Reports to the UI that the location was updated
     *
     * @param location The updated location.
     */
    @Override
    public void onLocationChanged(Location location) {
    	h.displayToast(R.string.location_updated, getApplicationContext());
    	
    	// update location, move camera and mark old location to reduce confusion of user (hopefully)
    	CurrentLocation = new LatLng(location.getLatitude(), location.getLongitude());
    	map.moveCamera(CameraUpdateFactory.newLatLngZoom(CurrentLocation, 15));
    	CurrentLocationMarker.remove();
    	LatLng startPosition = CurrentLocationMarker.getPosition();
    	map.addMarker(new MarkerOptions()
    				.position(startPosition)
    				.title(getString(R.string.user_start_location))
    				.icon(BitmapDescriptorFactory.fromResource(R.drawable.superstar_32x32_sw))
    				);		
    	CurrentLocationMarker = null;
    	CurrentLocationMarker = map.addMarker(new MarkerOptions()
			  						.position(CurrentLocation)
			  						.title(getString(R.string.user_current_location))
			  						.icon(BitmapDescriptorFactory.fromResource(R.drawable.superstar_32x32)));
        
    	checkForShopsNearby(location);
		
    }

    /**
     * Checks if shops with products on shoppinglist are in reach 
     * @param location
     */
	private void checkForShopsNearby(Location location) {
		Set<Integer> ks = UserShops.keySet();
    	distanceToNearestShop=-1.0f;
    	Integer nearestShopId = -1;
		for(Integer i : ks ){
			Shop s = UserShops.get(i);
			nearestShopLocation = new Location("nearestShopLocation");
			nearestShopLocation.setLatitude(s.getLat());
			nearestShopLocation.setLongitude(s.getLng());
			float currentDistanceToShop=location.distanceTo(nearestShopLocation);
			if(distanceToNearestShop == -1.0f){ 
				distanceToNearestShop = currentDistanceToShop;
				nearestShop = s;
				nearestShopId = i;
			}
			else if(distanceToNearestShop > currentDistanceToShop){
				distanceToNearestShop = currentDistanceToShop;
				nearestShop = s;
				nearestShopId = i;
			}
		}
		if(distanceToNearestShop < 10.0f) ShoppingListActivity.shop_id = (int)nearestShopId;
		else if(distanceToNearestShop < 30.0f && !nearestShopNotificationSend) sendMapNotification();
	}
    
    /**
     * Sends notification if near a shop with products on shoppinglist and in reach 
     */
    public void sendMapNotification(){
    	NotificationCompat.Builder notification;
		NotificationManager mNotificationManager;
		PendingIntent nPendingIntent = PendingIntent.getActivity(this, 0, new Intent(),PendingIntent.FLAG_UPDATE_CURRENT);
		notification = new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.ic_launcher)
						.setContentTitle("Location Notification")
						.setContentText("You are "+(int)distanceToNearestShop+"m away from: "+nearestShop.getName())
						.setContentIntent(nPendingIntent);
		notification.setDefaults(Notification.DEFAULT_SOUND);
		notification.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));			
		notification.setAutoCancel(true);
		mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, notification.build());
		nearestShopNotificationSend = true;
    }
    
    /**
     * Button for demo, fakes current coordinates
     * @param view Current view associated with the object, in this case a button
     */
    public void onDemoButtonClicked (View view){
    	/*
    	 * Dummy Code f�r Steps
    	 */
//    	setDemoLocation(lat, lng);
//    	Location curLoc = new Location("currentLocation");
//    	curLoc.setLatitude(CurrentLocation.latitude);
//    	curLoc.setLongitude(CurrentLocation.longitude);
//    	checkForShopsNearby(new Location("currentLocation"));
    	/*
    	 * End Dummy Code
    	 */
    	
    	 SQLiteDatabaseHandler db = new SQLiteDatabaseHandler(getApplicationContext());
         HashMap <String, String> user = db.getUserDetails();
         demoStep = Integer.parseInt(user.get("demoStep"));
         String uid = user.get("id");
    	
    	switch(demoStep){
    	case 0: {
    		// in der N�he von Shop 1
    		
        	LatLng startPosition = CurrentLocationMarker.getPosition();
        	map.addMarker(new MarkerOptions()
        				.position(startPosition)
        				.title(getString(R.string.user_start_location))
        				.icon(BitmapDescriptorFactory.fromResource(R.drawable.superstar_32x32_sw))
        				);	    		
    		
        	setDemoLocation(48.399594, 9.991517);
        	Location curLoc = new Location("currentLocation");
        	curLoc.setLatitude(CurrentLocation.latitude);
        	curLoc.setLongitude(CurrentLocation.longitude);
        	checkForShopsNearby(curLoc);    		
    		
    		demoStep++;
    		db.setDemoStep(String.valueOf(demoStep), uid);
    		break;
    	}
    	case 1: {
    		// in Shop 1
    		
        	setDemoLocation(48.399697, 9.991061);
        	Location curLoc = new Location("currentLocation");
        	curLoc.setLatitude(CurrentLocation.latitude);
        	curLoc.setLongitude(CurrentLocation.longitude);
        	checkForShopsNearby(curLoc);
    		ShoppingListActivity.shop_id = 5;
    		demoStep++;
    		db.setDemoStep(String.valueOf(demoStep), uid);
    		break;
    	}
    	
    	case 2: {
    		// in der N�he von Shop 2
    		
        	setDemoLocation(48.398222, 9.987293);
        	Location curLoc = new Location("currentLocation");
        	curLoc.setLatitude(CurrentLocation.latitude);
        	curLoc.setLongitude(CurrentLocation.longitude);
        	checkForShopsNearby(curLoc);
    		
    		demoStep++;
    		db.setDemoStep(String.valueOf(demoStep), uid);
    		break;
    	}
    	
    	case 3: {
    		// in Shop 2
    		
        	setDemoLocation(48.398988, 9.984578);
        	Location curLoc = new Location("currentLocation");
        	curLoc.setLatitude(CurrentLocation.latitude);
        	curLoc.setLongitude(CurrentLocation.longitude);
        	checkForShopsNearby(curLoc);
    		ShoppingListActivity.shop_id = 8;
    		//demoStep++;
        	//demoStep = 0;
        	db.setDemoStep(String.valueOf(demoStep), uid);
    		break;
    	}
    	
    	case Integer.MAX_VALUE: {
    		h.displayToast("Demo zu Ende", this.getApplicationContext());
    		break;
    		
    	}
    	default: h.displayToast("Demo zu Ende", this.getApplicationContext());
    	} 
    }

    /**
     * Sets location variables to specified values, used in Demo and Evaluation
     * Should not be called in real version!
     * @param lat Latitude as Double
     * @param lng Longitude as Double
     */
	private void setDemoLocation(Double lat, Double lng) {
		CurrentLocation = new LatLng(lat, lng);
    	map.moveCamera(CameraUpdateFactory.newLatLngZoom(CurrentLocation, 15));
    	CurrentLocationMarker.remove();
	
    	CurrentLocationMarker = null;
    	CurrentLocationMarker = map.addMarker(new MarkerOptions()
			  						.position(CurrentLocation)
			  						.title(getString(R.string.user_current_location))
			  						.icon(BitmapDescriptorFactory.fromResource(R.drawable.superstar_32x32)));
	}
	
	/**
     * showErrorDialog.
     */
    public static class ErrorDialogFragment extends DialogFragment {

        // Global field to contain the error dialog
        private Dialog mDialog;

        /**
         * Default constructor. Sets the dialog field to null
         */
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        /**
         * Set the dialog to display
         *
         * @param dialog An error dialog
         */
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        /*
         * This method must return a Dialog to the DialogFragment.
         */
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

}

